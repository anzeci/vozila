using System.Collections.Generic;
using VascoWebLib.Core.UI.Interfaces.Menu;

namespace VascoWebLib.Infrastructure.Services.Menu
{
    public class VascoMenuItem : IVascoMenuItem
    {
        private readonly IVascoMenuService _service;
        public string ID { get; set; }
        public string CategoryId { get; set; }
        public string Text { get; set; }
        public bool Expanded { get; set; }
        public bool Selected { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public IList<IVascoMenuItem> SubItems { get; set; }

        public VascoMenuItem(IVascoMenuService service, bool showExpanded)
        {
            _service = service;
            SubItems = new List<IVascoMenuItem>();
            ID = string.Empty;
            CategoryId = string.Empty;
            Text = string.Empty;
            Expanded = showExpanded;
            Selected = false;
            Image = string.Empty;
            Url = string.Empty;
        }

        public void CheckSelected(string currentUrl)
        {
            Selected = currentUrl.ToLower().Equals(Url.ToLower());
        }
    }
}