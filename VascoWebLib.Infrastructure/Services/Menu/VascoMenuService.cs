using System.Collections.Generic;
using VascoWebLib.Core.UI.Interfaces.Menu;

namespace VascoWebLib.Infrastructure.Services.Menu
{
    public class VascoMenuService : IVascoMenuService
    {
        protected readonly string _currentUrl;
        protected readonly bool _showExpanded;

        public string ImagesRootPath { get; }

        public VascoMenuService(string imagesRootPath, string currentUrl, bool showExpanded)
        {
            _currentUrl = currentUrl;
            _showExpanded = showExpanded;
            ImagesRootPath = imagesRootPath;
        }

        public virtual IList<IVascoMenuItem> GetMenuItems()
        {
            var result = new List<IVascoMenuItem>();
            return result;
        }
    }
}