using VascoWebLib.Core.Interfaces.Database;

namespace VascoWebLib.Infrastructure.Database
{
    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        public string Provider { get; set; }
        public string ConnectionString { get; set; }

        /// <summary>
        /// Firebird
        /// </summary>
        public static string FirebirdProviderName
        {
            get
            {
                return "Firebird";
            }
        }
        /// <summary>
        ///  SqlServer.version
        /// </summary>
        /// <param name="version">2000, 2005, 2008, 2012, 2014</param>
        /// <returns></returns>
        public static string SqlServerProviderName(int version)
        {
            return string.Format("SqlServer.{0}", version);
        }
    }
}
