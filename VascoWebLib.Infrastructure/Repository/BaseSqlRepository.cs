using LinqToDB.Common;
using LinqToDB.Data;
using System;
using VascoWebLib.Core.Interfaces.Database;

namespace VascoWebLib.Core.Repository
{
    public class BaseSqlRepository : IDisposable
    {
        private ISqlConnectionProvider connectionProvider;
        protected DataConnection db = null;

        public BaseSqlRepository(ISqlConnectionProvider connectionProvider)
        {
            this.connectionProvider = connectionProvider;
            //Configuration.Linq.DisableQueryCache = true;
            CreateDataConnection();
        }

        private void CreateDataConnection()
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
            db = new DataConnection(connectionProvider.Provider, connectionProvider.ConnectionString);
        }

        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }
    }

}
