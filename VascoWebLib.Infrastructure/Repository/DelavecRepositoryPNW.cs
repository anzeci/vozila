using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository
{
    public class DelavecRepositoryPNW : BaseSqlRepository, IDelavecRepositoryPNW
    {
        public DelavecRepositoryPNW(ISqlConnectionProvider context) : base(context)
        {

        }

        public bool Delete(int sifra)
        {
            return db.GetTable<PNW_DELAVEC>().Delete(f => f.SIFRA == sifra) > 0;
        }

        public IEnumerable<PNW_DELAVEC> GetAll()
        {
            return db.GetTable<PNW_DELAVEC>().LoadWith(j=>j.VOZILO_OBJ).LoadWith(j => j.SM_OBJ).LoadWith(j => j.SM2_OBJ).ToList();
        }

        public LoadResult GetAll(DataSourceLoadOptionsBase loadOptions)
        {
            return DataSourceLoader.Load(db.GetTable<PNW_DELAVEC>().LoadWith(j => j.VOZILO_OBJ).LoadWith(j => j.SM_OBJ).LoadWith(j => j.SM2_OBJ), loadOptions);
        }

        public PNW_DELAVEC GetBySifra(int sifra)
        {
            return db.GetTable<PNW_DELAVEC>().LoadWith(j => j.VOZILO_OBJ).LoadWith(j => j.SM_OBJ).LoadWith(j => j.SM2_OBJ).Where(f => f.SIFRA == sifra).FirstOrDefault();
        }

        public int GetNextSifra()
        {
            int? maxSifra = db.GetTable<PNW_DELAVEC>().Max(m => m.SIFRA);

            if (maxSifra.HasValue)
                maxSifra = maxSifra.Value + 1;

            return maxSifra.GetValueOrDefault(1);
        }

        public bool Insert(PNW_DELAVEC item)
        {
            item.SIFRA = GetNextSifra();
            return db.Insert<PNW_DELAVEC>(item) > 0;
        }

        public bool Update(PNW_DELAVEC item)
        {
            return db.Update<PNW_DELAVEC>(item) > 0;
        }
    }
}
