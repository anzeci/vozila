using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository
{
    public class SM2Repository : BaseSqlRepository, ISM2Repository
    {
        public SM2Repository(ISqlConnectionProvider context) : base(context)
        {

        }

        public bool Delete(int sifra)
        {
            return db.GetTable<SM2>().Delete(f => f.SIFRA == sifra) > 0;
        }

        public IEnumerable<SM2> GetAll()
        {
            return db.GetTable<SM2>().ToList();
        }

        public LoadResult GetAll(DataSourceLoadOptionsBase loadOptions)
        {
            return DataSourceLoader.Load(db.GetTable<SM2>(), loadOptions);
        }

        public SM2 GetBySifra(int sifra)
        {
            return db.GetTable<SM2>().Where(f => f.SIFRA == sifra).FirstOrDefault();
        }

        public int GetNextSifra()
        {
            int? maxSifra = db.GetTable<SM2>().Max(m => m.SIFRA);

            if (maxSifra.HasValue)
                maxSifra = maxSifra.Value + 1;

            return maxSifra.GetValueOrDefault(1);
        }

        public bool Insert(SM2 item)
        {
            item.SIFRA = GetNextSifra();
            return db.Insert<SM2>(item) > 0;
        }

        public bool Update(SM2 item)
        {
            return db.Update<SM2>(item) > 0;
        }
    }
}
