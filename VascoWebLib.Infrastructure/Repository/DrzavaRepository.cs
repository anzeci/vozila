using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository
{
    public class DrzavaRepository : BaseSqlRepository, IDrzavaRepository
    {
        public DrzavaRepository(ISqlConnectionProvider context) : base(context)
        {

        }

        public bool Delete(int sifra)
        {
            return db.GetTable<DRZAVA>().Delete(f => f.SIFRA == sifra) > 0;
        }

        public IEnumerable<DRZAVA> GetAll()
        {
            return db.GetTable<DRZAVA>().ToList();
        }

        public LoadResult GetAll(DataSourceLoadOptionsBase loadOptions)
        {
            return DataSourceLoader.Load(db.GetTable<DRZAVA>(), loadOptions);
        }

        public DRZAVA GetBySifra(int sifra)
        {
            return db.GetTable<DRZAVA>().Where(f => f.SIFRA == sifra).FirstOrDefault();
        }

        public int GetNextSifra()
        {
            int? maxSifra = db.GetTable<DRZAVA>().Max(m => m.SIFRA);

            if (maxSifra.HasValue)
                maxSifra = maxSifra.Value + 1;

            return maxSifra.GetValueOrDefault(1);
        }

        public bool Insert(DRZAVA item)
        {
            item.SIFRA = GetNextSifra();
            return db.Insert<DRZAVA>(item) > 0;
        }

        public bool Update(DRZAVA item)
        {
            return db.Update<DRZAVA>(item) > 0;
        }
    }
}
