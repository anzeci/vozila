using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository
{
    public class SMRepository : BaseSqlRepository, ISMRepository
    {
        public SMRepository(ISqlConnectionProvider context) : base(context)
        {

        }

        public bool Delete(int sifra)
        {
            return db.GetTable<SM>().Delete(f => f.SIFRA == sifra) > 0;
        }

        public IEnumerable<SM> GetAll()
        {
            return db.GetTable<SM>().ToList();
        }

        public LoadResult GetAll(DataSourceLoadOptionsBase loadOptions)
        {
            return DataSourceLoader.Load(db.GetTable<SM>(), loadOptions);
        }

        public SM GetBySifra(int sifra)
        {
            return db.GetTable<SM>().Where(f => f.SIFRA == sifra).FirstOrDefault();
        }

        public int GetNextSifra()
        {
            int? maxSifra = db.GetTable<SM>().Max(m => m.SIFRA);

            if (maxSifra.HasValue)
                maxSifra = maxSifra.Value + 1;

            return maxSifra.GetValueOrDefault(1);
        }

        public bool Insert(SM item)
        {
            item.SIFRA = GetNextSifra();
            return db.Insert<SM>(item) > 0;
        }

        public bool Update(SM item)
        {
            return db.Update<SM>(item) > 0;
        }
    }
}
