using System;
using System.Collections.Generic;
using System.Linq;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using LinqToDB;
using LinqToDB.Data;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository

{
    public class NarociloKupcaRepository : BaseSqlRepository, INarociloKupcaRepository
    {

        public NarociloKupcaRepository(ISqlConnectionProvider context) : base(context)
        {

        }

        public bool Update(FA_NAR_KUPCI item)
        {
            return db.Update<FA_NAR_KUPCI>(item) > 0;
        }

        public bool Insert(FA_NAR_KUPCI item)
        {
            return db.Insert<FA_NAR_KUPCI>(item) > 0;
        }

        public bool Update(FA_NAR_KUPCI_KNJ item)
        {
            return db.Update<FA_NAR_KUPCI_KNJ>(item) > 0;
        }

        public bool Insert(FA_NAR_KUPCI_KNJ item)
        {
            return db.Insert<FA_NAR_KUPCI_KNJ>(item) > 0;
        }


        /*public IList<FA_NAR_KUPCI> Get(DateTime odDatuma, int partnerSifra) {
            var q = dbContext.Db.GetTable<FA_NAR_KUPCI>

        }*/

        /// <summary>
        /// Vrne naslednjo �tevilko naro�ilo kupca glede na parametre
        /// </summary>
        /// <param name="year"></param>
        /// <param name="numberLength"></param>
        /// <param name="fromNumber"></param>
        /// <param name="toNumber"></param>
        /// <returns></returns>
        public string GetNextNumber(int year, int numberLength, int fromNumber, int toNumber)
        {

            try
            {
                string result = fromNumber.ToString();

                if (fromNumber <= 0)
                    fromNumber = 1;

                if (toNumber <= 0)
                    toNumber = 99999999;

                string fromNumberStr = fromNumber.ToString();
                string toNumberStr = toNumber.ToString();
                string toNumberMaxStr = toNumber.ToString();
                toNumberMaxStr = toNumberMaxStr.PadLeft(numberLength, '9');
                if (toNumberStr != toNumberMaxStr)
                {
                    toNumberStr = toNumberMaxStr;
                }

                fromNumberStr = fromNumberStr.PadLeft(numberLength, '0');
                toNumberStr = toNumberStr.PadLeft(numberLength, '0');
                result = fromNumberStr;

                var q = from n in db.GetTable<FA_NAR_KUPCI>()
                        where n.LETO == year && n.STEVILKA.Trim().CompareTo(fromNumberStr) >= 0 && n.STEVILKA.Trim().CompareTo(toNumberStr) <= 0
                        && n.STEVILKA.Trim().Length <= numberLength
                        orderby n.LETO, n.STEVILKA descending
                        select n.STEVILKA;

                var numberResult = q.FirstOrDefault();

                if (!string.IsNullOrEmpty(numberResult))
                {
                    if (Int32.TryParse(numberResult, out int numberInt))
                        numberInt++;
                    result = numberInt.ToString().PadLeft(numberLength, '0');
                }

                return result;

            }
            catch (Exception)
            {
                return "";
            }

        }
        /// <summary>
        /// Vrne �tevilo dnevnih "Web B2B" naro�il
        /// </summary>
        /// <param name="partnerSifra"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public int NumberOfDailyWebOrders(int partnerSifra, DateTime date)
        {
            var q = from n in db.GetTable<FA_NAR_KUPCI>()
                    where n.PARTNER == partnerSifra && n.SPLET_STATUS > 0 && n.DATUM == date
                    select n.STEVILKA;
            return q.Count();
        }

        public IList<FA_NAR_KUPCI_GLAVA> GetNarocilaKupca(int partnerSifra)
        {
            var q = from n in db.GetTable<FA_NAR_KUPCI_GLAVA>()
                    where n.PARTNER == partnerSifra
                    select n;
            return q.ToList();
        }

        public LoadResult GetNarocilaKupca(int partnerSifra, DataSourceLoadOptionsBase loadOptions)
        {
            var q = from n in db.GetTable<FA_NAR_KUPCI_GLAVA>()
                    where n.PARTNER == partnerSifra
                    select n;
            return DataSourceLoader.Load(q, loadOptions);
        }

        public IList<FA_NAR_KUPCI_KNJ_POSTAVKE> GetNarocilaKupcaKnjizbe(string stevilka, int leto)
        {
            var q = from n in db.GetTable<FA_NAR_KUPCI_KNJ_POSTAVKE>().LoadWith(j => j.ARTIKEL)
                    from dk in db.GetTable<FA_DOBAVNICA_KNJIZBA>().LeftJoin(j => j.NAR_KUP_LETO == n.LETO && j.NAR_KUP_STEVILKA.Trim() == n.STEVILKA.Trim() && j.NAR_KUP_ZS == n.ZS)
                    where n.STEVILKA.Trim() == stevilka && n.LETO == leto
                    select new
                    {
                        POSTAVKA = n,
                        KOLICINA = dk.KOLICINA
                    };

            var q1 = from n in q
                     group n by new { n.POSTAVKA } into g
                     select new FA_NAR_KUPCI_KNJ_POSTAVKE
                     {
                         AUTO_ID = g.Key.POSTAVKA.AUTO_ID,
                         STEVILKA = g.Key.POSTAVKA.STEVILKA,
                         LETO = g.Key.POSTAVKA.LETO,
                         PREDVID_DOBAVA = g.Key.POSTAVKA.PREDVID_DOBAVA,
                         DOBAVLJENA_KOLICINA = g.Sum(k => k.KOLICINA),
                         SKLADISCE = g.Key.POSTAVKA.SKLADISCE,
                         DATUM = g.Key.POSTAVKA.DATUM,
                         ZS = g.Key.POSTAVKA.ZS,
                         SIFRA = g.Key.POSTAVKA.SIFRA,
                         NAZIV = g.Key.POSTAVKA.NAZIV,
                         KOLICINA = g.Key.POSTAVKA.KOLICINA,
                         KOLICINA_POTRJENA = g.Key.POSTAVKA.KOLICINA_POTRJENA,
                         OPIS = g.Key.POSTAVKA.OPIS,
                         STOPNJA_DDV = g.Key.POSTAVKA.STOPNJA_DDV,
                         PRODAJNA_CENA = g.Key.POSTAVKA.PRODAJNA_CENA,
                         PRODAJNA_VREDNOST = g.Key.POSTAVKA.PRODAJNA_VREDNOST,
                         NETO_CENA = g.Key.POSTAVKA.NETO_CENA,
                         PRODAJNA_CENA_Z_DDV = g.Key.POSTAVKA.PRODAJNA_CENA_Z_DDV,
                         PRODAJNA_VREDNOST_Z_DDV = g.Key.POSTAVKA.PRODAJNA_VREDNOST_Z_DDV,
                         CENA = g.Key.POSTAVKA.CENA,
                         VREDNOST = g.Key.POSTAVKA.VREDNOST,
                         CENA_Z_DDV = g.Key.POSTAVKA.CENA_Z_DDV,
                         VREDNOST_Z_DDV = g.Key.POSTAVKA.VREDNOST_Z_DDV,
                         RABAT1 = g.Key.POSTAVKA.RABAT1,
                         RABAT_V_ZNESKU = g.Key.POSTAVKA.RABAT_V_ZNESKU,
                         DATUM_VELJAVNOSTI = g.Key.POSTAVKA.DATUM_VELJAVNOSTI,
                         ARTIKEL = g.Key.POSTAVKA.ARTIKEL
                     };
            return q1.ToList();
        }

        public LoadResult GetNarocilaKupcaKnjizbe(string stevilka, int leto, DataSourceLoadOptionsBase loadOptions)
        {
            return DataSourceLoader.Load(GetNarocilaKupcaKnjizbe(stevilka, leto), loadOptions);
        }
    }
}
