using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using LinqToDB;
using System.Collections.Generic;
using System.Linq;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository
{
    public class PartnerRepository : BaseSqlRepository, IPartnerRepository
    {
        public PartnerRepository(ISqlConnectionProvider context) : base(context)
        {
        }

        public IEnumerable<PARTNER> GetAllPartners()
        {
            var q = from k in db.GetTable<PARTNER>()
                    orderby k.SIFRA
                    select k;

            return q.ToList();
        }

        public IEnumerable<PARTNER_BASE> GetAllPartnersWithBaseFields()
        {
            var q = from k in db.GetTable<PARTNER_BASE>()
                    orderby k.SIFRA
                    select k;

            return q.ToList();
        }

        public LoadResult GetAllPartners(DataSourceLoadOptionsBase loadOptions)
        {
            var q = from k in db.GetTable<PARTNER>()
                    orderby k.SIFRA
                    select k;

            return DataSourceLoader.Load(q, loadOptions);
        }

        public LoadResult GetAllPartnersWithBaseFields(DataSourceLoadOptionsBase loadOptions)
        {
            var q = from k in db.GetTable<PARTNER_BASE>()
                    orderby k.SIFRA
                    select new
                    {
                        k.SIFRA,//opcijsko SIFRA = k.SIFRA.ToString();
                        k.NAZIV,
                        k.NAZIV2,
                        k.NASLOV,
                        k.POSTA,
                        k.DAVCNA,
                        k.DAVCNIZAVEZANEC,
                        k.ZIRORACUN
                    };

            return DataSourceLoader.Load(q, loadOptions);
        }

        public PARTNER GetPartnerBySifra(int sifra)
        {
            var q = from k in db.GetTable<PARTNER>()
                    where k.SIFRA == sifra
                    select k;

            return q.FirstOrDefault();
        }

        public PARTNER_BASE GetPartnerBySifraWithBaseFields(int sifra)
        {
            var q = from k in db.GetTable<PARTNER_BASE>()
                    where k.SIFRA == sifra
                    select k;

            return q.FirstOrDefault();
        }

        public PARTNER_BASE GetPartnerByUsername(string username)
        {
            var q = from k in db.GetTable<PARTNER_BASE>()
                    where k.UPORABNIK == username
                    select k;

            return q.FirstOrDefault();
        }

        public IEnumerable<FAPARTNERBLAGOVNA> GetPartnerBlagovneSkupine(int sifra)
        {
            var q = from p in db.GetTable<FAPARTNERBLAGOVNA>()
                    join b in db.GetTable<FABLAGOVNA>() on p.BLAGOVNA equals b.SIFRA
                    where p.PARTNER == sifra && !string.IsNullOrEmpty(b.NAZIV.Trim())
                    orderby b.NAZIV
                    select new FAPARTNERBLAGOVNA
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        BLAGOVNA = p.BLAGOVNA,
                        BLAGOVNA_NAZIV = b.NAZIV
                    };

            return q.ToList();
        }

        public IEnumerable<FAPARTNERNADGRUPA> GetPartnerNadGrupe(int sifra)
        {
            var q = from p in db.GetTable<FAPARTNERNADGRUPA>()
                    join g in db.GetTable<FANADGRUPA>() on p.NADGRUPA equals g.SIFRA
                    where p.PARTNER == sifra && !string.IsNullOrEmpty(g.NAZIV)
                    orderby g.NAZIV
                    select new FAPARTNERNADGRUPA
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        NADGRUPA = p.NADGRUPA,
                        NADGRUPA_NAZIV = g.NAZIV
                    };

            return q.ToList();
        }

        public IEnumerable<FAPARTNERGRUPA> GetPartnerGrupe(int sifra)
        {
            var q = from p in db.GetTable<FAPARTNERGRUPA>()
                    join g in db.GetTable<FAGRUPA>() on p.GRUPA equals g.SIFRA
                    where p.PARTNER == sifra && !string.IsNullOrEmpty(g.NAZIV)
                    orderby g.NAZIV
                    select new FAPARTNERGRUPA
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        GRUPA = p.GRUPA,
                        GRUPA_NAZIV = g.NAZIV
                    };
            return q.ToList();
        }

        public IEnumerable<FAPARTNERSKLADISCE> GetPartnerSkladisca(int sifra)
        {
            var q = from p in db.GetTable<FAPARTNERSKLADISCE>()
                    join s in db.GetTable<FASKLADISCE>() on p.SKLADISCE equals s.SIFRA
                    where p.PARTNER == sifra && !string.IsNullOrEmpty(s.NAZIV)
                    orderby s.NAZIV
                    select new FAPARTNERSKLADISCE
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        SKLADISCE = p.SKLADISCE,
                        SKLADISCE_NAZIV = s.NAZIV
                    };
            return q.ToList();
        }

        public LoadResult GetPartnerBlagovneSkupine(DataSourceLoadOptionsBase loadOptions)
        {
            var q = from p in db.GetTable<FAPARTNERBLAGOVNA>()
                    join b in db.GetTable<FABLAGOVNA>() on p.BLAGOVNA equals b.SIFRA
                    where !string.IsNullOrEmpty(b.NAZIV)
                    orderby b.NAZIV
                    select new FAPARTNERBLAGOVNA
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        BLAGOVNA = p.BLAGOVNA,
                        BLAGOVNA_NAZIV = b.NAZIV
                    };

            return DataSourceLoader.Load(q, loadOptions);
        }

        public LoadResult GetPartnerNadGrupe(DataSourceLoadOptionsBase loadOptions)
        {
            var q = from p in db.GetTable<FAPARTNERNADGRUPA>()
                    join g in db.GetTable<FANADGRUPA>() on p.NADGRUPA equals g.SIFRA
                    where !string.IsNullOrEmpty(g.NAZIV)
                    orderby g.NAZIV
                    select new FAPARTNERNADGRUPA
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        NADGRUPA = p.NADGRUPA,
                        NADGRUPA_NAZIV = g.NAZIV
                    };
            return DataSourceLoader.Load(q, loadOptions);
        }

        public LoadResult GetPartnerGrupe(DataSourceLoadOptionsBase loadOptions)
        {
            var q = from p in db.GetTable<FAPARTNERGRUPA>()
                    join g in db.GetTable<FAGRUPA>() on p.GRUPA equals g.SIFRA
                    where !string.IsNullOrEmpty(g.NAZIV)
                    orderby g.NAZIV
                    select new FAPARTNERGRUPA
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        GRUPA = p.GRUPA,
                        GRUPA_NAZIV = g.NAZIV
                    };

            return DataSourceLoader.Load(q, loadOptions);
        }

        public LoadResult GetPartnerSkladisca(DataSourceLoadOptionsBase loadOptions)
        {
            var q = from p in db.GetTable<FAPARTNERSKLADISCE>()
                    join s in db.GetTable<FASKLADISCE>() on p.SKLADISCE equals s.SIFRA
                    where !string.IsNullOrEmpty(s.NAZIV)
                    orderby s.NAZIV
                    select new FAPARTNERSKLADISCE
                    {
                        AUTOID = p.AUTOID,
                        PARTNER = p.PARTNER,
                        SKLADISCE = p.SKLADISCE,
                        SKLADISCE_NAZIV = s.NAZIV
                    };
            return DataSourceLoader.Load(q, loadOptions);
        }
    }
}