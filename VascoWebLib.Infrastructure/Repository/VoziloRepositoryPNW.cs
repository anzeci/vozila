using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.Repository;

namespace VascoWebLib.Infrastructure.Repository
{
    public class VoziloRepositoryPNW : BaseSqlRepository, IVoziloRepositoryPNW
    {
        public VoziloRepositoryPNW(ISqlConnectionProvider context) : base(context)
        {

        }

        public bool Delete(int sifra)
        {
            return db.GetTable<PNW_VOZILO>().Delete(f => f.SIFRA == sifra) > 0;
        }

        public IEnumerable<PNW_VOZILO> GetAll()
        {
            return db.GetTable<PNW_VOZILO>().ToList();
        }

        public LoadResult GetAll(DataSourceLoadOptionsBase loadOptions)
        {
            return DataSourceLoader.Load(db.GetTable<PNW_VOZILO>(), loadOptions);
        }

        public PNW_VOZILO GetBySifra(int sifra)
        {
            return db.GetTable<PNW_VOZILO>().Where(f => f.SIFRA == sifra).FirstOrDefault();
        }

        public int GetNextSifra()
        {
            int? maxSifra = db.GetTable<PNW_VOZILO>().Max(m => m.SIFRA);

            if (maxSifra.HasValue)
                maxSifra = maxSifra.Value + 1;

            return maxSifra.GetValueOrDefault(1);
        }

        public bool Insert(PNW_VOZILO item)
        {
            item.SIFRA = GetNextSifra();
            return db.Insert<PNW_VOZILO>(item) > 0;
        }

        public bool Update(PNW_VOZILO item)
        {
            return db.Update<PNW_VOZILO>(item) > 0;
        }
    }
}
