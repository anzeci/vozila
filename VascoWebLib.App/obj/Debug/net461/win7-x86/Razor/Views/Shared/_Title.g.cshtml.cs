#pragma checksum "C:\BitBucket\vozila_new\VascoWebLib.App\Views\Shared\_Title.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6b301d88b7ca141570beb224504f0f3264dbce60"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__Title), @"mvc.1.0.view", @"/Views/Shared/_Title.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_Title.cshtml", typeof(AspNetCore.Views_Shared__Title))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebApp.Infrastructure.Model.View;

#line default
#line hidden
#line 2 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebApp.Infrastructure.Model.Filter;

#line default
#line hidden
#line 3 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebLib.Core.Entity;

#line default
#line hidden
#line 4 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebLib.App.Helpers;

#line default
#line hidden
#line 5 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebApp.Infrastructure;

#line default
#line hidden
#line 6 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebLib.Core.UI.Helpers;

#line default
#line hidden
#line 7 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebLib.Core.UI.Model.View;

#line default
#line hidden
#line 8 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebLib.Core.UI.Interfaces.Menu;

#line default
#line hidden
#line 9 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using VascoWebLib.Core.UI.Model.Menu;

#line default
#line hidden
#line 11 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6b301d88b7ca141570beb224504f0f3264dbce60", @"/Views/Shared/_Title.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6da0ae9e644184cb6739464a5d351c462cd4388c", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__Title : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<string>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\Shared\_Title.cshtml"
  
    ViewBag.Title = Model;

#line default
#line hidden
            BeginContext(50, 28, true);
            WriteLiteral("\r\n<h6 style=\"color:#346392\">");
            EndContext();
            BeginContext(79, 5, false);
#line 6 "C:\BitBucket\vozila_new\VascoWebLib.App\Views\Shared\_Title.cshtml"
                     Write(Model);

#line default
#line hidden
            EndContext();
            BeginContext(84, 5, true);
            WriteLiteral("</h6>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<string> Html { get; private set; }
    }
}
#pragma warning restore 1591
