using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Model.View;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib_App.Controllers
{
    public class NarociloController : BaseVascoController
    {
        public IActionResult ListViewPage()
        {
            //TODO ali ima uporabnik pravice
            var delavecListViewModel = new DelavecListView();
            delavecListViewModel.SetAllowInsertUpdateDelete(true);
            var pageModel = new PageViewModel(delavecListViewModel);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        /// <summary>
        /// JSON Podatki za ListView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult ListViewData(DataSourceLoadOptions options, [FromServices] INarociloKupcaRepository narociloRep)
        {
            //TODO ali ima uporabnik pravice
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(narociloRep.GetNarocilaKupca(1, options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }
    }
}