using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Model.View;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib_App.Controllers
{
    public class AnzeTestController : BaseVascoController
    {
        public IActionResult ListViewPage()
        {
            var drzavaListViewModel = new DrzavaListView();
            var pageModel = new PageViewModel(drzavaListViewModel);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        /// <summary>
        /// JSON Podatki za ListView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult ListViewData(DataSourceLoadOptions options, [FromServices] IDrzavaRepository drzavaRep)
        {
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(drzavaRep.GetAll(options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }

       
    }
}