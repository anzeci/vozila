using Microsoft.AspNetCore.Mvc;

namespace VascoWebLib_App.Controllers
{
    [AutoValidateAntiforgeryToken]
    public abstract class BaseVascoController : Controller
    {
        public BaseVascoController()
        {
        }

        protected void SuccessMessage(string textMessage)
        {
            TempData["LastSuccessMessage"] = textMessage;
        }

        protected void ErrorMessage(string textMessage)
        {
            TempData["LastErrorMessage"] = textMessage;
        }
    }
}