using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Model.View;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib_App.Controllers
{
    public class SkupinaController : BaseVascoController
    {
        public IActionResult ListViewPage()
        {
            var pageModel = new PageViewModel(new SkupinaListView());
            return View(ViewStrings.PageView.Page, pageModel);
        }

        /// <summary>
        /// JSON Podatki za ListView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult ListViewData(DataSourceLoadOptions options, [FromServices] ISkupinaRepositoryPNW skupinaRep)
        {
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(skupinaRep.GetAll(options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }

        public IActionResult DetailViewDialogView(int sifra, string listViewID, [FromServices] ISkupinaRepositoryPNW rep)
        {
            //FORM DATA
            var formData = rep.GetBySifra(sifra);
            //DETAIL_VIEW
            var detailView = new SkupinaDetailView
            {
                FormData = formData,
                EditState = ViewEditState.View,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            detailView.SetReadOnly();
            //DIALOG
            var dialogModel = new SkupinaViewDialog(detailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogEdit(int sifra, string listViewID, [FromServices]  ISkupinaRepositoryPNW rep)
        {
            //FORM DATA
            var formData = rep.GetBySifra(sifra);
            //DETAIL_VIEW
            var detailView = new SkupinaDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Edit,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            //DIALOG
            var dialogModel = new SkupinaEditDialog(detailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogInsert(string listViewID, [FromServices] ISkupinaRepositoryPNW rep)
        {
            string title = "VNOS SKUPINE";
            //FORM DATA
            var formData = new PNW_SKUPINA();
            formData.SIFRA = rep.GetNextSifra();
            //DETAIL_VIEW
            var detailView = new SkupinaDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Insert,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = title
            };
            //DIALOG
            var dialogModel = new SkupinaInsertDialog(detailView)
            {
                Title = title
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditData(PNW_SKUPINA data, [FromServices] ISkupinaRepositoryPNW rep)
        {
            //TODO ali ima uporabnik pravice za urejanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (rep.Update(data))
            {
                return Json(WebResponseModel.ResponseOk("Skupina uspešno ažurirana!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InsertData(PNW_SKUPINA data, [FromServices] ISkupinaRepositoryPNW rep)
        {
            //TODO ali ima uporabnik pravice za vnašanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (rep.Insert(data))
            {
                return Json(WebResponseModel.ResponseOk("Skupina uspešno shranjena!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteData(int sifra, string listViewID, [FromServices] ISkupinaRepositoryPNW rep)
        {
            //TODO ali ima uporabnik pravice za brisanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (rep.Delete(sifra))
            {
                return Json(WebResponseModel.ResponseOk("Skupina uspešno brisana!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }
    }
}