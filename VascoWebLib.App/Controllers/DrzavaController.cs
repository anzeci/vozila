using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Model.View;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib_App.Controllers
{
    public class DrzavaController : BaseVascoController
    {
        public IActionResult ListViewPage()
        {
            var drzavaListViewModel = new DrzavaListView();
            var pageModel = new PageViewModel(drzavaListViewModel);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        /// <summary>
        /// JSON Podatki za ListView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult ListViewData(DataSourceLoadOptions options, [FromServices] IDrzavaRepository drzavaRep)
        {
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(drzavaRep.GetAll(options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }

        public IActionResult DetailViewDialogView(int sifra, string listViewID, [FromServices] IDrzavaRepository drzavaRep)
        {
            //FORM DATA
            var formData = drzavaRep.GetBySifra(sifra);
            //DETAIL_VIEW
            var detailView = new DrzavaDetailView
            {
                FormData = formData,
                EditState = ViewEditState.View,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            detailView.SetReadOnly();
            //DIALOG
            var dialogModel = new DrzavaViewDialog(detailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogEdit(int sifra, string listViewID, [FromServices] IDrzavaRepository drzavaRep)
        {
            //FORM DATA
            var formData = drzavaRep.GetBySifra(sifra);
            //DETAIL_VIEW
            var detailView = new DrzavaDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Edit,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            //DIALOG
            var dialogModel = new DrzavaEditDialog(detailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogInsert(string listViewID, [FromServices] IDrzavaRepository drzavaRep)
        {
            string title = "VNOS DRŽAVE";
            //FORM DATA
            var formData = new DRZAVA();
            formData.SIFRA = drzavaRep.GetNextSifra();
            //DETAIL_VIEW
            var detailView = new DrzavaDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Insert,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = title
            };
            //DIALOG
            var dialogModel = new DrzavaInsertDialog(detailView)
            {
                Title = title
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditData(DRZAVA data, [FromServices] IDrzavaRepository drzavaRep)
        {
            //TODO ali ima uporabnik pravice za urejanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (drzavaRep.Update(data))
            {
                return Json(WebResponseModel.ResponseOk("Država uspešno ažurirana!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InsertData(DRZAVA data, [FromServices] IDrzavaRepository drzavaRep)
        {
            //TODO ali ima uporabnik pravice za vnašanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (drzavaRep.Insert(data))
            {
                return Json(WebResponseModel.ResponseOk("Država uspešno shranjena!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteData(int sifra, string listViewID, [FromServices] IDrzavaRepository drzavaRep)
        {
            //TODO ali ima uporabnik pravice za brisanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (drzavaRep.Delete(sifra))
            {
                return Json(WebResponseModel.ResponseOk("Delavec uspešno brisan!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }
    }
}