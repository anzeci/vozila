using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Model.View;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib_App.Controllers
{
    public class SMController : BaseVascoController
    {
        public IActionResult ListViewPage()
        {
            var pageModel = new PageViewModel(new SMListView());
            return View(ViewStrings.PageView.Page, pageModel);
        }

        /// <summary>
        /// JSON Podatki za ListView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult ListViewData(DataSourceLoadOptions options, [FromServices] ISMRepository smRep)
        {
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(smRep.GetAll(options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }

        /// <summary>
        /// SifEdit selection dialog
        /// </summary>
        /// <param name="ViewID"></param>
        /// <returns></returns>
        public IActionResult SifEditViewSelectionDialog(string ViewID)
        {
            var listView = new SMListView();
            listView.ParentViewID = ViewID;
            var listDialog = new SMListViewSelectionDialog(listView);
            return ViewComponent(ViewStrings.Components.DialogView, listDialog);
        }

        /// <summary>
        /// JSON Podatki za SifEditView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult SifEditViewData(DataSourceLoadOptions options, [FromServices] ISMRepository rep)
        {
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(rep.GetAll(options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }

        public IActionResult DetailViewDialogView(int sifra, string listViewID, [FromServices] ISMRepository rep)
        {
            //FORM DATA
            var formData = rep.GetBySifra(sifra);
            //DETAIL_VIEW
            var detailView = new SMDetailView
            {
                FormData = formData,
                EditState = ViewEditState.View,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            detailView.SetReadOnly();
            //DIALOG
            var dialogModel = new SMViewDialog(detailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogEdit(int sifra, string listViewID, [FromServices]  ISMRepository rep)
        {
            //FORM DATA
            var formData = rep.GetBySifra(sifra);
            //DETAIL_VIEW
            var detailView = new SMDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Edit,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            //DIALOG
            var dialogModel = new SMEditDialog(detailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogInsert(string listViewID, [FromServices] ISMRepository rep)
        {
            string title = "VNOS SM";
            //FORM DATA
            var formData = new SM();
            formData.SIFRA = rep.GetNextSifra();
            //DETAIL_VIEW
            var detailView = new SMDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Insert,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = title
            };
            //DIALOG
            var dialogModel = new SMInsertDialog(detailView)
            {
                Title = title
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditData(SM data, [FromServices] ISMRepository rep)
        {
            //TODO ali ima uporabnik pravice za urejanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (rep.Update(data))
            {
                return Json(WebResponseModel.ResponseOk("SM uspešno ažuriran!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InsertData(SM data, [FromServices] ISMRepository rep)
        {
            //TODO ali ima uporabnik pravice za vnašanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (rep.Insert(data))
            {
                return Json(WebResponseModel.ResponseOk("SM uspešno shranjeno!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteData(int sifra, string listViewID, [FromServices] ISMRepository rep)
        {
            //TODO ali ima uporabnik pravice za brisanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (rep.Delete(sifra))
            {
                return Json(WebResponseModel.ResponseOk("SM uspešno brisano!"));
            }

            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }
    }
}