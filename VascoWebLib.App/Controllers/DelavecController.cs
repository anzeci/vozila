using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Model.Filter;
using VascoWebApp.Infrastructure.Model.View;
using VascoWebLib.Core.Entity;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib_App.Controllers
{
    public class DelavecController : BaseVascoController
    {
        public IActionResult ListViewPage()
        {
            //TODO ali ima uporabnik pravice
            var delavecListViewModel = new DelavecListView();
            delavecListViewModel.SetAllowInsertUpdateDelete(true);
            var pageModel = new PageViewModel(delavecListViewModel);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        public IActionResult ListViewPageNoDialog()
        {
            //TODO ali ima uporabnik pravice
            var delavecListViewModel = new DelavecListView();
            delavecListViewModel.UsePages();
            delavecListViewModel.SetAllowInsertUpdateDelete(true);
            var pageModel = new PageViewModel(delavecListViewModel);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        /// <summary>
        /// JSON Podatki za ListView
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IActionResult ListViewData(DataSourceLoadOptions options, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            //podatke v JSON
            string serializedData = JsonConvert.SerializeObject(delavecRep.GetAll(options));
            //pošljemo rezultat
            return Content(serializedData, "application/json");
        }

        public IActionResult FilterDetailViewDialog([FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //FORM DATA
            var formData = new DelavecFilterModel();
            //DETAIL_VIEW
            var delavecFilterDetailView = new DelavecFilterDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Edit,
                Title = "Delavec - filter"
            };
            //DIALOG
            var dialogModel = new DelavecFilterDialog(delavecFilterDetailView);

            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewDialogView(int sifra, string listViewID, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            //FORM DATA
            var formData = delavecRep.GetBySifra(sifra);
            //DETAIL_VIEW
            var delavecDetailView = new DelavecDetailView
            {
                FormData = formData,
                EditState = ViewEditState.View,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };

            //DIALOG
            var dialogModel = new DelavecViewDialog(delavecDetailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewPageView(int sifra, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            //FORM DATA
            var formData = delavecRep.GetBySifra(sifra);
            //DETAIL_VIEW
            var delavecDetailView = new DelavecDetailView
            {
                FormData = formData,
                EditState = ViewEditState.View,
                Title = formData.ToString()
            };
            //PAGE
            var pageModel = new DelavecViewPage(delavecDetailView);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        public IActionResult DetailViewDialogEdit(int sifra, string listViewID, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            //FORM DATA
            var formData = delavecRep.GetBySifra(sifra);
            //DETAIL_VIEW
            var delavecDetailView = new DelavecDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Edit,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = formData.ToString()
            };
            //DIALOG
            var dialogModel = new DelavecEditDialog(delavecDetailView)
            {
                Title = formData.ToString()
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewPageEdit(int sifra, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            //FORM DATA
            var formData = delavecRep.GetBySifra(sifra);
            //DETAIL_VIEW
            var delavecDetailView = new DelavecDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Edit,
                Title = formData.ToString()
            };
            //PAGE
            var pageModel = new DelavecEditPage(delavecDetailView);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        public IActionResult DetailViewDialogInsert(string listViewID, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            string title = "VNOS DELAVCA";
            //FORM DATA
            var formData = new PNW_DELAVEC();
            formData.SIFRA = delavecRep.GetNextSifra();
            //DETAIL_VIEW
            var delavecDetailView = new DelavecDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Insert,
                ParentViewID = listViewID,
                ParentViewType = ViewType.ListView,
                Title = title
            };
            //DIALOG
            var dialogModel = new DelavecInsertDialog(delavecDetailView)
            {
                Title = title
            };
            return ViewComponent(ViewStrings.Components.DialogView, dialogModel);
        }

        public IActionResult DetailViewPageInsert(int sifra, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice
            string title = "VNOS DELAVCA";
            //FORM DATA
            var formData = new PNW_DELAVEC();
            formData.SIFRA = delavecRep.GetNextSifra();
            //DETAIL_VIEW
            var delavecDetailView = new DelavecDetailView
            {
                FormData = formData,
                EditState = ViewEditState.Insert,
                Title = title
            };
            //PAGE
            var pageModel = new DelavecInsertPage(delavecDetailView);
            return View(ViewStrings.PageView.Page, pageModel);
        }

        [HttpPost]
        public IActionResult EditData(PNW_DELAVEC data, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice za urejanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (delavecRep.Update(data))
            {
                var msg = "Delavec uspešno ažuriran!";
                return Json(WebResponseModel.ResponseOk(msg));
            }
            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        public IActionResult InsertData(PNW_DELAVEC data, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice za vnašanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (delavecRep.Insert(data))
            {
                var msg = "Delavec uspešno vnešen!";
                return Json(WebResponseModel.ResponseOk(msg));
            }
            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }

        [HttpPost]
        public IActionResult DeleteData(int sifra, string listViewID, [FromServices] IDelavecRepositoryPNW delavecRep)
        {
            //TODO ali ima uporabnik pravice za brisanje
            //TODO model validation (ali so vsi podatki vneseni)
            if (delavecRep.Delete(sifra))
            {
                var msg = "Delavec uspešno brisan!";
                return Json(WebResponseModel.ResponseOk(msg));
            }
            return Json(WebResponseModel.ResponseError("TODO (ni pravic, napaka.....)"));
        }
    }
}