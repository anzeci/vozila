
var VascoUiLib = function () {

    var msgPosition = { my: 'center', at: 'bottom', of: window, offset: '0 -100' };

    function msgOptions(msgText) {
        return { position: this.msgPosition, message: msgText };
    }
    /*
    WORK INDICATOR - SHOW
    */
    function showBusy(title) {
        this.hideBusy();
        var $div = $('<div />').appendTo('body');
        $div.attr('id', 'vascoLoadIndicator');

        $div.dxLoadPanel({
            shadingColor: "rgba(0,0,0,0.4)",
            visible: true,
            showIndicator: true,
            showPane: true,
            shading: true,
            message: title,
            closeOnOutsideClick: false
        });
    }
    /*
        WORK INDICATOR - HIDE
    */
    function hideBusy() {
        $('#vascoLoadIndicator').remove();
    }

    function showToast(message, type, displayTimeMS) {
        if (!displayTimeMS)
            displayTimeMS = 600;

        $('<div />').appendTo('body').dxToast({
            message: message,
            type: type,
            displayTime: displayTimeMS,
            visible: true,
            position: msgPosition,
            onHidden: function (e) {
                e.element.remove();
            }
        });
    }

    function showInfo(message, displayTimeMS) {
        showToast(message, "info", displayTimeMS);
    }

    function showWarning(message, displayTimeMS) {
        showToast(message, "warning", displayTimeMS);
    }

    function showError(message, displayTimeMS) {
        showToast(message, "error", displayTimeMS);
    }

    function showSuccess(message, displayTimeMS) {
        showToast(message, "success", displayTimeMS);
    }

    function showMessage(message, title, okCallback) {
        DevExpress.ui.dialog.alert(message, title).then(function () {
            if (okCallback)
                okCallback();
        }
        );
    }

    function showYesNo(message, title, yesCallback, noCallback) {
        var result = DevExpress.ui.dialog.confirm(message, title);
        result.done(function (dialogResult) {
            if (dialogResult && yesCallback)
                yesCallback();
            else if (noCallback)
                noCallback();
        });
    }

    /**
     * 
     * @param {any} message
     * @param {any} element String | DOM Node | jQuery
     * @param {any} autoCloseAfterMS Po koliko časa avtomatsko zapre prikaz (default 2000ms)
     */

    function showPopoverAtElement(message, element, autoCloseAfterMS) {

        var width = "auto";

        if (message && message.length > 15) {
            width = "300px";
        }

        var popOver = $('<div />').appendTo('body').dxPopover({
            contentTemplate: function () { return "<div>" + message + "</div>"; },
            position: "top",
            width: width,
            popoverTimer: 0,
            onHidden: function (e) {
                e.element.remove();
            }
        });
        var popoverInstance = popOver.dxPopover('instance');
        popoverInstance.show(element);

        if (!autoCloseAfterMS)
            autoCloseAfterMS = 2000;

        if (autoCloseAfterMS && autoCloseAfterMS > 0) {
            setTimeout(function () {
                popoverInstance.hide();
            }, autoCloseAfterMS);
        }
    }

    /**
     * Shows DetailView (form dialog)
     * @param {string} controller Server side controller name
     * @param {string} action Server side action name
     * @param {object} requestParams Example: { Stevilka: 1, Leto: 2018} or { ID = '1234'} or { any }
     * @param {object} dialogResultCallback Opcijsko func callback: function (result) {} 
     */
    function showDialogView(controller, action, requestParams, dialogResultCallback) {
        Vasco.ui.showBusy("... pridobivam podatke ... ");
        Vasco.server.getData(controller, action, requestParams, function (resultData) {

            var dialogElement = $("<div/>").append(resultData).appendTo($("body"));

            if (dialogResultCallback && typeof dialogResultCallback === "function") {
                
                setTimeout(function () {
                    var dialogID = $(dialogElement).find("div[view = 'DialogView']")[0].id;
                    if (dialogID) {
                        var dialogInstance = $("#" + dialogID).dxPopup('instance');
                        if (dialogInstance) {
                            dialogInstance.option('vasco.dialogResult', dialogResultCallback);
                        }
                    }
                }, 10);
            };
            
        }, function (res) {
            //fail
        }, function () {
            //always
            Vasco.ui.hideBusy();
        });
    }

   

    /**
     * @param {string} controller Controller
     * @param {string} action Action
     * @param {object} queryParams Extra query params
     * @param {object} options options see bellow
     *   {
     *      confirmCallback : func(ListView),
     *      cancelCallback: func(ListView),
     *      title: string,
     *      confirmButtonText: string,
     *      errorCallback: func(errorInfo),
     *      selectionCallback: func(dataArray),
     *      selectionMode : 0 = none, 1 = Single, 2 = Multi
     *   }
     */
    function showListView(controller, action, queryParams, options) {
        Vasco.ui.showBusy("... pridobivam podatke ... ");

        var urlParams = '';
        if (queryParams) {
            urlParams = '?' + jQuery.param(queryParams);
        }

        $.ajax({
            type: "GET",
            url: Vasco.BaseUrl + '/' + controller + '/' + action + urlParams,
            success: function (response) {

                function BtnConfirm(e) {
                    if (options) {

                        if ($.isFunction(options.confirmCallback)) {
                            dialogInstance.ListView.selectedItems = dialogInstance.ListView.listView.getSelectedRowsData();
                            options.confirmCallback(dialogInstance.ListView);
                        }
                    }
                    setTimeout(function () { dialogInstance.hide(); }, 100);
                }

                try {
                    if (response) {

                        var title = "";
                        var confirmButtonText = "Potrdi";

                        if (options) {
                            if (options.title) {
                                title = options.title;
                            }
                            if (options.confirmButtonText) {
                                confirmButtonText = options.confirmButtonText;
                            }
                        }



                        var toolbarItems = [{
                            toolbar: 'bottom',
                            widget: 'dxButton',
                            options: {
                                text: confirmButtonText,
                                icon: 'todo',
                                type: 'success',
                                width: 200,
                                onClick: BtnConfirm
                            },
                            location: 'center'
                        }, {
                            toolbar: 'bottom',
                            widget: 'dxButton',
                            options: {
                                text: 'Prekliči',
                                icon: 'close',
                                type: 'danger',
                                width: 200,
                                onClick: function () {

                                    if (options) {
                                        if ($.isFunction(options.cancelCallback)) {
                                            options.cancelCallback(dialogInstance.ListView);
                                        }
                                    }
                                    dialogInstance.hide();
                                }
                            },
                            location: 'center'
                        }];

                        //SCROLLVIEW
                        var $divScrollView = $('<div />');
                        $divScrollView.attr('id', new DevExpress.data.Guid());
                        var scrollViewWidget = $divScrollView.dxScrollView({
                            scrollByContent: true,
                            scrollByThumb: true,
                            showScrollbar: true,
                            height: '100%'
                        }).dxScrollView("instance");
                        var scrollContentElement = scrollViewWidget.content();
                        $(response).appendTo(scrollContentElement);


                        //POPUP
                        popupOptions = {
                            width: '90%',
                            height: '90%',
                            showTitle: true,
                            title: title,
                            visible: true,
                            dragEnabled: true,
                            closeOnOutsideClick: false,
                            toolbarItems: toolbarItems,
                            contentTemplate: function () {
                                return $divScrollView;
                            },
                            onHidden: function (e) { e.element.remove(); }
                        };

                        var $divPopup = $('<div />').appendTo('body');
                        $divPopup.attr('id', new DevExpress.data.Guid());
                        var dialogInstance = $divPopup.dxPopup(popupOptions).dxPopup('instance');

                        //FIND LISTVIEW
                        var $listElement = $divPopup.find("[listview='DataGrid']");
                        if ($listElement && $listElement.length >= 1) {

                            var listInstance = $listElement.dxDataGrid('instance');
                            listInstance.ListView = ListView(dialogInstance, listInstance, options);
                            dialogInstance.ListView = listInstance.ListView;

                            /**
                            SELECTION MODE AND DBL CLICK HANDLER
                            */
                            if (options) {
                                if (options.selectionMode) {
                                    if (options.selectionMode === 1) {
                                        listInstance.option('selection.mode', 'single');
                                    } else if (options.selectionMode === 2) {
                                        listInstance.option('selection.mode', 'multiple');
                                        listInstance.option('selection.selectAllMode', 'allPages');
                                        listInstance.option('selection.showCheckBoxesMode', 'always');
                                    }

                                    listInstance.on('rowClick', function (e) {
                                        var component = e.component;
                                        prevClickTime = component.lastClickTime;
                                        component.lastClickTime = new Date();
                                        if (prevClickTime && component.lastClickTime - prevClickTime < 250) {
                                            clearTimeout(myVar);
                                            BtnConfirm(e);
                                        }
                                        else {
                                            myVar = setInterval(function () {
                                                clearTimeout(myVar);
                                                //Single click code
                                            }, 250);
                                        }
                                    });

                                }
                            }

                        }

                    }
                } catch (e) {
                    console.log(e);
                    Vasco.ui.hideBusy();
                }
                Vasco.ui.hideBusy();



            },
            error: function (error) {
                Vasco.ui.hideBusy();
                if (options) {
                    if ($.isFunction(options.errorCallback)) {
                        options.errorCallback(error);
                    }
                }
            }
        });
    }

    function ListView(popupInstance, listViewInstance, options) {
        return {
            popup: popupInstance,
            listView: listViewInstance,
            options: options,
            selectedItems: null
        };
    }

    var input_types;
    input_types = "input, select, button, textarea";

    function handleFocus(e, parentID) {

            var enter_key, form, input_array, move_direction, move_to, new_index, self, tab_index, tab_key;
            enter_key = 13;
            tab_key = 9;

        if (e.keyCode === tab_key || e.keyCode === enter_key) {

                var thisContext = e.target;
                self = $(thisContext);
  
                // some controls should react as designed when pressing enter
                if (e.keyCode === enter_key && (self.prop('type') === "submit" || self.prop('type') === "textarea")) {
                    return true;
                }

            form = self.parents("#" + parentID + ":eq(0)");
                // Sort by tab indexes if they exist
                tab_index = parseInt(self.attr('tabindex'));
                if (tab_index) {
                    input_array = form.find("[tabindex]").filter(':visible').sort(function (a, b) {
                        return parseInt($(a).attr('tabindex')) - parseInt($(b).attr('tabindex'));
                    });
                } else {
                    input_array = form.find(input_types).filter(':visible');
                }
                // reverse the direction if using shift
                move_direction = e.shiftKey ? -1 : 1;
                new_index = input_array.index(thisContext) + move_direction;

                // wrap around the controls
                if (new_index === input_array.length) {
                    new_index = 0;
                } else if (new_index === -1) {
                    new_index = input_array.length - 1;
                }

                move_to = input_array.eq(new_index);
                move_to.focus();
                move_to.select();
                return false;
            }
    }

    function enter_as_tab(parentID) {

        var fn = function (e) {
            var parentExist = $("#" + parentID).length > 0;
            if (parentExist) {
                return handleFocus(e, parentID);
            } else {
                $("body").off("keypress", fn);
                return true;
            }
        };

        window["enter_as_tab_" + parentID] = fn;
            
        return $("body").on("keypress", input_types, fn);
    };

    



    return {
        showBusy: showBusy,
        hideBusy: hideBusy,
        showInfo: showInfo,
        showPopoverAtElement: showPopoverAtElement,
        showWarning: showWarning,
        showError: showError,
        showSuccess: showSuccess,
        showMessage: showMessage,
        showYesNo: showYesNo,
        showDialogView: showDialogView,
        //showListView: showListView,
        enable_enter_as_tab: enter_as_tab,
        simulateTab: handleFocus 
    };
};
var Vasco = {
    ui: VascoUiLib(),
    BaseUrl: "",
    util: {
        toolbarParam: function (e) {
            var param = {};
            param.cancel = false;
            param.cancelReason = '';
            param.id = e.component.option('vasco.ID');
            param.tag = e.component.option('vasco.Tag');
            param.dialogResult = e.component.option('vasco.DialogResult');
            param.event = e;
            param.viewID = e.component.option('vasco.ViewID');
            param.parentViewID = e.component.option('vasco.ParentViewID');
            param.buttonCommand = e.component.option('vasco.ButtonCommand');
            param.controllerNameGet = e.component.option('vasco.ControllerNameGet');
            param.actionNameGet = e.component.option('vasco.ActionNameGet');
            param.controllerNamePost = e.component.option('vasco.ControllerNamePost');
            param.actionNamePost = e.component.option('vasco.ActionNamePost');
            param.controllerNameRedirect = e.component.option('vasco.ControllerNameRedirect');
            param.actionNameRedirect = e.component.option('vasco.ActionNameRedirect');
            return param;
        },
        objectExist: function (array, key, keyValue) {
            for (i = 0; i < array.length; i++) {
                if (array[i][key] === keyValue) {
                    return array[i][key];
                }
            }
            return null;
        },
        formatNumber: function (val, options) {
            return Globalize.formatNumber(val, options);
        },
        formatDecimal2: function (val, options) {
            return Vasco.util.formatNumber(val, { maximumFractionDigits: 2, minimumFractionDigits: 2 });
        },
        formatCurrency: function (val, options) {
            if (!options)
                options = 'EUR';
            return Globalize.formatCurrency(val, options);
        },
        lessOrEqualZero: function (value) {
            return value <= 0;
        },
        higherThanZero: function (value) {
            return value > 0;
        },
        formatDate: function (date) {
            var formatter = Globalize.dateFormatter({ date: "vasco" });
            return formatter(date);
        },
        formatDateTime: function (date) {
            var formatter = Globalize.dateFormatter({ date: "vasco", time: "vasco" });
            return formatter(date);
        },
        formatTime: function (date) {
            var formatter = Globalize.dateFormatter({ time: "vasco" });
            return formatter(date);
        },
        isFunction: function (fn) {
            return $.isFunction(fn);
        }
    },
    actionButton: {
        onInitialized: function (e) {
            console.log(e);
        }
    },
    listView: {
        onToolbarPreparing: function (e) {
            var gridInstance = e.component;

            if (e.component.option('vasco.searchPosition') && e.component.option('vasco.searchPosition') > 0) {
                var position = e.component.option('vasco.searchPosition');
                var toolbarItems = e.toolbarOptions.items;
                var searchPanel = $.grep(toolbarItems, function (item) {
                    return item.name === "searchPanel";
                })[0];

                if (searchPanel) {
                    switch (position) {
                        case 1:
                            searchPanel.location = "before";
                            searchPanel.sortIndex = 1;

                            var groupPanel = $.grep(toolbarItems, function (item) {
                                return item.name === "groupPanel";
                            })[0];

                            if (groupPanel) {
                                groupPanel.sortIndex = 2;
                                toolbarItems.sort(function (a, b) {
                                    // if they are equal, return 0 (no sorting)
                                    if (a.sortIndex == b.sortIndex) { return 0; }
                                    if (a.sortIndex > b.sortIndex) {
                                        // if a should come after b, return 1
                                        return 1;
                                    }
                                    else {
                                        // if b should come after a, return -1
                                        return -1;
                                    }
                                });
                            }
                            break;

                        case 2:
                            searchPanel.location = "center";
                            break;
                        case 3:
                            searchPanel.location = "after";
                            break;

                        default:
                            searchPanel.location = "after";
                    }
                }
            }

            if (e.component.option('vasco.allowAdd')) {
                e.toolbarOptions.items.splice(2, 0,
                    {
                        location: "before",
                        widget: "dxButton",
                        options: {
                            icon: "plus",
                            text: "Dodaj",
                            onClick: function (arg) {
                                var fn = window[e.component.option('vasco.insertFuncName')];
                                if (typeof fn === "function") fn(arg);
                            }
                        }
                    });
            }

        }
    },
    sifEditView: {
        listViewShowAction: function (e) {
            var model = e.component.option("model");
            Vasco.ui.showListView(model.Controller, model.Action, model.RequestParams, model.Options);
            e.event.preventDefault();
            e.event.stopPropagation();
        },
        detailViewShowAction: function (e) {
            var model = e.component.option("model");
            Vasco.ui.showDetailView(model.Controller, model.Action, model.RequestParams, model.Options);
            e.event.preventDefault();
            e.event.stopPropagation();
        },
        partnerItemTemplate: function (data) {
            return "<b>(" + data.SIFRA + ") " + data.NAZIV + "</b></br>" +
                data.NASLOV + ", " + data.POSTA + "</br>"
                + "Davčna št: " + data.DAVCNA;
        },
        valueChangedCallback: function (e) {
            if (e.component.option("vasco.isInitializing"))
                return;
            var oldValue = e.previousValue;
            var newValue = e.value;
            var newData = null;
            if (newValue) {
                newData = e.component.option('selectedItem');
            }
            var fnValueChanged = window[e.component.element()[0].id + '_ValueChanged'];
            if (typeof fnValueChanged === "function") {
                fnValueChanged({ "component": e.component, "oldValue": oldValue, "newValue": newValue, "newData": newData });
            }
        },
        initializeDefaultValue: function (e) {
            setTimeout(function () {

                e.component.getDataSource().load().done(function (data) {
                    try {

                        e.component.option("vasco.isInitializing", true);
                        e.component.option("value", e.component.option("vasco.defaultValue"));
                        var selectedItem = e.component.option("selectedItem");
                        var dataSource = new DevExpress.data.DataSource({
                            store: DevExpress.data.AspNet.createStore({
                                "key": e.component.option("vasco.valueField"),
                                "loadUrl": e.component.option("vasco.loadUrl")
                            }),
                            paginate: e.component.option("vasco.paginate"),
                            pageSize: e.component.option("vasco.pageSize")
                        }, 10);
                        dataSource.items().push(selectedItem);
                        dataSource._isLoaded = true;
                        //dataSource.filter(e.component.option("vasco.valueField"), "=", e.component.option("vasco.defaultValue"));
                        e.component.option("dataSource", dataSource);
                    }
                    catch (err) {
                        console.log(err);
                    }
                    finally {
                        e.component.option("vasco.isInitializing", false);

                    }
                });
            }, 100);
        },
        tabKeyDownHandler: function (e, view, buttonID, parentViewID) {
           
            if ((e.event.keyCode === 9 /*|| e.event.keyCode === 13*/) && !e.event.shiftKey) {
                setTimeout(function () {
                    var buttonInstance = $("#" + buttonID).dxButton('instance');
                    if (buttonInstance) {
                        buttonInstance.focus();
                    }
                }, 50);
                e.event.preventDefault();
                e.event.stopPropagation();
                return false;
            } else if (e.event.keyCode === 13) {
                if (parentViewID) {
                    setTimeout(function () {
                        e.event.target = view.field();
                        view.close();
                        Vasco.ui.simulateTab(e.event, parentViewID);
                    }, 50);
                    e.event.preventDefault();
                    e.event.stopPropagation();
                    return false;
                }
            }
        },
        fieldTemplate2: function (value, container) {
            var sb = this;
            //console.log(value);
            //console.log(container);
            //console.log(sb);
            //console.log(sb.element()[0].id);
            var searchButtonID = sb.element()[0].id + '_btn';
            var parentViewID = sb.option("vasco.parentViewID");
            sb.option('onKeyDown', function (e) { Vasco.sifEditView.tabKeyDownHandler(e, sb, searchButtonID, parentViewID);});

            var displayExpr = sb.option('displayExpr');

            var $input = $('<div>').dxTextBox({
                onKeyDown: function (e) {
                    return Vasco.sifEditView.tabKeyDownHandler(e, sb, searchButtonID, parentViewID );
                },
                text: displayExpr(value)
            });

            var selectListViewFormController = sb.option("vasco.selectListViewFormController");
            var selectListViewFormAction = sb.option("vasco.selectListViewFormAction");

            if (selectListViewFormController && selectListViewFormAction) {

                var $customButton = $('<div id="' + searchButtonID + '">').dxButton({
                    icon: 'search',
                    onClick: function (e) {
                        var reqData = {};
                        reqData.ViewID = sb.element()[0].id;
                        Vasco.ui.showBusy("... pripravljam podatke ...");
                        Vasco.server.getData(selectListViewFormController, selectListViewFormAction, reqData, function (resultData) {
                            $("<div/>").append(resultData).appendTo($("body"));

                            window[reqData.ViewID + "_RowSelectionResult"] = function (selectedsRows) {
                                if (selectedsRows && selectedsRows.length > 0) {
                                    var item = selectedsRows[0];

                                    var keyField = sb.getDataSource().key();

                                    var currentItems = sb.getDataSource().items();
                                    var foundItem = Vasco.util.objectExist(currentItems, keyField, item[keyField]);
                                    if (foundItem) {
                                        sb.option("value", item[keyField]);
                                    } else {
                                        sb.getDataSource().filter(keyField, '=', item[keyField]);
                                        sb.getDataSource().load().done(function (result) {
                                            sb.getDataSource().filter(null);
                                            sb.option("value", item[keyField]);
                                        });
                                    }
                                }
                            };

                        }, function () {
                            //fail
                        }, function () {
                            Vasco.ui.hideBusy();
                        });
                        e.event.preventDefault();
                        e.event.stopPropagation();
                    }
                });
                $customButton.css({
                    position: 'absolute',
                    right: '0px',
                    top: '0px',
                    bottom: '0px'
                });

                container.append($input);
                container.append($customButton);

            } else {
                container.append($input);
            }
        }



    },
    server: {
        saveData: function (controller, action, dataObj, doneCallback, failCallback, alwaysCallback) {
            dataObj["__RequestVerificationToken"] = $('input[name=__RequestVerificationToken]').val();
            $.ajax({
                url: Vasco.BaseUrl + '/' + controller + '/' + action,
                type: 'POST',
                data: dataObj
            }).done(function (resData) {
                if (doneCallback)
                    doneCallback(resData);
            }).fail(function (resData) {
                if (failCallback)
                    failCallback(resData);
            }).always(function (resData) {
                if (alwaysCallback)
                    alwaysCallback(resData);
            });
        },
        getData: function (controller, action, dataObj, doneCallback, failCallback, alwaysCallback) {
            $.ajax({
                url: Vasco.BaseUrl + '/' + controller + '/' + action,
                type: 'GET',
                data: dataObj
            }).done(function (resData) {
                if (doneCallback)
                    doneCallback(resData);
            }).fail(function (resData) {
                if (failCallback)
                    failCallback(resData);
            }).always(function (resData) {
                if (alwaysCallback)
                    alwaysCallback(resData);
            });

        },
        goToRelativeUrl: function (url, params, history) {
            var paramsTxt = "";
            if (params) {
                paramsTxt = "?" + $.param(params);
            }
            var urlLink = Vasco.BaseUrl + url + paramsTxt;
            if (history) {
                window.open(urlLink, "_self");
            } else
                window.location.replace(urlLink);
        },
        goTo: function (controller, action, params, history) {
            var paramsTxt = "";
            if (params) {
                paramsTxt = "?" + $.param(params);
            }
            var url = Vasco.BaseUrl + '/' + controller + '/' + action + paramsTxt;
            if (history) {
                window.open(url, "_self");
            } else
                window.location.replace(url);
        },
        goToWithHistory: function (controller, action, params) {
            Vasco.server.goTo(controller, action, params, true);
        },
        goToNewTab: function (controller, action, params) {
            var paramsTxt = "";
            if (params) {
                paramsTxt = "?" + $.param(params);
            }
            window.open(Vasco.BaseUrl + '/' + controller + '/' + action + paramsTxt, "_blank");
        },
        goToRelativeUrlWithHistory: function (url, params) {
            Vasco.server.goToRelativeUrl(url, params, true);
        },
        goToRelativeUrlNewTab: function (url, params) {
            var paramsTxt = "";
            if (params) {
                paramsTxt = "?" + $.param(params);
            }
            window.open(Vasco.BaseUrl + url + paramsTxt, "_blank");
        }
    },
    resources: {
        init: function (baseURL) {
            Vasco.BaseUrl = baseURL;
            $.holdReady(true); // important!
            $.when(
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/main/sl/ca-gregorian.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/main/sl/numbers.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/main/sl/currencies.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/supplemental/likelySubtags.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/supplemental/timeData.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/supplemental/weekData.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/supplemental/currencyData.json"),
                $.getJSON(Vasco.BaseUrl + "/js/devextreme/cldr/supplemental/numberingSystems.json")
            ).then(function () {
                return [].slice.apply(arguments, [0]).map(function (result) {
                    return result[0];
                });
            }).then(
                Globalize.load
                ).then(function () {
                    DevExpress.localization.loadMessages({
                        "sl": {
                            "validation-required-formatted": "Zahtevano: {0}"
                        }
                    });

                    Globalize.locale('sl');
                    DevExpress.localization.locale("sl");
                    DevExpress.config({ defaultCurrency: 'EUR' });
                    $.holdReady(false); // important!
                });

        }
    },
    goHome: function () {
        window.location.replace(Vasco.BaseUrl);
    }

};

var prevClickTime = 0;
var lastClickTime = 0;
var clickTimer = 0;
