function sidebarMenuClicked(e) {
    $('#vascoSidebar').toggle(0, function () {
        var isVisible = $('#vascoSidebar').is(":visible");
        if (isVisible) {
            $('#vascoContent').removeClass('vascoContentNoSidebar');
            $('#vascoContent').addClass('vascoContentSidebar');
        } else {
            $('#vascoContent').removeClass('vascoContentSidebar');
            $('#vascoContent').addClass('vascoContentNoSidebar');
        }
        checkContentResize();
    });
}

function hideSidebar() {
    $('#vascoSidebar').hide(0);
    $('#vascoContent').removeClass('vascoContentSidebar');
    $('#vascoContent').addClass('vascoContentNoSidebar');

}

function showSidebar() {
    $('#vascoSidebar').show(0);
    $('#vascoContent').removeClass('vascoContentNoSidebar');
    $('#vascoContent').addClass('vascoContentSidebar');
}

function checkSidebarResize() {
    if ($(window).width() < 640) {
        hideSidebar();
    } else {
        showSidebar();
    }
}

//menu image template
function mainMenuItemTemplate(itemData, itemIndex, itemElement) {
    if (itemData.Image != "") {
        itemElement.append('<img src="' + itemData.Image + '" />');
        itemElement.append('&nbsp;');
    }
    itemElement.append(itemData.Text);
}

//ob spremembi teksta v iskalniku menija
function mainMenuSearchBoxValueChanged(e) {
    $("#mainMenu").dxTreeView("instance").option("searchValue", e.value);
}
//klik na posamezno izbiro menija
function mainMenuItemClick(e) {
 
    var item = e.itemData;
    if (item.Url.length > 0) {
        if (e.event.ctrlKey)
            Vasco.server.goToRelativeUrlNewTab(item.Url, null);
        else
            Vasco.server.goToRelativeUrlWithHistory(item.Url, null);
    } 
}

function checkContentResize() {
    pageContentHeight = $('#vascoContent').height();
    //pageContentWidth = $('#vascoContent').width();
    pageContentHeaderHeight = $('#pageHeader').height();

    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function () {
        $('#vascoContent').children("div[view='ListView']").each(function (key, value) {

            var viewHeaderHeight = $('#' + $(value).attr('id') + '_viewHeader').height();
            var viewFooterHeight = $('#' + $(value).attr('id') + '_viewFooter').height();
            var heightType = $(value).attr('viewautoheight');
            var height = 0;

            if (heightType == "Fill")
                height = pageContentHeight - pageContentHeaderHeight - viewHeaderHeight - viewFooterHeight - 25;
            else if (heightType == "Half")
                height = (pageContentHeight - pageContentHeaderHeight - viewHeaderHeight - viewFooterHeight ) / 2 - 25;

            if (height > 0) {
                var gridInstance = $(value).dxDataGrid('instance');
                gridInstance.beginUpdate();
                gridInstance.option('height', height);
                gridInstance.endUpdate();
            }
        });
    }, 100);

    
}

function focusListViewSearch() {
    setTimeout(function () { $('.dx-datagrid .dx-datagrid-search-panel').dxTextBox('instance').focus(); }, 300);
}


$(document).ready(function () {
    checkSidebarResize();
    $(window).resize(function () {

        checkSidebarResize();
        checkContentResize();
    });
});

var pageContentHeaderHeight = 0;
var pageContentHeight = 0;
var pageContentWidth = 0;
var resizeTimeout = 0;

