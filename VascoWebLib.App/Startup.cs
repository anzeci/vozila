using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using VascoWebApp.Infrastructure;
using VascoWebApp.Infrastructure.Services.Menu;
using VascoWebLib.Core.Interfaces.Database;
using VascoWebLib.Core.Interfaces.Repository;
using VascoWebLib.Core.UI.Helpers;
using VascoWebLib.Core.UI.Interfaces.Menu;
using VascoWebLib.Infrastructure.Database;
using VascoWebLib.Infrastructure.Repository;

namespace VascoWebLib_App
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;
        }

        public IConfiguration Configuration { get; private set; }
        public IHostingEnvironment HostingEnvironment { get; private set; }
        public IHttpContextAccessor HttpContextAccessor { get; private set; }
        public IActionContextAccessor ActionContextAccessor { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //HTTP CONTEXT & HELPER
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper>(x =>
            {
                var actionContext = x.GetRequiredService<IActionContextAccessor>().ActionContext;
                var factory = x.GetRequiredService<IUrlHelperFactory>();
                return factory.GetUrlHelper(actionContext);
            });
            //DEFAULT DATABASE CONNECTION PROIVIDER
            services.AddTransient<ISqlConnectionProvider>(r =>
            {
                return new SqlConnectionProvider
                {
                    Provider = SqlConnectionProvider.FirebirdProviderName,
                    ConnectionString = Configuration.GetConnectionString("WebAppDBConnection")
                };
            });

            //DELAVEC (PNW) repository
            services.AddTransient<IDelavecRepositoryPNW, DelavecRepositoryPNW>();
            //SM repository
            services.AddTransient<ISMRepository, SMRepository>();
            //SM2 repository
            services.AddTransient<ISM2Repository, SM2Repository>();
            //DRZAVA repository
            services.AddTransient<IDrzavaRepository, DrzavaRepository>();
            //SKUPINA (PNW )repository
            services.AddTransient<ISkupinaRepositoryPNW, SkupinaRepositoryPNW>();
            //VOZILO (PNW) repository
            services.AddTransient<IVoziloRepositoryPNW, VoziloRepositoryPNW>();

            //MENU SERVICE
            services.AddScoped<IVascoMenuService>(m =>
            {
                var urlHelper = m.GetService<IUrlHelper>();
                string path = HttpContextAccessor.HttpContext.Request.Path;
                if (string.IsNullOrEmpty(path) || path.Equals("/"))
                {
                    path = ViewStrings.DefaultViewPath;
                }
                return new VascoAppMenuService(urlHelper.Content("~/images"), path, true);
            });

            // Add framework services.
            services
                .AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=" + ViewStrings.DefaultController + "}/{action=" + ViewStrings.DefaultAction + "}/{id?}");
            });

            HttpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
            ActionContextAccessor = app.ApplicationServices.GetRequiredService<IActionContextAccessor>();
            VascoWebHelper.Configure(HttpContextAccessor, ActionContextAccessor);
        }
    }
}