using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SkupinaListView : ListViewModel
    {
        public SkupinaListView()
        {
            Controller = ViewStrings.Controller.Skupina;
            LoadAction = ViewStrings.Action.ListViewData;
            ViewPath = ViewStrings.ListViewPartialPath.SkupinaListView;
            EditFormAction = ViewStrings.Action.DetailViewDialogEdit;
            InsertFormAction = ViewStrings.Action.DetailViewDialogInsert;
            ViewFormAction = ViewStrings.Action.DetailViewDialogView;
            DeleteAction = ViewStrings.Action.Delete;
            Key = new string[] { "SIFRA" };
            Title = "Seznam skupin";
            RowTextDisplayExpression = "'('+rowData.SIFRA+') '+ rowData.NAZIV";
        }

        /// <summary>
        /// Insert/Update/View se ne odpira v dialogih ampak na svojih straneh
        /// </summary>
        public void UsePages()
        {
            EditFormAction = ViewStrings.Action.DetailViewPageEdit;
            EditFormActionAsDialog = false;
            InsertFormAction = ViewStrings.Action.DetailViewPageInsert;
            InsertFormActionAsDialog = false;
            ViewFormAction = ViewStrings.Action.DetailViewPageView;
            ViewFormActionAsDialog = false;
        }
    }
}