namespace VascoWebApp.Infrastructure.Model.View
{
    public class SkupinaInsertDialog : BaseDialogViewModel
    {
        public SkupinaInsertDialog(SkupinaDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}