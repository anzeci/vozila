namespace VascoWebApp.Infrastructure.Model.View
{
    public class SkupinaEditDialog : BaseDialogViewModel
    {
        public SkupinaEditDialog(SkupinaDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}