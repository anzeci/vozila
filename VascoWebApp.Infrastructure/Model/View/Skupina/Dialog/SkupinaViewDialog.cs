namespace VascoWebApp.Infrastructure.Model.View
{
    public class SkupinaViewDialog : BaseDialogViewModel
    {
        public SkupinaViewDialog(SkupinaDetailView detailView) : base(detailView, false, false, true)
        {
        }
    }
}