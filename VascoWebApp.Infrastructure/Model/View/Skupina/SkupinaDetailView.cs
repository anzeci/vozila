using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SkupinaDetailView : DetailViewModel
    {
        public SkupinaDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.SkupinaDetailView;
            Controller = ViewStrings.Controller.Skupina;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            DefaultFocusedField = "NAZIV";
        }
    }
}