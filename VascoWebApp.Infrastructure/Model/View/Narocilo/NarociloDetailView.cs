using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class NarociloDetailView : DetailViewModel
    {
        public NarociloDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.NarociloDetailView;
            Controller = ViewStrings.Controller.Narocilo;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            RedirectAction = ViewStrings.Action.ListViewPage;
            FormColumnCount = 2;
            //DefaultFocusedField = "PRIIMEK";
        }

        public override void GetToolbarButtons()
        {
            TopToolbar.Clear();
            BottomToolbar.Clear();
        }
    }
}