using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class NarociloListView : ListViewModel
    {
        public NarociloListView()
        {
            Controller = ViewStrings.Controller.Narocilo;
            LoadAction = ViewStrings.Action.ListViewData;
            ViewPath = ViewStrings.ListViewPartialPath.NarociloListView;
            EditFormAction = ViewStrings.Action.DetailViewDialogEdit;
            InsertFormAction = ViewStrings.Action.DetailViewDialogInsert;
            ViewFormAction = ViewStrings.Action.DetailViewDialogView;
            DeleteAction = ViewStrings.Action.Delete;
            Key = new string[] { "STEVILKA", "LETO" };
            Title = "Seznam naro�il";
            RowTextDisplayExpression = "rowData.STEVILKA + '.' + rowData.LETO";
        }

        /// <summary>
        /// Insert/Update/View se ne odpira v dialogih ampak na svojih straneh
        /// </summary>
        public void UsePages()
        {
            EditFormAction = ViewStrings.Action.DetailViewPageEdit;
            EditFormActionAsDialog = false;
            InsertFormAction = ViewStrings.Action.DetailViewPageInsert;
            InsertFormActionAsDialog = false;
            ViewFormAction = ViewStrings.Action.DetailViewPageView;
            ViewFormActionAsDialog = false;
        }
    }
}