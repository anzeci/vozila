using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class NarociloDetailViewData
    {
        public NarociloDetailViewData()
        {
            Glava = new FA_NAR_KUPCI();
            Postavke = new List<FA_NAR_KUPCI_KNJ>();
        }

        public FA_NAR_KUPCI Glava { get; set; }
        public IList<FA_NAR_KUPCI_KNJ> Postavke { get; set; }

    }
}