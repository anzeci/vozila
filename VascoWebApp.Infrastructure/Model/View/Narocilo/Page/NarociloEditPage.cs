namespace VascoWebApp.Infrastructure.Model.View
{
    public class NarociloEditPage : BaseDetailPageViewModel
    {
        public NarociloEditPage(NarociloDetailView detailView) : base(detailView, true, true, false)
        {
            detailView.GetToolbarButtons();
        }
    }
}