namespace VascoWebApp.Infrastructure.Model.View
{
    public class NarociloViewPage : BaseDetailPageViewModel
    {
        public NarociloViewPage(NarociloDetailView detailView) : base(detailView, false, false, true)
        {
           detailView.SetReadOnly();
           detailView.GetToolbarButtons();
        }
    }
}