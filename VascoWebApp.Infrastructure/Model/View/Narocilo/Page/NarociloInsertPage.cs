namespace VascoWebApp.Infrastructure.Model.View
{
    public class NarociloInsertPage : BaseDetailPageViewModel
    {
        public NarociloInsertPage(NarociloDetailView detailView) : base(detailView, true, true, false)
        {
            detailView.GetToolbarButtons();
        }
    }
}