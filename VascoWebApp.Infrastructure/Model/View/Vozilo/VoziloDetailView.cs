using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class VoziloDetailView : DetailViewModel
    {
        public VoziloDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.VoziloDetailView;
            Controller = ViewStrings.Controller.Vozilo;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            DefaultFocusedField = "OPIS";
        }
    }
}