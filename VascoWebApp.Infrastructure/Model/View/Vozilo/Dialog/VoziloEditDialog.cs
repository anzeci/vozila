namespace VascoWebApp.Infrastructure.Model.View
{
    public class VoziloEditDialog : BaseDialogViewModel
    {
        public VoziloEditDialog(VoziloDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}