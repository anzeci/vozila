using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class VoziloViewDialog : DialogViewModel
    {
        public VoziloViewDialog(VoziloDetailView detailView) : base(detailView)
        {
        }
    }
}