namespace VascoWebApp.Infrastructure.Model.View
{
    public class VoziloInsertDialog : BaseDialogViewModel
    {
        public VoziloInsertDialog(VoziloDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}