
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class VoziloListViewSelectionDialog : BaseSelectionDialogViewModel
    {
        public VoziloListViewSelectionDialog(VoziloListView listView): base(listView)
        {
            listView.ListViewOptions.SelectionMode = ListViewSelectionMode.Single;
            listView.ListViewOptions.RowDblClickSelection = true;
        }
    }
}
