using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecListView : ListViewModel
    {
        public DelavecListView()
        {
            Controller = ViewStrings.Controller.Delavec;
            LoadAction = ViewStrings.Action.ListViewData;
            ViewPath = ViewStrings.ListViewPartialPath.DelavecListView;
            EditFormAction = ViewStrings.Action.DetailViewDialogEdit;
            InsertFormAction = ViewStrings.Action.DetailViewDialogInsert;
            ViewFormAction = ViewStrings.Action.DetailViewDialogView;
            DeleteAction = ViewStrings.Action.Delete;
            Key = new string[] { "SIFRA" };
            Title = "Seznam delavcev";
            RowTextDisplayExpression = "'('+rowData.SIFRA+') '+ rowData.PRIIMEK";
            GetToolbarButtons();
        }

        /// <summary>
        /// Insert/Update/View se ne odpira v dialogih ampak na svojih straneh
        /// </summary>
        public void UsePages()
        {
            EditFormAction = ViewStrings.Action.DetailViewPageEdit;
            EditFormActionAsDialog = false;
            InsertFormAction = ViewStrings.Action.DetailViewPageInsert;
            InsertFormActionAsDialog = false;
            ViewFormAction = ViewStrings.Action.DetailViewPageView;
            ViewFormActionAsDialog = false;
        }

        public override void GetToolbarButtons()
        {
            TopToolbar.Clear();
            BottomToolbar.Clear();

            TopToolbar.Add(new ToolbarViewButton
            {
                ID = "btnFilter",
                Text = "Filter",
                DialogResult = DialogViewResult.drNone,
                ButtonType = ToolbarViewButtonType.Normal,
                ButtonLocation = ToolbarViewButtonLocation.Before,
                Tag = "Filter"
            });
        }
    }
}