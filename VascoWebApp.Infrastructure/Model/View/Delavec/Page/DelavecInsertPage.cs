namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecInsertPage : BaseDetailPageViewModel
    {
        public DelavecInsertPage(DelavecDetailView detailView) : base(detailView, true, true, false)
        {
            detailView.GetToolbarButtons();
        }
    }
}