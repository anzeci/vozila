namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecViewPage : BaseDetailPageViewModel
    {
        public DelavecViewPage(DelavecDetailView detailView) : base(detailView, false, false, true)
        {
           detailView.SetReadOnly();
           detailView.GetToolbarButtons();
        }
    }
}