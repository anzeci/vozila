namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecEditPage : BaseDetailPageViewModel
    {
        public DelavecEditPage(DelavecDetailView detailView) : base(detailView, true, true, false)
        {
            detailView.GetToolbarButtons();
        }
    }
}