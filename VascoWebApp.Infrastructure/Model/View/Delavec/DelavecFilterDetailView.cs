using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecFilterDetailView : DetailViewModel
    {
        public DelavecFilterDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.DelavecFilterDetailView;
            Controller = ViewStrings.Controller.Delavec;
            FormColumnCount = 1;
            DefaultFocusedField = "SIFRA";
        }
    }
}