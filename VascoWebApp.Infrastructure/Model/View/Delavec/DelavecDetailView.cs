using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecDetailView : DetailViewModel
    {
        public DelavecDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.DelavecDetailView;
            Controller = ViewStrings.Controller.Delavec;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            RedirectAction = ViewStrings.Action.ListViewPageNoDialog;
            FormColumnCount = 2;
            DefaultFocusedField = "PRIIMEK";
        }

        public override void GetToolbarButtons()
        {
            TopToolbar.Clear();
            BottomToolbar.Clear();

            TopToolbar.Add(new ToolbarViewButton
            {
                ID = "btnToolbarAkcija1",
                Text = "Akcija na obrazcu",
                Tag = "001",
                DialogResult = DialogViewResult.drNone,
                ButtonType = ToolbarViewButtonType.Normal,
                ButtonLocation = ToolbarViewButtonLocation.Before,
                Visible = EditState != ViewEditState.View
            });
        }
    }
}