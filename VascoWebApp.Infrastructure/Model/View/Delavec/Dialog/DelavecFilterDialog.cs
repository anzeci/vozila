namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecFilterDialog : BaseFilterDialogViewModel
    {
        public DelavecFilterDialog(DelavecFilterDetailView detailView) : base(detailView, true, true)
        {
            Title = detailView.Title;
            detailView.GetToolbarButtons();
        }
    }
}