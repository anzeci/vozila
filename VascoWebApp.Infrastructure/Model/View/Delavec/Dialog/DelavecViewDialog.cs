namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecViewDialog : BaseDialogViewModel
    {
        public DelavecViewDialog(DelavecDetailView detailView) : base(detailView, false, false, true)
        {
            detailView.SetReadOnly();
            detailView.GetToolbarButtons();
        }
    }
}