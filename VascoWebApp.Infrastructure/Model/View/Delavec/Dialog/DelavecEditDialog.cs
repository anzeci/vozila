namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecEditDialog : BaseDialogViewModel
    {
        public DelavecEditDialog(DelavecDetailView detailView) : base(detailView, true, true, false)
        {
            detailView.GetToolbarButtons();
        }
    }
}