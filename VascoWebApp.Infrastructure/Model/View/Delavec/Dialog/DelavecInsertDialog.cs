namespace VascoWebApp.Infrastructure.Model.View
{
    public class DelavecInsertDialog : BaseDialogViewModel
    {
        public DelavecInsertDialog(DelavecDetailView detailView) : base(detailView, true, true, false)
        {
            detailView.GetToolbarButtons();
        }
    }
}