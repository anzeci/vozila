using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class DrzavaDetailView : DetailViewModel
    {
        public DrzavaDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.DrzavaDetailView;
            Controller = ViewStrings.Controller.Drzava;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            DefaultFocusedField = "NAZIV";
        }
    }
}