namespace VascoWebApp.Infrastructure.Model.View
{
    public class DrzavaInsertDialog : BaseDialogViewModel
    {
        public DrzavaInsertDialog(DrzavaDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}