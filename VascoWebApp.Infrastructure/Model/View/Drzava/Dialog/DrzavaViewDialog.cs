namespace VascoWebApp.Infrastructure.Model.View
{
    public class DrzavaViewDialog : BaseDialogViewModel
    {
        public DrzavaViewDialog(DrzavaDetailView detailView) : base(detailView, false, false, true)
        {
        }
    }
}