namespace VascoWebApp.Infrastructure.Model.View
{
    public class DrzavaEditDialog : BaseDialogViewModel
    {
        public DrzavaEditDialog(DrzavaDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}