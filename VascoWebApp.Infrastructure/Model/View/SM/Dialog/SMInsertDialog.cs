namespace VascoWebApp.Infrastructure.Model.View
{
    public class SMInsertDialog : BaseDialogViewModel
    {
        public SMInsertDialog(SMDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}