namespace VascoWebApp.Infrastructure.Model.View
{
    public class SMViewDialog : BaseDialogViewModel
    {
        public SMViewDialog(SMDetailView detailView) : base(detailView, false, false, true)
        {
        }
    }
}