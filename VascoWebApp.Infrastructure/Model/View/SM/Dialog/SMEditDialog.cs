namespace VascoWebApp.Infrastructure.Model.View
{
    public class SMEditDialog : BaseDialogViewModel
    {
        public SMEditDialog(SMDetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}