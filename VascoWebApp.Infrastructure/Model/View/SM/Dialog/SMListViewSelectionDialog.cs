using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SMListViewSelectionDialog : BaseSelectionDialogViewModel
    {
        public SMListViewSelectionDialog(SMListView listView): base(listView)
        {
            listView.ListViewOptions.SelectionMode = ListViewSelectionMode.Single;
            listView.ListViewOptions.RowDblClickSelection = true;
        }
    }
}
