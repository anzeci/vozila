using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SMListView : ListViewModel
    {
        public SMListView()
        {
            Controller = ViewStrings.Controller.SM;
            LoadAction = ViewStrings.Action.ListViewData;
            ViewPath = ViewStrings.ListViewPartialPath.SMListView;
            EditFormAction = ViewStrings.Action.DetailViewDialogEdit;
            InsertFormAction = ViewStrings.Action.DetailViewDialogInsert;
            ViewFormAction = ViewStrings.Action.DetailViewDialogView;
            DeleteAction = ViewStrings.Action.Delete;
            Key = new string[] { "SIFRA" };
            Title = "Sto�kovna mesta";
            RowTextDisplayExpression = "'('+rowData.SIFRA+') '+ rowData.NAZIV";
        }

        /// <summary>
        /// Insert/Update/View se ne odpira v dialogih ampak na svojih straneh
        /// </summary>
        public void UsePages()
        {
            EditFormAction = ViewStrings.Action.DetailViewPageEdit;
            EditFormActionAsDialog = false;
            InsertFormAction = ViewStrings.Action.DetailViewPageInsert;
            InsertFormActionAsDialog = false;
            ViewFormAction = ViewStrings.Action.DetailViewPageView;
            ViewFormActionAsDialog = false;
        }
    }
}