using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SMDetailView : DetailViewModel
    {
        public SMDetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.SMDetailView;
            Controller = ViewStrings.Controller.SM;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            DefaultFocusedField = "NAZIV";
        }
    }
}