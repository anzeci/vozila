using VascoWebLib.Core.UI.Model;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class BaseSelectionDialogViewModel : DialogViewModel
    {
        public BaseSelectionDialogViewModel(ListViewModel listView) : base(listView)
        {
        }

        public override void GetToolbarButtons()
        {
            BottomToolbar.Add(new ToolbarViewButton
            {
                ID = "btnOk",
                Text = ViewStrings.Captions.ButtonSelect,
                DialogResult = DialogViewResult.drOk,
                ButtonType = ToolbarViewButtonType.Success,
                ButtonCommand = ToolbarViewButtonCommand.cmdSelectListViewRows,
                ViewID = ViewModel.ViewID,
                ParentViewID = ViewModel.ParentViewID
            });

            BottomToolbar.Add(new ToolbarViewButton
            {
                ID = "btnCancel",
                Text = ViewStrings.Captions.ButtonCancel,
                DialogResult = DialogViewResult.drCancel,
                ButtonType = ToolbarViewButtonType.Danger
            });
        }
    }
}