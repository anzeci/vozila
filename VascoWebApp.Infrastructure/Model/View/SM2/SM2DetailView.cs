using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SM2DetailView : DetailViewModel
    {
        public SM2DetailView()
        {
            ViewPath = ViewStrings.DetailViewPartialPath.SM2DetailView;
            Controller = ViewStrings.Controller.SM2;
            EditAction = ViewStrings.Action.Edit;
            InsertAction = ViewStrings.Action.Insert;
            DeleteAction = ViewStrings.Action.Delete;
            DefaultFocusedField = "NAZIV";
        }
    }
}