using VascoWebLib.Core.UI.Model.View;

namespace VascoWebApp.Infrastructure.Model.View
{
    public class SM2ListViewSelectionDialog : BaseSelectionDialogViewModel
    {
        public SM2ListViewSelectionDialog(SM2ListView listView) : base(listView)
        {
            listView.ListViewOptions.SelectionMode = ListViewSelectionMode.Single;
            listView.ListViewOptions.RowDblClickSelection = true;
        }
    }
}