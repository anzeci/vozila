namespace VascoWebApp.Infrastructure.Model.View
{
    public class SM2EditDialog : BaseDialogViewModel
    {
        public SM2EditDialog(SM2DetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}