namespace VascoWebApp.Infrastructure.Model.View
{
    public class SM2InsertDialog : BaseDialogViewModel
    {
        public SM2InsertDialog(SM2DetailView detailView) : base(detailView, true, true, false)
        {
        }
    }
}