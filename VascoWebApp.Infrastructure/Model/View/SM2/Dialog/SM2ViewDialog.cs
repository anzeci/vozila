namespace VascoWebApp.Infrastructure.Model.View
{
    public class SM2ViewDialog : BaseDialogViewModel
    {
        public SM2ViewDialog(SM2DetailView detailView) : base(detailView, false, false, true)
        {
        }
    }
}