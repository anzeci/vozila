namespace VascoWebApp.Infrastructure
{
    public static class ViewStrings
    {
        public const string DefaultViewPath = "/Delavec/ListViewPage";
        public const string DefaultController = "Delavec";
        public const string DefaultAction = "ListViewPage";

        public static class Controller
        {
            public const string Delavec = "Delavec";
            public const string Drzava = "Drzava";
            public const string Skupina = "Skupina";
            public const string SM = "SM";
            public const string SM2 = "SM2";
            public const string Vozilo = "Vozilo";
            public const string Narocilo = "Narocilo";
        }

        public static class Action
        {
            public const string ListViewPage = "ListViewPage";
            public const string ListViewPageNoDialog = "ListViewPageNoDialog";
            public const string ListViewData = "ListViewData";
            public const string SifEditViewSelectionDialog = "SifEditViewSelectionDialog";
            public const string SifEditViewData = "SifEditViewData";
            public const string DetailViewDialogEdit = "DetailViewDialogEdit";
            public const string DetailViewDialogInsert = "DetailViewDialogInsert";
            public const string DetailViewDialogView = "DetailViewDialogView";
            public const string DetailViewPageEdit = "DetailViewPageEdit";
            public const string DetailViewPageInsert = "DetailViewPageInsert";
            public const string DetailViewPageView = "DetailViewPageView";
            public const string Insert = "InsertData";
            public const string Edit = "EditData";
            public const string Delete = "DeleteData";
        }

        public static class ListViewPartialPath
        {
            public const string DelavecListView = "/Views/Shared/Partial/Delavec/_ListView.cshtml";
            public const string DrzavaListView = "/Views/Shared/Partial/Drzava/_ListView.cshtml";
            public const string SkupinaListView = "/Views/Shared/Partial/Skupina/_ListView.cshtml";
            public const string SMListView = "/Views/Shared/Partial/SM/_ListView.cshtml";
            public const string SM2ListView = "/Views/Shared/Partial/SM2/_ListView.cshtml";
            public const string VoziloListView = "/Views/Shared/Partial/Vozilo/_ListView.cshtml";
            public const string NarociloListView = "/Views/Shared/Partial/Narocilo/_ListView.cshtml";
        }

        public static class DetailViewPartialPath
        {
            public const string DelavecDetailView = "/Views/Shared/Partial/Delavec/_DetailView.cshtml";
            public const string DelavecFilterDetailView = "/Views/Shared/Partial/Delavec/_FilterDetailView.cshtml";
            public const string DrzavaDetailView = "/Views/Shared/Partial/Drzava/_DetailView.cshtml";
            public const string SkupinaDetailView = "/Views/Shared/Partial/Skupina/_DetailView.cshtml";
            public const string SMDetailView = "/Views/Shared/Partial/SM/_DetailView.cshtml";
            public const string SM2DetailView = "/Views/Shared/Partial/SM2/_DetailView.cshtml";
            public const string VoziloDetailView = "/Views/Shared/Partial/Vozilo/_DetailView.cshtml";
            public const string NarociloDetailView = "/Views/Shared/Partial/Narocilo/_DetailView.cshtml";
        }

        public static class PageView
        {
            public const string Page = "Page";
        }

        public static class Components
        {
            public const string ListView = "ListView";
            public const string DetailView = "DetailView";
            public const string DialogView = "DialogView";
            public const string Menu = "Menu";
        }

        public static class Captions
        {
            public const string ButtonAdd = "DODAJ";
            public const string ButtonConfirm = "POTRDI";
            public const string ButtonCancel = "PREKLI�I";
            public const string ButtonSelect = "IZBERI";
            public const string ButtonOk = "V REDU";
            public const string ButtonSave = "SHRANI";
            public const string ButtonClose = "ZAPRI";
        }
    }
}