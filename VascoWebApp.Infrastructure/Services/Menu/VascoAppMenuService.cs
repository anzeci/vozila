using System.Collections.Generic;
using VascoWebLib.Core.UI.Interfaces.Menu;
using VascoWebLib.Infrastructure.Services.Menu;

namespace VascoWebApp.Infrastructure.Services.Menu
{
    public class VascoAppMenuService : VascoMenuService
    {
        public VascoAppMenuService(string imagesRootPath, string currentUrl, bool showExpanded) : base(imagesRootPath, currentUrl, showExpanded)
        {
        }

        public override IList<IVascoMenuItem> GetMenuItems()
        {
            var result = new List<IVascoMenuItem>();

            IVascoMenuItem sifranti = new VascoMenuItem(this, _showExpanded)
            {
                ID = "0",
                Text = "�ifranti",
                Url = "",
                Selected = false
            };

            sifranti.Image = $"{ImagesRootPath}/sifranti.png";
            result.Add(sifranti);

            IVascoMenuItem itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "1A",
                Text = "Delavec (pogled,urejanje:DIALOGI)",
                Url = $"/{ViewStrings.Controller.Delavec}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/delavec.png";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "1B",
                Text = "Delavec (pogled,urejanje:STRAN)" + ")",
                Url = $"/{ViewStrings.Controller.Delavec}/{ViewStrings.Action.ListViewPageNoDialog}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/delavec.png";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "2",
                Text = "Vozila",
                Url = $"/{ViewStrings.Controller.Vozilo}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/avto.png";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "3",
                Text = "Skupina",
                Url = $"/{ViewStrings.Controller.Skupina}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/skupina.png";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "4",
                Text = "SM",
                Url = $"/{ViewStrings.Controller.SM}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/sm.png?v1";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "5",
                Text = "SM - 2",
                Url = $"/{ViewStrings.Controller.SM2}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/sm.png?v1";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "6",
                Text = "Dr�ava",
                Url = $"/{ViewStrings.Controller.Drzava}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/drzava.png";

            sifranti.SubItems.Add(itm);

            itm = new VascoMenuItem(this, _showExpanded)
            {
                ID = "6",
                Text = "Dr�ava",
                Url = $"/{"ANZETEST"}/{ViewStrings.Action.ListViewPage}"
            };
            itm.CheckSelected(_currentUrl);
            itm.Image = $"{ImagesRootPath}/drzava.png";

            sifranti.SubItems.Add(itm);

            return result;
        }
    }
}