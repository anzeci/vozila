using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface ISM2Repository
    {
        IEnumerable<SM2> GetAll();
        LoadResult GetAll(DataSourceLoadOptionsBase loadOptions);
        SM2 GetBySifra(int sifra);
        int GetNextSifra();
        bool Insert(SM2 item);
        bool Update(SM2 item);
        bool Delete(int sifra);
    }
}
