using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using System.Text;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository

{
    public interface INarociloKupcaRepository
    {
        IList<FA_NAR_KUPCI_GLAVA> GetNarocilaKupca(int partnerSifra);
        LoadResult GetNarocilaKupca(int partnerSifra, DataSourceLoadOptionsBase loadOptions);
        IList<FA_NAR_KUPCI_KNJ_POSTAVKE> GetNarocilaKupcaKnjizbe(string stevilka, int leto);
        LoadResult GetNarocilaKupcaKnjizbe(string stevilka, int leto, DataSourceLoadOptionsBase loadOptions);
        bool Update(FA_NAR_KUPCI item);
        bool Insert(FA_NAR_KUPCI item);
        bool Update(FA_NAR_KUPCI_KNJ item);
        bool Insert(FA_NAR_KUPCI_KNJ item);
        string GetNextNumber(int year, int numberLength, int fromNumber, int toNumber);
        int NumberOfDailyWebOrders(int partnerSifra, DateTime date);
    }
}
