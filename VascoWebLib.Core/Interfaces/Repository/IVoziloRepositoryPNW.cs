using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface IVoziloRepositoryPNW
    {
        IEnumerable<PNW_VOZILO> GetAll();
        LoadResult GetAll(DataSourceLoadOptionsBase loadOptions);
        PNW_VOZILO GetBySifra(int sifra);
        int GetNextSifra();
        bool Insert(PNW_VOZILO item);
        bool Update(PNW_VOZILO item);
        bool Delete(int sifra);
    }
}
