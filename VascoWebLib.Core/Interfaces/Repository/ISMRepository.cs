using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface ISMRepository
    {
        IEnumerable<SM> GetAll();
        LoadResult GetAll(DataSourceLoadOptionsBase loadOptions);
        SM GetBySifra(int sifra);
        int GetNextSifra();
        bool Insert(SM item);
        bool Update(SM item);
        bool Delete(int sifra);
    }
}
