using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface ISkupinaRepositoryPNW
    {
        IEnumerable<PNW_SKUPINA> GetAll();
        LoadResult GetAll(DataSourceLoadOptionsBase loadOptions);
        PNW_SKUPINA GetBySifra(int sifra);
        int GetNextSifra();
        bool Insert(PNW_SKUPINA item);
        bool Update(PNW_SKUPINA item);
        bool Delete(int sifra);
    }
}
