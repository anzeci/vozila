using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System.Collections.Generic;
using System.Linq;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface IPartnerRepository
    {
        PARTNER GetPartnerBySifra(int sifra);
        PARTNER_BASE GetPartnerBySifraWithBaseFields(int sifra);
        PARTNER_BASE GetPartnerByUsername(string username);

        IEnumerable<PARTNER> GetAllPartners();
        IEnumerable<PARTNER_BASE> GetAllPartnersWithBaseFields();

        IEnumerable<FAPARTNERBLAGOVNA> GetPartnerBlagovneSkupine(int sifra);
        IEnumerable<FAPARTNERNADGRUPA> GetPartnerNadGrupe(int sifra);
        IEnumerable<FAPARTNERGRUPA> GetPartnerGrupe(int sifra);
        IEnumerable<FAPARTNERSKLADISCE> GetPartnerSkladisca(int sifra);

        LoadResult GetPartnerBlagovneSkupine(DataSourceLoadOptionsBase loadOptions);
        LoadResult GetPartnerNadGrupe(DataSourceLoadOptionsBase loadOptions);
        LoadResult GetPartnerGrupe(DataSourceLoadOptionsBase loadOptions);
        LoadResult GetPartnerSkladisca(DataSourceLoadOptionsBase loadOptions);

        LoadResult GetAllPartners(DataSourceLoadOptionsBase loadOptions);
        LoadResult GetAllPartnersWithBaseFields(DataSourceLoadOptionsBase loadOptions);

    }
}