using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface IDrzavaRepository
    {
        IEnumerable<DRZAVA> GetAll();
        LoadResult GetAll(DataSourceLoadOptionsBase loadOptions);
        DRZAVA GetBySifra(int sifra);
        int GetNextSifra();
        bool Insert(DRZAVA item);
        bool Update(DRZAVA item);
        bool Delete(int sifra);
    }
}
