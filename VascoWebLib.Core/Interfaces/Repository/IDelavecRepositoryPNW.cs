using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using System;
using System.Collections.Generic;
using VascoWebLib.Core.Entity;

namespace VascoWebLib.Core.Interfaces.Repository
{
    public interface IDelavecRepositoryPNW
    {
        IEnumerable<PNW_DELAVEC> GetAll();
        LoadResult GetAll(DataSourceLoadOptionsBase loadOptions);
        PNW_DELAVEC GetBySifra(int sifra);
        int GetNextSifra();
        bool Insert(PNW_DELAVEC item);
        bool Update(PNW_DELAVEC item);
        bool Delete(int sifra);
    }
}
