using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.Interfaces.Database
{
    public interface ISqlConnectionProvider
    {
        string Provider { get; set; }
        string ConnectionString { get; set; }
    }
}
