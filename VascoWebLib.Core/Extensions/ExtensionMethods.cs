﻿using System.Globalization;

namespace VascoWebLib.Core.Extensions
{
    public static class ExtensionMethods
    {

        private static NumberFormatInfo CreateFormatInfo(int decimalPlaces, string decimalSeparator, string thousandSeparator)
        {
            NumberFormatInfo nf = new NumberFormatInfo
            {
                CurrencyDecimalSeparator = decimalSeparator,
                NumberDecimalSeparator = decimalSeparator,
                CurrencyGroupSeparator = thousandSeparator,
                NumberGroupSeparator = thousandSeparator,
                NumberDecimalDigits = decimalPlaces
            };
            return nf;

        }
        public static string ToStringSlo(this double value, int decimalPlaces = 2, string decimalSeparator = ",", string thousandSeparator = ".")
        {
            return value.ToString("N", CreateFormatInfo(decimalPlaces, decimalSeparator, thousandSeparator));
        }

        public static string ToStringSloNoThousand(this double value, int decimalPlaces = 2, string decimalSeparator = ",")
        {
            return value.ToString("N", CreateFormatInfo(decimalPlaces, decimalSeparator, ""));
        }

        public static string ToStringSlo(this decimal value, int decimalPlaces = 2, string decimalSeparator = ",", string thousandSeparator = ".")
        {
            return value.ToString("N", CreateFormatInfo(decimalPlaces, decimalSeparator, thousandSeparator));
        }

        public static string ToStringSloNoThousand(this decimal value, int decimalPlaces = 2, string decimalSeparator = ",")
        {
            return value.ToString("N", CreateFormatInfo(decimalPlaces, decimalSeparator, ""));
        }

        public static double FromStringSlo(this string value, string decimalSeparator = ",", string thousandSeparator = ".")
        {
            NumberFormatInfo nf = CreateFormatInfo(4, decimalSeparator, thousandSeparator);
            double.TryParse(value, NumberStyles.Number, nf, out double result);
            return result;
        }
        public static string AppendEuroSymbol(this string value)
        {
            return value + " €";
        }
    }
}
