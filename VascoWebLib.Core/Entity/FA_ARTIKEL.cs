using LinqToDB;
using LinqToDB.Mapping;
using LinqToDB.Reflection;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_ARTIKEL")]
    public partial class FAARTIKEL
    {
        public static string DBTableName = "FA_ARTIKEL";

        [Column(), Nullable] public string SIFRA { get; set; } // char(18)
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column(), Nullable] public string NAZIV2 { get; set; } // char(40)
        [Column(), Nullable] public string ENOTA { get; set; } // char(5)
        [Column(), Nullable] public string GRUPA { get; set; } // char(15)
        [Column("STOPNJA_DDV"), Nullable] public int? STOPNJADDV { get; set; } // integer
        [Column(), Nullable] public double? KOLICINA { get; set; } // double precision
        [Column("DOLZINA_SIFRE"), Nullable] public int? DOLZINASIFRE { get; set; } // integer
        [Column("ALFANUM_SIFRA"), Nullable] public int? ALFANUMSIFRA { get; set; } // integer
        [Column(), Nullable] public string IDENT { get; set; } // char(20)
        [Column("NAROCENA_KOLICINA"), Nullable] public double? NAROCENAKOLICINA { get; set; } // double precision
        [Column("TEZA_ARTIKLA"), Nullable] public double? TEZAARTIKLA { get; set; } // double precision
        [Column("TEZA_ARTIKLA_BRUTO"), Nullable] public double? TEZAARTIKLABRUTO { get; set; } // double precision
        [Column(), Nullable] public string MERE { get; set; } // char(30)
        [Column(), Nullable] public double? VOLUMEN { get; set; } // double precision
        [Column(), Nullable] public int? EMBALAZA { get; set; } // integer
        [Column(), Nullable] public int? PREFAKTURIRANJE { get; set; } // integer
        [Column(), Nullable] public int? AKCIJA { get; set; } // integer
        [Column("MINIMALNA_ZALOGA"), Nullable] public double? MINIMALNAZALOGA { get; set; } // double precision
        [Column("MAKSIMALNA_ZALOGA"), Nullable] public double? MAKSIMALNAZALOGA { get; set; } // double precision
        [Column(), Nullable] public double? PAKIRANJE { get; set; } // double precision
        [Column(), Nullable] public double? PAKIRANJE2 { get; set; } // double precision
        [Column(), Nullable] public double? PAKIRANJE3 { get; set; } // double precision
        [Column("OPTIMALNA_ZALOGA"), Nullable] public double? OPTIMALNAZALOGA { get; set; } // double precision
        [Column("TIP_ZALOGE"), Nullable] public int? TIPZALOGE { get; set; } // integer
        [Column("KOEFICIENT_OBRACANJA_ARTIKLA"), Nullable] public double? KOEFICIENTOBRACANJAARTIKLA { get; set; } // double precision
        [Column("DOBAVNI_ROK"), Nullable] public int? DOBAVNIROK { get; set; } // integer
        [Column("DOBAVITELJ_ARTIKLA"), Nullable] public int? DOBAVITELJARTIKLA { get; set; } // integer
        [Column("ENOT_V_KARTONU"), Nullable] public double? ENOTVKARTONU { get; set; } // double precision
        [Column("KARTONOV_NA_PALETI"), Nullable] public double? KARTONOVNAPALETI { get; set; } // double precision
        [Column("SIFRA_ZALOGA"), Nullable] public string SIFRAZALOGA { get; set; } // char(18)
        [Column(), Nullable] public double? FAKTOR { get; set; } // double precision
        [Column("ODVISNI_PROCENT"), Nullable] public double? ODVISNIPROCENT { get; set; } // double precision
        [Column("FORMULA_ZA_CENO"), Nullable] public int? FORMULAZACENO { get; set; } // integer
        [Column("RIP_FAKTOR"), Nullable] public double? RIPFAKTOR { get; set; } // double precision
        [Column("LOT_NACIN"), Nullable] public int? LOTNACIN { get; set; } // integer
        [Column("LOT_DATUM_ZACETKA"), Nullable] public DateTime? LOTDATUMZACETKA { get; set; } // timestamp
        [Column(), Nullable] public string OPIS1 { get; set; } // blob sub_type 1
        [Column(), Nullable] public string OPIS2 { get; set; } // blob sub_type 1
        [Column("CRTNA_KODA"), Nullable] public string CRTNAKODA { get; set; } // char(20)
        [Column("DOMACA_STRAN"), Nullable] public string DOMACASTRAN { get; set; } // char(150)
        [Column(), Nullable] public string KOMERCIALIST { get; set; } // char(5)
        [Column(), Nullable] public int? IZBRAN1 { get; set; } // integer
        [Column(), Nullable] public int? IZBRAN2 { get; set; } // integer
        [Column("NABAVNA_CENA_ZA_VRACILA"), Nullable] public double? NABAVNACENAZAVRACILA { get; set; } // double precision
        [Column("NABAVNA_CENA"), Nullable] public double? NABAVNACENA { get; set; } // double precision
        [Column("VNESENA_NABAVNA_CENA"), Nullable] public double? VNESENANABAVNACENA { get; set; } // double precision
        [Column("PRODAJNA_CENA"), Nullable] public double? PRODAJNACENA { get; set; } // double precision
        [Column("PRODAJNA_CENA2"), Nullable] public double? PRODAJNACENA2 { get; set; } // double precision
        [Column("PRODAJNA_CENA3"), Nullable] public double? PRODAJNACENA3 { get; set; } // double precision
        [Column("PRODAJNA_CENA4"), Nullable] public double? PRODAJNACENA4 { get; set; } // double precision
        [Column("PRODAJNA_CENA5"), Nullable] public double? PRODAJNACENA5 { get; set; } // double precision
        [Column("PRODAJNA_CENA6"), Nullable] public double? PRODAJNACENA6 { get; set; } // double precision
        [Column("PRODAJNA_CENA7"), Nullable] public double? PRODAJNACENA7 { get; set; } // double precision
        [Column("PRODAJNA_CENA8"), Nullable] public double? PRODAJNACENA8 { get; set; } // double precision
        [Column("PRODAJNA_CENA9"), Nullable] public double? PRODAJNACENA9 { get; set; } // double precision
        [Column("PRODAJNA_CENA1_DDV"), Nullable] public double? PRODAJNACENA1DDV { get; set; } // double precision
        [Column("PRODAJNA_CENA2_DDV"), Nullable] public double? PRODAJNACENA2DDV { get; set; } // double precision
        [Column("TUJ_NAZIV"), Nullable] public string TUJNAZIV { get; set; } // char(40)
        [Column("TUJ_NAZIV2"), Nullable] public string TUJNAZIV2 { get; set; } // char(40)
        [Column(), Nullable] public string ENOTA2 { get; set; } // char(5)
        [Column(), Nullable] public double? PRETVORNIK { get; set; } // double precision
        [Column("KARTICA_ARTIKLA"), Nullable] public int? KARTICAARTIKLA { get; set; } // integer
        [Column("ZADNJA_SPREMEMBA_CENE"), Nullable] public DateTime? ZADNJASPREMEMBACENE { get; set; } // timestamp
        [Column("ZADNJA_SPR_NABAVNE_CENE"), Nullable] public DateTime? ZADNJASPRNABAVNECENE { get; set; } // timestamp
        [Column(), Nullable] public int? KONSIGNATOR { get; set; } // integer
        [Column(), Nullable] public int? DOBAVITELJ { get; set; } // integer
        [Column("DOBAVITELJEVA_SIFRA"), Nullable] public string DOBAVITELJEVASIFRA { get; set; } // char(80)
        [Column("PROCENT_REZIJE"), Nullable] public double? PROCENTREZIJE { get; set; } // double precision
        [Column(), Nullable] public string KONTO { get; set; } // char(8)
        [Column("BLAGOVNA_SKUPINA")] public int BLAGOVNASKUPINA { get; set; } // integer
        [Column("CARINSKA_STOPNJA"), Nullable] public string CARINSKASTOPNJA { get; set; } // char(10)
        [Column("BREZ_OKOLJSKE_TAKSE"), Nullable] public int? BREZOKOLJSKETAKSE { get; set; } // integer
        [Column(), Nullable] public double? TROSARINA { get; set; } // double precision
        [Column(), Nullable] public double? TROSARINA2 { get; set; } // double precision
        [Column(), Nullable] public double? TROSARINA3 { get; set; } // double precision
        [Column("FAKTOR_CARINSKA_ENOTA"), Nullable] public double? FAKTORCARINSKAENOTA { get; set; } // double precision
        [Column("NORMATIV_KOLICINA"), Nullable] public double? NORMATIVKOLICINA { get; set; } // double precision
        [Column("NORMATIV_OPIS"), Nullable] public string NORMATIVOPIS { get; set; } // blob sub_type 1
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column("DATUM_VNOSA"), Nullable] public DateTime? DATUMVNOSA { get; set; } // timestamp
        [Column("CENA_ZA_KOMADOV"), Nullable] public double? CENAZAKOMADOV { get; set; } // double precision
        [Column("OSNOVNA_ENOTA"), Nullable] public string OSNOVNAENOTA { get; set; } // char(5)
        [Column("KOL_PAKIRANJA"), Nullable] public double? KOLPAKIRANJA { get; set; } // double precision
        [Column(), Nullable] public int? LOKACIJA { get; set; } // integer
        [Column(), Nullable] public string LOKACIJA1 { get; set; } // char(20)
        [Column(), Nullable] public int? PROIZVAJALEC { get; set; } // integer
        [Column(), Nullable] public int? UVOZNIK { get; set; } // integer
        [Column(), Nullable] public int? KUPEC { get; set; } // integer
        [Column("DRZAVA_POREKLA"), Nullable] public int? DRZAVAPOREKLA { get; set; } // integer
        [Column("ARTIKEL_IZVEN_UPORABE"), Nullable] public int? ARTIKELIZVENUPORABE { get; set; } // integer
        [Column("VALUTA_ZA_NABAVO"), Nullable] public int? VALUTAZANABAVO { get; set; } // integer
        [Column("NABAVNI_RABAT"), Nullable] public double? NABAVNIRABAT { get; set; } // double precision
        [Column("MALOPRODAJNI_RABAT"), Nullable] public double? MALOPRODAJNIRABAT { get; set; } // double precision
        [Column(), Nullable] public double? RABAT { get; set; } // double precision
        [Column("MEM_FILTER"), Nullable] public int? MEMFILTER { get; set; } // integer
        [Column("VNOS_SERIJSKIH"), Nullable] public int? VNOSSERIJSKIH { get; set; } // integer
        [Column(), Nullable] public string SLIKA { get; set; } // char(150)
        [Column("SIFRA_SKUPINE_SPLET"), Nullable] public int? SIFRASKUPINESPLET { get; set; } // integer
        [Column("PROSTO_I1"), Nullable] public int? PROSTOI1 { get; set; } // integer
        [Column("PROSTO_I2"), Nullable] public int? PROSTOI2 { get; set; } // integer
        [Column("PROSTO_R1"), Nullable] public double? PROSTOR1 { get; set; } // double precision
        [Column("PROSTO_R2"), Nullable] public double? PROSTOR2 { get; set; } // double precision
        [Column("PROSTO_R3"), Nullable] public double? PROSTOR3 { get; set; } // double precision
        [Column("PROSTO_R4"), Nullable] public double? PROSTOR4 { get; set; } // double precision
        [Column("PROSTO_R5"), Nullable] public double? PROSTOR5 { get; set; } // double precision
        [Column("PROSTO_R6"), Nullable] public double? PROSTOR6 { get; set; } // double precision
        [Column("PROSTO_R7"), Nullable] public double? PROSTOR7 { get; set; } // double precision
        [Column("PROSTO_R8"), Nullable] public double? PROSTOR8 { get; set; } // double precision
        [Column("PROSTO_R9"), Nullable] public double? PROSTOR9 { get; set; } // double precision
        [Column("PROSTO_R10"), Nullable] public double? PROSTOR10 { get; set; } // double precision
        [Column("PROSTO_R11"), Nullable] public double? PROSTOR11 { get; set; } // double precision
        [Column("PROSTO_R12"), Nullable] public double? PROSTOR12 { get; set; } // double precision
        [Column("PROSTO_R13"), Nullable] public double? PROSTOR13 { get; set; } // double precision
        [Column("PROSTO_R14"), Nullable] public double? PROSTOR14 { get; set; } // double precision
        [Column("PROSTO_R15"), Nullable] public double? PROSTOR15 { get; set; } // double precision
        [Column("PROSTO_R16"), Nullable] public double? PROSTOR16 { get; set; } // double precision
        [Column("PROSTO_R17"), Nullable] public double? PROSTOR17 { get; set; } // double precision
        [Column("PROSTO_R18"), Nullable] public double? PROSTOR18 { get; set; } // double precision
        [Column("PROSTO_R19"), Nullable] public double? PROSTOR19 { get; set; } // double precision
        [Column("PROSTO_R20"), Nullable] public double? PROSTOR20 { get; set; } // double precision
        [Column("PROSTO_C1"), Nullable] public string PROSTOC1 { get; set; } // char(60)
        [Column("PROSTO_C2"), Nullable] public string PROSTOC2 { get; set; } // char(60)
        [Column("PROSTO_C3"), Nullable] public string PROSTOC3 { get; set; } // char(60)
        [Column("PROSTO_C4"), Nullable] public string PROSTOC4 { get; set; } // char(60)
        [Column("PROSTO_C5"), Nullable] public string PROSTOC5 { get; set; } // char(60)
        [Column("PROSTO_C6"), Nullable] public string PROSTOC6 { get; set; } // char(60)
        [Column("PROSTO_C7"), Nullable] public string PROSTOC7 { get; set; } // char(60)
        [Column("PROSTO_C8"), Nullable] public string PROSTOC8 { get; set; } // char(60)
        [Column("PROSTO_C9"), Nullable] public string PROSTOC9 { get; set; } // char(80)
        [Column("PROSTO_C10"), Nullable] public string PROSTOC10 { get; set; } // char(80)
        [Column("PROSTO_C11"), Nullable] public string PROSTOC11 { get; set; } // char(40)
        [Column("PROSTO_C12"), Nullable] public string PROSTOC12 { get; set; } // char(40)
        [Column("PROSTO_C13"), Nullable] public string PROSTOC13 { get; set; } // char(30)
        [Column("PROSTO_C14"), Nullable] public string PROSTOC14 { get; set; } // char(30)
        [Column("PROSTO_D1"), Nullable] public DateTime? PROSTOD1 { get; set; } // timestamp
        [Column("PROSTO_D2"), Nullable] public DateTime? PROSTOD2 { get; set; } // timestamp
        [Column("OBJAVA_B2B"), Nullable] public int? OBJAVAB2B { get; set; } // integer
        [Column("OBJAVA_B2C"), Nullable] public int? OBJAVAB2C { get; set; } // integer
        [Column("PRODAJNA_CENA_EUR"), Nullable] public double? PRODAJNACENAEUR { get; set; } // double precision
        [Column("PRODAJNA_CENA_EUR_DDV"), Nullable] public double? PRODAJNACENAEURDDV { get; set; } // double precision
        [Column(), Nullable] public int? SIFRANT1 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT2 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT3 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT4 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT5 { get; set; } // integer
        [Column("ART_BARVA"), Nullable] public string ARTBARVA { get; set; } // char(10)
        [Column("ART_VELIKOST"), Nullable] public string ARTVELIKOST { get; set; } // char(10)
        [Column(), Nullable] public int? NADARTIKEL { get; set; } // integer
        [Column("SIFRA_NADARTIKLA"), Nullable] public string SIFRANADARTIKLA { get; set; } // char(18)
        [Column("DFS_DAVEK"), Nullable] public int? DFSDAVEK { get; set; } // integer
        [Column("TRENUTNA_ZALOGA"), Nullable] public double? TRENUTNAZALOGA { get; set; } // double precision
        [Column(), Nullable] public int? SKLADISCE { get; set; } // integer
        [Column("PRODAJNA_CENA3_DDV"), Nullable] public double? PRODAJNACENA3DDV { get; set; } // double precision
        [Column("SIFRA_REDNA"), Nullable] public string SIFRAREDNA { get; set; } // char(18)
        [Column("S_MESTO"), Nullable] public int? SMESTO { get; set; } // integer
        [Column("RABAT_MAX"), Nullable] public double? RABATMAX { get; set; } // double precision
        [Column("LOK_OSNOVNIH_ENOT"), Nullable] public double? LOKOSNOVNIHENOT { get; set; } // double precision
        [Column("NACIN_VNOSA"), Nullable] public int? NACINVNOSA { get; set; } // integer
        [Column("URL_NASLOV"), Nullable] public string URLNASLOV { get; set; } // char(200)
        [Column(), Nullable] public double? DOLZINA { get; set; } // double precision
        [Column(), Nullable] public double? SIRINA { get; set; } // double precision
        [Column(), Nullable] public double? VISINA { get; set; } // double precision
        [Column("PRODAJNA_CENA4_DDV"), Nullable] public double? PRODAJNACENA4DDV { get; set; } // double precision
        [Column(), Nullable] public int? MBLAGAJNA { get; set; } // integer
        [Column("PROSTO_I3"), Nullable] public int? PROSTOI3 { get; set; } // integer
        [Column("PROSTO_I4"), Nullable] public int? PROSTOI4 { get; set; } // integer
    }


    [Table("FA_ARTIKEL")]
    public partial class FAARTIKEL_BASE
    {
        public static string DBTableName = "FA_ARTIKEL";

        [Column(), Nullable] public string SIFRA { get; set; } // char(18)
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column(), Nullable] public string NAZIV2 { get; set; } // char(40)
        [Column(), Nullable] public string ENOTA { get; set; } // char(5)
        [Column(), Nullable] public string GRUPA { get; set; } // char(15)
        [Column("STOPNJA_DDV"), Nullable] public int? STOPNJADDV { get; set; } // integer
    }
}
