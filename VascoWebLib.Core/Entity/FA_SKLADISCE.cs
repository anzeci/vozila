﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_SKLADISCE")]
    public partial class FASKLADISCE
    {
        public static string DBTableName = "FA_SKLADISCE";

        [Column(), Nullable] public int? SIFRA { get; set; } // integer
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column(), Nullable] public string NAZIV2 { get; set; } // char(40)
        [Column(), Nullable] public string NASLOV { get; set; } // char(30)
        [Column(), Nullable] public string POSTA { get; set; } // char(30)
        [Column(), Nullable] public string KONTO { get; set; } // char(8)
        [Column(), Nullable] public int? POSLOVALNICA { get; set; } // integer
        [Column("SKLADISCE_POS"), Nullable] public int? SKLADISCEPOS { get; set; } // integer
        [Column(), Nullable] public int? ZUNANJE { get; set; } // integer
        [Column("NE_VPLIVA_NA_MPC"), Nullable] public int? NEVPLIVANAMPC { get; set; } // integer
        [Column("PREPOVED_DOBAVNICA"), Nullable] public int? PREPOVEDDOBAVNICA { get; set; } // integer
        [Column("TIP_DOSTAVE"), Nullable] public int? TIPDOSTAVE { get; set; } // integer
        [Column(), Nullable] public int? KONSIGNACIJA { get; set; } // integer
        [Column(), Nullable] public int? PARTNER { get; set; } // integer
        [Column(), Nullable] public int? PRODAJALNA { get; set; } // integer
        [Column("FIRMA_KOPIJE_ODDALJENO"), Nullable] public int? FIRMAKOPIJEODDALJENO { get; set; } // integer
        [Column("KONTO_REAL_DOBAVNICA"), Nullable] public string KONTOREALDOBAVNICA { get; set; } // char(8)
        [Column("KONTO_REAL_DN"), Nullable] public string KONTOREALDN { get; set; } // char(8)
        [Column("KONTO_REAL_PARAGON"), Nullable] public string KONTOREALPARAGON { get; set; } // char(8)
        [Column("S_MESTO"), Nullable] public int? SMESTO { get; set; } // integer
        [Column("ZACETEK_STEVILCENJA"), Nullable] public string ZACETEKSTEVILCENJA { get; set; } // char(8)
        [Column("CARINSKO_SKLADISCE"), Nullable] public int? CARINSKOSKLADISCE { get; set; } // integer
        [Column("TROSARINSKO_SKLADISCE"), Nullable] public int? TROSARINSKOSKLADISCE { get; set; } // integer
        [Column("SKLADISCE_ZAMRZNJENI_PROGRAM"), Nullable] public int? SKLADISCEZAMRZNJENIPROGRAM { get; set; } // integer
        [Column("VRSTA_CENE_PARAGON"), Nullable] public int? VRSTACENEPARAGON { get; set; } // integer
        [Column("VNASA_SE_MIN_MAX_ZALOGA"), Nullable] public int? VNASASEMINMAXZALOGA { get; set; } // integer
        [Column("ZALOGA_ZA_SPLET"), Nullable] public int? ZALOGAZASPLET { get; set; } // integer
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column("ZAL_1"), Nullable] public int? ZAL1 { get; set; } // integer
        [Column("NAR_1"), Nullable] public int? NAR1 { get; set; } // integer
        [Column("REZ_1"), Nullable] public int? REZ1 { get; set; } // integer
        [Column("RRR_1"), Nullable] public int? RRR1 { get; set; } // integer
        [Column("ZAL_2"), Nullable] public int? ZAL2 { get; set; } // integer
        [Column("NAR_2"), Nullable] public int? NAR2 { get; set; } // integer
        [Column("REZ_2"), Nullable] public int? REZ2 { get; set; } // integer
        [Column("RRR_2"), Nullable] public int? RRR2 { get; set; } // integer
        [Column("ZAL_3"), Nullable] public int? ZAL3 { get; set; } // integer
        [Column("NAR_3"), Nullable] public int? NAR3 { get; set; } // integer
        [Column("REZ_3"), Nullable] public int? REZ3 { get; set; } // integer
        [Column("RRR_3"), Nullable] public int? RRR3 { get; set; } // integer
        [Column("ZAL_4"), Nullable] public int? ZAL4 { get; set; } // integer
        [Column("NAR_4"), Nullable] public int? NAR4 { get; set; } // integer
        [Column("REZ_4"), Nullable] public int? REZ4 { get; set; } // integer
        [Column("RRR_4"), Nullable] public int? RRR4 { get; set; } // integer
        [Column("ZAL_5"), Nullable] public int? ZAL5 { get; set; } // integer
        [Column("NAR_5"), Nullable] public int? NAR5 { get; set; } // integer
        [Column("REZ_5"), Nullable] public int? REZ5 { get; set; } // integer
        [Column("RRR_5"), Nullable] public int? RRR5 { get; set; } // integer
        [Column("ZAL_6"), Nullable] public int? ZAL6 { get; set; } // integer
        [Column("NAR_6"), Nullable] public int? NAR6 { get; set; } // integer
        [Column("REZ_6"), Nullable] public int? REZ6 { get; set; } // integer
        [Column("RRR_6"), Nullable] public int? RRR6 { get; set; } // integer
        [Column("ZAL_7"), Nullable] public int? ZAL7 { get; set; } // integer
        [Column("NAR_7"), Nullable] public int? NAR7 { get; set; } // integer
        [Column("REZ_7"), Nullable] public int? REZ7 { get; set; } // integer
        [Column("RRR_7"), Nullable] public int? RRR7 { get; set; } // integer
        [Column("ZAL_8"), Nullable] public int? ZAL8 { get; set; } // integer
        [Column("NAR_8"), Nullable] public int? NAR8 { get; set; } // integer
        [Column("REZ_8"), Nullable] public int? REZ8 { get; set; } // integer
        [Column("RRR_8"), Nullable] public int? RRR8 { get; set; } // integer
        [Column("MP_SKLADISCE"), Nullable] public int? MPSKLADISCE { get; set; } // integer
        [Column("EMAIL_IMAS_ROBO"), Nullable] public string EMAILIMASROBO { get; set; } // char(80)
        [Column("UREJENOST_FIFO_METODA"), Nullable] public int? UREJENOSTFIFOMETODA { get; set; } // integer
        [Column("LOKACIJSKO_SKLADISCE"), Nullable] public int? LOKACIJSKOSKLADISCE { get; set; } // integer
        [Column("E_POSTA"), Nullable] public string EPOSTA { get; set; } // char(80)
        [Column("KONTO_NABAVNA"), Nullable] public string KONTONABAVNA { get; set; } // char(8)
        [Column("SKLADISCE_ZA_NAR_KUP"), Nullable] public int? SKLADISCEZANARKUP { get; set; } // integer
        [Column("STOLPEC_Z_ZALOGO"), Nullable] public int? STOLPECZZALOGO { get; set; } // integer
        [Column("DODATNO_SKLADISCE_SKLADISCA"), Nullable] public int? DODATNOSKLADISCESKLADISCA { get; set; } // integer
        [Column("LOTI_NA_TEM_SKLADISCU"), Nullable] public int? LOTINATEMSKLADISCU { get; set; } // integer
        [Column("PROCENT_OSNOVNE_CENE"), Nullable] public double? PROCENTOSNOVNECENE { get; set; } // double precision
        [Column("XML_POSLOVALNICA"), Nullable] public int? XMLPOSLOVALNICA { get; set; } // integer
        [Column("AVTOMATSKA_POTRDITEV"), Nullable] public int? AVTOMATSKAPOTRDITEV { get; set; } // integer
        [Column("PARAGON_DOKUMENT_PREFIX"), Nullable] public string PARAGONDOKUMENTPREFIX { get; set; } // char(20)
        [Column("N_ZAL_1"), Nullable] public int? NZAL1 { get; set; } // integer
        [Column("N_ZAL_2"), Nullable] public int? NZAL2 { get; set; } // integer
        [Column("N_ZAL_3"), Nullable] public int? NZAL3 { get; set; } // integer
        [Column("N_ZAL_4"), Nullable] public int? NZAL4 { get; set; } // integer
        [Column("N_ZAL_5"), Nullable] public int? NZAL5 { get; set; } // integer
        [Column("N_ZAL_6"), Nullable] public int? NZAL6 { get; set; } // integer
        [Column("N_ZAL_7"), Nullable] public int? NZAL7 { get; set; } // integer
        [Column("N_ZAL_8"), Nullable] public int? NZAL8 { get; set; } // integer
        [Column("PROSTO_C1"), Nullable] public string PROSTOC1 { get; set; } // char(200)
        [Column("PROSTO_C2"), Nullable] public string PROSTOC2 { get; set; } // char(200)
        [Column("PROSTO_C3"), Nullable] public string PROSTOC3 { get; set; } // char(200)
        [Column("PROSTO_C4"), Nullable] public string PROSTOC4 { get; set; } // char(80)
        [Column("PROSTO_C5"), Nullable] public string PROSTOC5 { get; set; } // char(80)
        [Column("PROSTO_C6"), Nullable] public string PROSTOC6 { get; set; } // char(80)
        [Column("PROSTO_I1"), Nullable] public int? PROSTOI1 { get; set; } // integer
        [Column("PROSTO_I2"), Nullable] public int? PROSTOI2 { get; set; } // integer
        [Column("PROSTO_I3"), Nullable] public int? PROSTOI3 { get; set; } // integer
        [Column("PROSTO_I4"), Nullable] public int? PROSTOI4 { get; set; } // integer
        [Column("PROSTO_I5"), Nullable] public int? PROSTOI5 { get; set; } // integer
        [Column("PROSTO_I6"), Nullable] public int? PROSTOI6 { get; set; } // integer
        [Column("PROSTO_R1"), Nullable] public double? PROSTOR1 { get; set; } // double precision
        [Column("PROSTO_R2"), Nullable] public double? PROSTOR2 { get; set; } // double precision
        [Column("PROSTO_R3"), Nullable] public double? PROSTOR3 { get; set; } // double precision
        [Column("PROSTO_R4"), Nullable] public double? PROSTOR4 { get; set; } // double precision
        [Column("PROSTO_R5"), Nullable] public double? PROSTOR5 { get; set; } // double precision
        [Column("PROSTO_R6"), Nullable] public double? PROSTOR6 { get; set; } // double precision
        [Column("NE_UPOSTEVA_PRI_SPR_CENE"), Nullable] public int? NEUPOSTEVAPRISPRCENE { get; set; } // integer
        [Column("PROSTO_D1"), Nullable] public DateTime? PROSTOD1 { get; set; } // timestamp
        [Column("PROSTO_D2"), Nullable] public DateTime? PROSTOD2 { get; set; } // timestamp
        [Column("KONSIG_SKLADISCE_PREVZEMA"), Nullable] public int? KONSIGSKLADISCEPREVZEMA { get; set; } // integer
        [Column("KONSIG_DOD_OD_SKL"), Nullable] public int? KONSIGDODODSKL { get; set; } // integer
        [Column("KONSIG_DOD_DO_SKL"), Nullable] public int? KONSIGDODDOSKL { get; set; } // integer
        [Column("ZACASNO_SKLADISCE_SKLADISCA"), Nullable] public int? ZACASNOSKLADISCESKLADISCA { get; set; } // integer
        [Column("SKLADISCA_NE"), Nullable] public string SKLADISCANE { get; set; } // char(250)
    }
}
