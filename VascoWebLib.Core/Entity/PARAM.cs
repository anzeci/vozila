using LinqToDB.Mapping;
using System;
using System.Globalization;
using VascoWebLib.Core.Extensions;

namespace VascoWebLib.Core.Entity
{
    [Table(Name = "PARAM")]
    public class PARAM
    {
        public static string DBTableName = "PARAM";

        [Column] public int ID_PARAM { get; set; }
        [Column] public string IME_PROGRAMA { get; set; }
        [Column] public string OPIS { get; set; }
        [Column] public string VREDNOST { get; set; }
        [Column] public string TIP { get; set; }

        public bool IsBool
        {
            get
            {
                return TIP.Equals("L") || TIP.Equals("B");
            }
        }

        public bool AsBoolean
        {
            get
            {
                return VREDNOST.Equals("1");
            }
        }

        public bool IsString
        {
            get
            {
                return TIP.Equals("C") || string.IsNullOrEmpty(TIP);
            }
        }

        public string AsString
        {
            get
            {
                return VREDNOST;
            }
        }

        public bool IsDate
        {
            get
            {
                return TIP.Equals("D");
            }
        }

        public DateTime? AsDate
        {
            get
            {
                if (VREDNOST.Equals("0"))
                    return null;

                if (DateTime.TryParseExact(VREDNOST, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out DateTime dt))
                    return dt;

                return null;
            }
        }

        public bool IsDateTime
        {
            get
            {
                return TIP.Equals("T");
            }
        }

        public DateTime? AsDateTime
        {
            get
            {
                if (VREDNOST.Equals("0"))
                    return null;

                if (DateTime.TryParseExact(VREDNOST, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out DateTime dt))
                    return dt;

                return null;
            }
        }

        public bool IsInteger
        {
            get
            {
                return TIP.Equals("I");
            }
        }

        public int AsInteger
        {
            get
            {
                int.TryParse(VREDNOST, out int result);
                return result;
            }
        }

        public bool IsDouble
        {
            get
            {
                return TIP.Equals("R");
            }
        }

        public double AsDouble
        {
            get
            {
                return VREDNOST.FromStringSlo();
            }
        }

        public override string ToString()
        {
            return string.Format("ID_PARAM: {0}, IME_PROGRAMA: {1}, OPIS: {2}, VREDNOST: {3}, TIP:{4}", ID_PARAM, IME_PROGRAMA, OPIS, VREDNOST, TIP);
        }
    }
}