﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_GRUPA")]
    public partial class FAGRUPA
    {
        public static string DBTableName = "FA_GRUPA";

        [Column(), Nullable] public string SIFRA { get; set; } // char(15)
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column(), Nullable] public string KONTO { get; set; } // char(8)
        [Column("KONTO_IZVOZ"), Nullable] public string KONTOIZVOZ { get; set; } // char(8)
        [Column("KONTO_REAL_NEZAVEZANCI"), Nullable] public string KONTOREALNEZAVEZANCI { get; set; } // char(8)
        [Column("KONTO_REAL_PARAGONI"), Nullable] public string KONTOREALPARAGONI { get; set; } // char(8)
        [Column("GOVORECA_SIFRA"), Nullable] public int? GOVORECASIFRA { get; set; } // integer
        [Column(), Nullable] public string NADGRUPA { get; set; } // char(6)
        [Column("S_MESTO"), Nullable] public int? SMESTO { get; set; } // integer
        [Column("SPREMINJAM_PRODAJNO_CENO"), Nullable] public int? SPREMINJAMPRODAJNOCENO { get; set; } // integer
        [Column("MAX_KOLICINA"), Nullable] public double? MAXKOLICINA { get; set; } // double precision
        [Column("MALOPRODAJNI_RABAT"), Nullable] public double? MALOPRODAJNIRABAT { get; set; } // double precision
        [Column("PROC_PRICAKOVANE_MARZE"), Nullable] public double? PROCPRICAKOVANEMARZE { get; set; } // double precision
        [Column("TROSARINSKI_ARTIKLI"), Nullable] public int? TROSARINSKIARTIKLI { get; set; } // integer
        [Column("POT_DO_SLIKE"), Nullable] public string POTDOSLIKE { get; set; } // char(80)
        [Column("DOBAVITELJ_SKUPINE"), Nullable] public int? DOBAVITELJSKUPINE { get; set; } // integer
        [Column("NI_CASASCONTO_PARTNER"), Nullable] public int? NICASASCONTOPARTNER { get; set; } // integer
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column("OBJAVA_B2B"), Nullable] public int? OBJAVAB2B { get; set; } // integer
        [Column("OBJAVA_B2C"), Nullable] public int? OBJAVAB2C { get; set; } // integer
        [Column(), Nullable] public int? IZBRAN1 { get; set; } // integer
        [Column(), Nullable] public int? IZBRAN2 { get; set; } // integer
        [Column("PROSTO_I1"), Nullable] public int? PROSTOI1 { get; set; } // integer
        [Column("PROSTO_I2"), Nullable] public int? PROSTOI2 { get; set; } // integer
        [Column("PROSTO_R1"), Nullable] public double? PROSTOR1 { get; set; } // double precision
        [Column("PROSTO_R2"), Nullable] public double? PROSTOR2 { get; set; } // double precision
        [Column("PROSTO_C1"), Nullable] public string PROSTOC1 { get; set; } // char(80)
        [Column("PROSTO_C2"), Nullable] public string PROSTOC2 { get; set; } // char(80)
        [Column("KONTO_EU"), Nullable] public string KONTOEU { get; set; } // char(8)
        [Column(), Nullable] public double? RABAT { get; set; } // double precision
        [Column("MAX_RABAT"), Nullable] public double? MAXRABAT { get; set; } // double precision
        [Column("BONUS_PROCENT"), Nullable] public double? BONUSPROCENT { get; set; } // double precision
        [Column("KOMISIJSKI_ARTIKLI"), Nullable] public int? KOMISIJSKIARTIKLI { get; set; } // integer
        [Column("VRSTA_MATERIALA"), Nullable] public int? VRSTAMATERIALA { get; set; } // integer
        [Column("LOT_NACIN"), Nullable] public int? LOTNACIN { get; set; } // integer
        [Column(), Nullable] public string OPIS { get; set; } // blob sub_type 1
        [Column("PROSTO_R3"), Nullable] public double? PROSTOR3 { get; set; } // double precision
        [Column("PROSTO_R4"), Nullable] public double? PROSTOR4 { get; set; } // double precision
        [Column("PROSTO_R5"), Nullable] public double? PROSTOR5 { get; set; } // double precision
        [Column("PROSTO_R6"), Nullable] public double? PROSTOR6 { get; set; } // double precision
        [Column("PROSTO_R7"), Nullable] public double? PROSTOR7 { get; set; } // double precision
        [Column("PROSTO_R8"), Nullable] public double? PROSTOR8 { get; set; } // double precision
        [Column("PROSTO_R9"), Nullable] public double? PROSTOR9 { get; set; } // double precision
        [Column("PROSTO_R10"), Nullable] public double? PROSTOR10 { get; set; } // double precision
        [Column("DOLZINA_IMENA_SLIKE"), Nullable] public int? DOLZINAIMENASLIKE { get; set; } // integer
        [Column("KONTO_TROSARINA"), Nullable] public string KONTOTROSARINA { get; set; } // char(8)
        [Column(), Nullable] public string KOMERCIALIST { get; set; } // char(5)
    }
}
