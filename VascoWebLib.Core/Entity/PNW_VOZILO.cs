using LinqToDB.Mapping;
using System;
using System.ComponentModel;

namespace VascoWebLib.Core.Entity
{
    [Table("PNW_VOZILO")]
    public partial class PNW_VOZILO
    {
        [Column, Nullable, PrimaryKey, DisplayName("�ifra")] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Opis")] public string OPIS { get; set; } // char(50)
        [Column, Nullable, DisplayName("Opis 2")] public string OPIS2 { get; set; } // char(50)
        [Column, Nullable, DisplayName("Registrska")] public string REGISTERKSA { get; set; } // char(10)
        [Column, Nullable, DisplayName("Slu�beno vozilo")] public int? SLUZBENO { get; set; } // integer
        [Column, Nullable, DisplayName("Datum registracije")] public DateTime? REGISTRACIJA { get; set; } // timestamp
        [Column, Nullable, DisplayName("Datum pregleda")] public DateTime? PREGLED { get; set; } // timestamp
        [Column, Nullable, DisplayName("Neaktivno vozilo")] public int? NEAKTIVNO { get; set; } // integer
        [Column, Nullable, DisplayName("Opomba")] public string OPOMBA { get; set; } // blob sub_type 1
        [Column, Nullable, DisplayName("Stalna uporaba lastnega vozila")] public int? STALNA_UPORABA { get; set; } // integer
        public override string ToString()
        {
            return $"({SIFRA}) {OPIS}";
        }
    }

    [Table("PNW_VOZILO")]
    public partial class PNW_VOZILO_MIN
    {
        [Column, Nullable, PrimaryKey, DisplayName("�ifra")] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Opis")] public string OPIS { get; set; } // char(50)
        public override string ToString()
        {
            return $"({SIFRA}) {OPIS}";
        }
    }
}
