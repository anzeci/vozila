using LinqToDB.Mapping;
using System;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_NAR_KUPCI_KNJ")]
    public partial class FA_NAR_KUPCI_KNJ
    {
        [Column, Nullable, PrimaryKey] public int? AUTO_ID { get; set; } // integer
        [Column, Nullable] public string STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? LETO { get; set; } // integer
        [Column, Nullable] public DateTime? PREDVID_DOBAVA { get; set; } // timestamp
        [Column, Nullable] public int? SKLADISCE { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM { get; set; } // timestamp
        [Column, Nullable] public int? ZS { get; set; } // integer
        [Column, Nullable] public string ID_MASTER { get; set; } // char(50)
        [Column, Nullable] public string SIFRA { get; set; } // char(18)
        [Column, Nullable] public string NAZIV { get; set; } // char(40)
        [Column, Nullable] public double? KOLICINA { get; set; } // double precision
        [Column, Nullable] public double? KOLICINA_POTRJENA { get; set; } // double precision
        [Column, Nullable] public int? AKCIJA { get; set; } // integer
        [Column, Nullable] public int? ZAPRTO { get; set; } // integer
        [Column, Nullable] public int? PROIZVOD_NALOG { get; set; } // integer
        [Column, Nullable] public int? PROIZVOD_NALOG_LETO { get; set; } // integer
        [Column, Nullable] public string OPIS { get; set; } // blob sub_type 1
        [Column, Nullable] public int? STOPNJA_DDV { get; set; } // integer
        [Column, Nullable] public int? STEVILKA_DELOVNEGA_NALOGA { get; set; } // integer
        [Column, Nullable] public string LOT { get; set; } // char(50)
        [Column, Nullable] public double? MP_MARZA { get; set; } // double precision
        [Column, Nullable] public double? MP_CENA_BREZ_DDV { get; set; } // double precision
        [Column, Nullable] public double? PRODAJNA_CENA { get; set; } // double precision
        [Column, Nullable] public double? PRODAJNA_VREDNOST { get; set; } // double precision
        [Column, Nullable] public double? NETO_CENA { get; set; } // double precision
        [Column, Nullable] public double? TROSARINA { get; set; } // double precision
        [Column, Nullable] public int? TIP_POSTAVKE { get; set; } // integer
        [Column, Nullable] public double? PRODAJNA_CENA_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? PRODAJNA_VREDNOST_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? CENA { get; set; } // double precision
        [Column, Nullable] public double? VREDNOST { get; set; } // double precision
        [Column, Nullable] public double? CENA_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? VREDNOST_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? DEVIZE_PRODAJNA_CENA { get; set; } // double precision
        [Column, Nullable] public double? DEVIZE_PRODAJNA_VREDNOST { get; set; } // double precision
        [Column, Nullable] public double? RABAT1 { get; set; } // double precision
        [Column, Nullable] public double? RABAT2 { get; set; } // double precision
        [Column, Nullable] public double? RABAT3 { get; set; } // double precision
        [Column, Nullable] public double? RABAT_V_ZNESKU { get; set; } // double precision
        [Column, Nullable] public string RIP_IZMENJA { get; set; } // char(14)
        [Column, Nullable] public string RIP_OPOMBA { get; set; } // char(70)
        [Column, Nullable] public string RIP_P_RAZLOG { get; set; } // char(3)
        [Column, Nullable] public string RIP_EAN_UPC_A { get; set; } // char(35)
        [Column, Nullable] public string RIP_V_ST_KODE { get; set; } // char(3)
        [Column, Nullable] public string RIP_DOB_SIF_A { get; set; } // char(35)
        [Column, Nullable] public string RIP_INT_SIF_A { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_ART_1 { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_ART_2 { get; set; } // char(35)
        [Column, Nullable] public string RIP_E_MERE { get; set; } // char(3)
        [Column, Nullable] public DateTime? RIP_DAT_UPORA { get; set; } // timestamp
        [Column, Nullable] public double? RIP_CENA_ART { get; set; } // double precision
        [Column, Nullable] public string RIP_PAK_ZAH1 { get; set; } // char(3)
        [Column, Nullable] public string RIP_PAK_ZAH2 { get; set; } // char(3)
        [Column, Nullable] public string RIP_PAK_ZAH3 { get; set; } // char(3)
        [Column, Nullable] public double? PROSTO_R1 { get; set; } // double precision
        [Column, Nullable] public double? PROSTO_R2 { get; set; } // double precision
        [Column, Nullable] public double? PROSTO_R3 { get; set; } // double precision
        [Column, Nullable] public string PROSTO_C1 { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C2 { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C3 { get; set; } // char(60)
        [Column, Nullable] public string EBA_NAROCILO { get; set; } // blob sub_type 1
        [Column, Nullable] public DateTime? DATUM_VELJAVNOSTI { get; set; } // timestamp
        [Column, Nullable] public string NAR_DOB_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? NAR_DOB_LETO { get; set; } // integer
        [Column, Nullable] public int? NAR_DOB_ZS { get; set; } // integer
        [Column, Nullable] public double? KOLICINA_PRETVORNIK { get; set; } // double precision
        [Column, Nullable] public double? KARTONI { get; set; } // double precision
        [Column, Nullable] public double? PALETE { get; set; } // double precision
        [Column, Nullable] public string OPIS_2 { get; set; } // blob sub_type 1
        [Column, Nullable] public double? NABAVNA_CENA { get; set; } // double precision
        [Column, Nullable] public double? NABAVNA_VREDNOST { get; set; } // double precision
        [Column, Nullable] public string NABAVNA_CENA_OPIS { get; set; } // char(60)
        [Column, Nullable] public string NABAVNA_CENA_OPOMBA { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C4 { get; set; } // char(60)
        [Column, Nullable] public int? PROSTO_I1 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I2 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I3 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I4 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I5 { get; set; } // integer
        [Column, Nullable] public DateTime? PROSTO_D1 { get; set; } // timestamp
        [Column, Nullable] public DateTime? PROSTO_D2 { get; set; } // timestamp
        [Column, Nullable] public DateTime? PROSTO_D3 { get; set; } // timestamp
        [Column, Nullable] public string NAPRAVA { get; set; } // char(20)
        [Column, Nullable] public int? ZAPRTO_POS { get; set; } // integer
        [Column, Nullable] public int? KONCNA_CENA { get; set; } // integer
        [Column, Nullable] public string TERMINAL_ID { get; set; } // char(30)
        [Column, Nullable] public string TERMINAL_DOK { get; set; } // char(40)
        [Column, Nullable] public DateTime? TERMINAL_DATUM { get; set; } // timestamp
        [Column, Nullable] public string ORG_UPORABNIK { get; set; } // char(20)
        [Column, Nullable] public int? ORG_ID_UPORABNIK { get; set; } // integer
        [Column, Nullable] public DateTime? ORG_KDAJ { get; set; } // timestamp
        [Column, Nullable] public string ORG_POSTAJA { get; set; } // char(20)
        [Column, Nullable] public DateTime? POP_KDAJ { get; set; } // timestamp
        [Column, Nullable] public string RAZREZ_MEM { get; set; } // blob sub_type 1
        [Column, Nullable] public double? PROSTO_R4 { get; set; } // double precision
        [Column, Nullable] public double? PROSTO_R5 { get; set; } // double precision
        [Column, Nullable] public string RIP_VRSTA { get; set; } // char(20)
        [Column, Nullable] public int? OBVEZNA_KOLICINA { get; set; } // integer
    }


    [Table("FA_NAR_KUPCI_KNJ")]
    public partial class FA_NAR_KUPCI_KNJ_POSTAVKE
    {
        [Column, Nullable, PrimaryKey] public int? AUTO_ID { get; set; } // integer
        [Column, Nullable] public string STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? LETO { get; set; } // integer
        [Column, Nullable] public DateTime? PREDVID_DOBAVA { get; set; } // timestamp
        
        [Column, Nullable] public int? SKLADISCE { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM { get; set; } // timestamp
        [Column, Nullable] public int? ZS { get; set; } // integer
        [Column, Nullable] public string SIFRA { get; set; } // char(18)
        [Column, Nullable] public string NAZIV { get; set; } // char(40)
        [Column, Nullable] public double? KOLICINA { get; set; } // double precision
        [Column, Nullable] public double? KOLICINA_POTRJENA { get; set; } // double precision
        [Column, Nullable] public string OPIS { get; set; } // blob sub_type 1
        [Column, Nullable] public int? STOPNJA_DDV { get; set; } // integer
        [Column, Nullable] public double? PRODAJNA_CENA { get; set; } // double precision
        [Column, Nullable] public double? PRODAJNA_VREDNOST { get; set; } // double precision
        [Column, Nullable] public double? NETO_CENA { get; set; } // double precision
        [Column, Nullable] public double? PRODAJNA_CENA_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? PRODAJNA_VREDNOST_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? CENA { get; set; } // double precision
        [Column, Nullable] public double? VREDNOST { get; set; } // double precision
        [Column, Nullable] public double? CENA_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? VREDNOST_Z_DDV { get; set; } // double precision
        [Column, Nullable] public double? RABAT1 { get; set; } // double precision
        [Column, Nullable] public double? RABAT_V_ZNESKU { get; set; } // double precision
        [Column, Nullable] public DateTime? DATUM_VELJAVNOSTI { get; set; } // timestamp


        [NotColumn] public double? DOBAVLJENA_KOLICINA { get; set; } // double precision

        /// <summary>
        /// Stopnja DDV v procentih
        /// </summary>
        public double DDV
        {
            get
            {
                switch (STOPNJA_DDV)
                {
                    case 0:
                        return 22.0;
                    case 1:
                        return 9.5;
                    default:
                        return 0;
                }
            }
        }

        [Association(ThisKey = "SIFRA", OtherKey = "SIFRA")]
        public virtual FAARTIKEL_BASE ARTIKEL { get; set; }
        
    }

}
