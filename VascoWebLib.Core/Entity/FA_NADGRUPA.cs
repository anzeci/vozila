﻿using LinqToDB.Mapping;
using System;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_NADGRUPA")]
    public partial class FANADGRUPA
    {
        public static string DBTableName = "FA_NADGRUPA";

        [Column(), Nullable] public string SIFRA { get; set; } // char(6)
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column("OBJAVA_B2B"), Nullable] public int? OBJAVAB2B { get; set; } // integer
        [Column("OBJAVA_B2C"), Nullable] public int? OBJAVAB2C { get; set; } // integer
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column(), Nullable] public string OPIS { get; set; } // blob sub_type 1
        [Column("BL_SKUPINA"), Nullable] public int? BLSKUPINA { get; set; } // integer
        [Column("BONUS_PROCENT"), Nullable] public double? BONUSPROCENT { get; set; } // double precision
        [Column("MAX_RABAT"), Nullable] public double? MAXRABAT { get; set; } // double precision
        [Column("PROSTO_I1"), Nullable] public int? PROSTOI1 { get; set; } // integer
        [Column("PROSTO_I2"), Nullable] public int? PROSTOI2 { get; set; } // integer
        [Column("PROSTO_R1"), Nullable] public double? PROSTOR1 { get; set; } // double precision
        [Column("PROSTO_R2"), Nullable] public double? PROSTOR2 { get; set; } // double precision
        [Column("PROSTO_R3"), Nullable] public double? PROSTOR3 { get; set; } // double precision
        [Column("PROSTO_R4"), Nullable] public double? PROSTOR4 { get; set; } // double precision
        [Column("PROSTO_R5"), Nullable] public double? PROSTOR5 { get; set; } // double precision
        [Column("PROSTO_R6"), Nullable] public double? PROSTOR6 { get; set; } // double precision
        [Column("PROSTO_R7"), Nullable] public double? PROSTOR7 { get; set; } // double precision
        [Column("PROSTO_R8"), Nullable] public double? PROSTOR8 { get; set; } // double precision
        [Column("PROSTO_R9"), Nullable] public double? PROSTOR9 { get; set; } // double precision
        [Column("PROSTO_R10"), Nullable] public double? PROSTOR10 { get; set; } // double precision
        [Column("PROSTO_C1"), Nullable] public string PROSTOC1 { get; set; } // char(80)
        [Column("PROSTO_C2"), Nullable] public string PROSTOC2 { get; set; } // char(80)
    }
}
