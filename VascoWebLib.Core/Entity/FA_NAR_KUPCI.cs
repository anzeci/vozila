using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_NAR_KUPCI")]
    public partial class FA_NAR_KUPCI_GLAVA
    {
        [Column, Nullable,PrimaryKey(0)] public string STEVILKA { get; set; } // char(8)
        [Column, Nullable,PrimaryKey(1)] public int? LETO { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM { get; set; } // timestamp
        [Column, Nullable] public string KOMERCIALIST { get; set; } // char(5)
        [Column, Nullable] public int? PARTNER { get; set; } // integer
        [Column, Nullable] public int? PRODAJALNA { get; set; } // integer
        [Column, Nullable] public int? SKLADISCE { get; set; } // integer
        [Column, Nullable] public int? KATERI_DDV { get; set; } // integer
        [Column, Nullable] public string PREJEMNIK_NAZIV { get; set; } // char(40)
        [Column, Nullable] public string PREJEMNIK_NAZIV2 { get; set; } // char(40)
        [Column, Nullable] public string PREJEMNIK_NASLOV { get; set; } // char(30)
        [Column, Nullable] public string PREJEMNIK_POSTA { get; set; } // char(30)
        [Column, Nullable] public double? RABAT1 { get; set; } // double precision
        [Column, Nullable] public double? RABAT2 { get; set; } // double precision
        [Column, Nullable] public double? RABAT3 { get; set; } // double precision
        [Column, Nullable] public int? VRSTA_CENE { get; set; } // integer
        [Column, Nullable] public DateTime? PREDVID_DOBAVA { get; set; } // timestamp
        [Column, Nullable] public int? INTERNET { get; set; } // integer
        [Column, Nullable] public int? OBDELAN { get; set; } // integer
        [Column, Nullable] public int? ZAPRTO { get; set; } // integer
        [Column, Nullable] public string BESEDILO { get; set; } // blob sub_type 1
        [Column, Nullable] public int? REZERVACIJA_ZALOGE { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM_VELJAVNOSTI { get; set; } // timestamp
        [Column, Nullable] public string POTNIK { get; set; } // char(5)
        [Column, Nullable] public string KOMISIONAR { get; set; } // char(5)
        [Column, Nullable] public double? ZNESEK { get; set; } // double precision
        [Column, Nullable] public double? ZNESEK_KONCNI { get; set; } // double precision
    }


    [Table("FA_NAR_KUPCI")]
    public partial class FA_NAR_KUPCI
    {
        [Column, Nullable, PrimaryKey(0)] public string STEVILKA { get; set; } // char(8)
        [Column, Nullable, PrimaryKey(1)] public int? LETO { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM { get; set; } // timestamp
        [Column, Nullable] public string KOMERCIALIST { get; set; } // char(5)
        [Column, Nullable] public int? PARTNER { get; set; } // integer
        [Column, Nullable] public int? S_MESTO { get; set; } // integer
        [Column, Nullable] public int? NAR_DOB_PARTNER { get; set; } // integer
        [Column, Nullable] public int? PRODAJALNA { get; set; } // integer
        [Column, Nullable] public int? SKLADISCE { get; set; } // integer
        [Column, Nullable] public int? KATERI_DDV { get; set; } // integer
        [Column, Nullable] public string PREJEMNIK_NAZIV { get; set; } // char(40)
        [Column, Nullable] public string PREJEMNIK_NAZIV2 { get; set; } // char(40)
        [Column, Nullable] public string PREJEMNIK_NASLOV { get; set; } // char(30)
        [Column, Nullable] public string PREJEMNIK_POSTA { get; set; } // char(30)
        [Column, Nullable] public double? RABAT1 { get; set; } // double precision
        [Column, Nullable] public double? RABAT2 { get; set; } // double precision
        [Column, Nullable] public double? RABAT3 { get; set; } // double precision
        [Column, Nullable] public int? VRSTA_CENE { get; set; } // integer
        [Column, Nullable] public int? SIFRA_VALUTE { get; set; } // integer
        [Column, Nullable] public double? TECAJ { get; set; } // double precision
        [Column, Nullable] public DateTime? PREDVID_DOBAVA { get; set; } // timestamp
        [Column, Nullable] public int? TEKST1 { get; set; } // integer
        [Column, Nullable] public int? TEKST2 { get; set; } // integer
        [Column, Nullable] public int? TEKST3 { get; set; } // integer
        [Column, Nullable] public string POPRAVLJA_DOKUMENT { get; set; } // char(50)
        [Column, Nullable] public int? NACIN_POSILJANJA { get; set; } // integer
        [Column, Nullable] public int? PRIORITETA { get; set; } // integer
        [Column, Nullable] public int? ROK_DOBAVE { get; set; } // integer
        [Column, Nullable] public DateTime? XML_DATUM_PRENOSA { get; set; } // timestamp
        [Column, Nullable] public DateTime? XML_DATUM_POTRDITVE { get; set; } // timestamp
        [Column, Nullable] public DateTime? PRENOS_DATUM_URA_POSLJI { get; set; } // timestamp
        [Column, Nullable] public DateTime? PRENOS_DATUM_URA_SPREJMI { get; set; } // timestamp
        [Column, Nullable] public string STEVILKA_LN { get; set; } // char(8)
        [Column, Nullable] public int? LETO_LN { get; set; } // integer
        [Column, Nullable] public int? STANJE_POTRDITVE { get; set; } // integer
        [Column, Nullable] public double? PROCENT_POTRDITVE { get; set; } // double precision
        [Column, Nullable] public double? PROCENT_DOBAVE { get; set; } // double precision
        [Column, Nullable] public string NAROCILO_STEVILKA { get; set; } // char(40)
        [Column, Nullable] public int? SIFRA_ODPREME { get; set; } // integer
        [Column, Nullable] public int? SIFRA_PLACILA { get; set; } // integer
        [Column, Nullable] public string PREDRAC_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? PREDRAC_LETO { get; set; } // integer
        [Column, Nullable] public int? INTERNET { get; set; } // integer
        [Column, Nullable] public int? OBDELAN { get; set; } // integer
        [Column, Nullable] public int? ZAPRTO { get; set; } // integer
        [Column, Nullable] public string BESEDILO { get; set; } // blob sub_type 1
        [Column, Nullable] public int? RIP_POTRDITEV { get; set; } // integer
        [Column, Nullable] public DateTime? RIP_DATUM_POTRDITVE_1 { get; set; } // timestamp
        [Column, Nullable] public DateTime? RIP_DATUM_POTRDITVE_2 { get; set; } // timestamp
        [Column, Nullable] public string RIP_ST_IZMENJA { get; set; } // char(14)
        [Column, Nullable] public string RIP_EAN_LOK_ME { get; set; } // char(35)
        [Column, Nullable] public string RIP_EAN_LOK_PR { get; set; } // char(35)
        [Column, Nullable] public string RIP_ST_DOKUMEN { get; set; } // char(35)
        [Column, Nullable] public string RIP_ZAP_ST_NAR { get; set; } // char(14)
        [Column, Nullable] public DateTime? RIP_DAT_CAS_N { get; set; } // timestamp
        [Column, Nullable] public DateTime? RIP_DAT_CAS_D { get; set; } // timestamp
        [Column, Nullable] public string RIP_EAN_KUP { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_KUP { get; set; } // char(35)
        [Column, Nullable] public string RIP_EAN_MD { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_MD { get; set; } // char(35)
        [Column, Nullable] public string RIP_EAN_OE_D { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_OE_D { get; set; } // char(35)
        [Column, Nullable] public string RIP_ST_PROM { get; set; } // char(35)
        [Column, Nullable] public string RIP_EAN_NAR { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_NAR { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAC_DOST { get; set; } // char(3)
        [Column, Nullable] public string RIP_BESEDILO { get; set; } // blob sub_type 1
        [Column, Nullable] public int? DIREKT { get; set; } // integer
        [Column, Nullable] public int? DIREKT_DOBAVITELJ { get; set; } // integer
        [Column, Nullable] public int? DIREKT_DOSTAVNA_SLUZBA { get; set; } // integer
        [Column, Nullable] public int? DIREKT_VRSTA_IZPISA { get; set; } // integer
        [Column, Nullable] public int? DIREKT_SIFRA_SPREJEMNE_POSTE { get; set; } // integer
        [Column, Nullable] public string DIREKT_POSTNA_STEVILKA { get; set; } // char(11)
        [Column, Nullable] public string DIREKT_ZS_POSILJKE { get; set; } // char(25)
        [Column, Nullable] public string DIREKT_SIFRA_POTRDITVE_NAROCILA { get; set; } // char(20)
        [Column, Nullable] public int? DIREKT_Z_ODKUPNINO { get; set; } // integer
        [Column, Nullable] public int? DIREKT_Z_PAZLJIVIM_RAVNANJEM { get; set; } // integer
        [Column, Nullable] public int? DIREKT_PO16 { get; set; } // integer
        [Column, Nullable] public int? DIREKT_DO10 { get; set; } // integer
        [Column, Nullable] public int? DIREKT_KLIC1 { get; set; } // integer
        [Column, Nullable] public string EBA_NAROCILO { get; set; } // blob sub_type 1
        [Column, Nullable] public int? PROSTO_I1 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I2 { get; set; } // integer
        [Column, Nullable] public double? PROSTO_R1 { get; set; } // double precision
        [Column, Nullable] public double? PROSTO_R2 { get; set; } // double precision
        [Column, Nullable] public string PROSTO_C1 { get; set; } // char(30)
        [Column, Nullable] public string PROSTO_C2 { get; set; } // char(40)
        [Column, Nullable] public string PROSTO_C3 { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C4 { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C5 { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C6 { get; set; } // char(60)
        [Column, Nullable] public string PROSTO_C7 { get; set; } // char(200)
        [Column, Nullable] public string PROSTO_C8 { get; set; } // char(60)
        [Column, Nullable] public DateTime? PROSTO_D1 { get; set; } // timestamp
        [Column, Nullable] public DateTime? PROSTO_D2 { get; set; } // timestamp
        [Column, Nullable] public int? REZERVACIJA_ZALOGE { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM_VELJAVNOSTI { get; set; } // timestamp
        [Column, Nullable] public string POTNIK { get; set; } // char(5)
        [Column, Nullable] public string KOMISIONAR { get; set; } // char(5)
        [Column, Nullable] public string PARAMETRI_PRERACUNA { get; set; } // char(50)
        [Column, Nullable] public int? S_MESTO2 { get; set; } // integer
        [Column, Nullable] public int? S_MESTO3 { get; set; } // integer
        [Column, Nullable] public int? PARTNER2 { get; set; } // integer
        [Column, Nullable] public int? KONTAKT_OSEBA { get; set; } // integer
        [Column, Nullable] public double? ZNESEK { get; set; } // double precision
        [Column, Nullable] public double? ZNESEK_ZAOKROZEVANJA { get; set; } // double precision
        [Column, Nullable] public double? ZNESEK_KONCNI { get; set; } // double precision
        [Column, Nullable] public double? ZNESEK_KONCNI_VALUTA { get; set; } // double precision
        [Column, Nullable] public int? SESTEVEK_BREZ_DDV { get; set; } // integer
        [Column, Nullable] public double? STEVILO_TOVORKOV { get; set; } // double precision
        [Column, Nullable] public string OPIS_TOVORKOV { get; set; } // char(30)
        [Column, Nullable] public int? NEOBDELANIH_POSTAVK { get; set; } // integer
        [Column, Nullable] public int? PT_STATUS { get; set; } // integer
        [Column, Nullable] public int? SPLET_STATUS { get; set; } // integer
        [Column, Nullable] public string BESEDILO_NAR { get; set; } // blob sub_type 1
        [Column, Nullable] public int? SKLADISCE_KONSIGNATORJA { get; set; } // integer
        [Column, Nullable] public int? LOKACIJA { get; set; } // integer
        [Column, Nullable] public string SKLIC { get; set; } // char(40)
        [Column, Nullable] public string PREDRAC1_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? PREDRAC1_LETO { get; set; } // integer
        [Column, Nullable] public double? PREDRAC1_ZNESEK { get; set; } // double precision
        [Column, Nullable] public int? ROK_PLACILA { get; set; } // integer
        [Column, Nullable] public int? TEKST_RACUN1 { get; set; } // integer
        [Column, Nullable] public int? TEKST_RACUN2 { get; set; } // integer
        [Column, Nullable] public int? TEKST_RACUN3 { get; set; } // integer
        [Column, Nullable] public int? TEKST_RACUN4 { get; set; } // integer
        [Column, Nullable] public int? TEKST_RACUN5 { get; set; } // integer
        [Column, Nullable] public int? TEKST_DOBAVNICA1 { get; set; } // integer
        [Column, Nullable] public int? TEKST_DOBAVNICA2 { get; set; } // integer
        [Column, Nullable] public string RIP_PREBRAL_UPORABNIK { get; set; } // char(20)
        [Column, Nullable] public int? RIP_PREBRAL_ID_UPORABNIK { get; set; } // integer
        [Column, Nullable] public string RIP_PREBRAL_POSTAJA { get; set; } // char(20)
        [Column, Nullable] public DateTime? RIP_DAT_CAS_UVOZA { get; set; } // timestamp
        [Column, Nullable] public string DIREKT_TELEFON { get; set; } // char(20)
        [Column, Nullable] public int? DIREKT_MED18IN20 { get; set; } // integer
        [Column, Nullable] public int? DIREKT_OSA { get; set; } // integer
        [Column, Nullable] public int? DIREKT_VEC_PKT { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I3 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I4 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I5 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I6 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I7 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I8 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I9 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I10 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I11 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I12 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I13 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I14 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I15 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I16 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I17 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I18 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I19 { get; set; } // integer
        [Column, Nullable] public int? PROSTO_I20 { get; set; } // integer
        [Column, Nullable] public string IZVOR_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? IZVOR_LETO { get; set; } // integer
        [Column, Nullable] public int? ZAKLENJENO { get; set; } // integer
        [Column, Nullable] public int? PROJEKT { get; set; } // integer
        [Column, Nullable] public string TEL_KONTAKT_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? TEL_KONTAKT_LETO { get; set; } // integer
        [Column, Nullable] public int? NAR_KUPCI_NI_OBRACUNAN { get; set; } // integer
        [Column, Nullable] public int? TIP_AVTOMATSKEGA_NAROCILA { get; set; } // integer
        [Column, Nullable] public string RIP_NAZ_MD_ULICA { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_MD_KRAJ { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_NAR_ULICA { get; set; } // char(35)
        [Column, Nullable] public string RIP_NAZ_NAR_KRAJ { get; set; } // char(35)
        [Column, Nullable] public string REDOK_NAROCILO { get; set; } // blob sub_type 1
        [Column, Nullable] public string POSTAVKE_MEM { get; set; } // blob sub_type 1
        [Column, Nullable] public int? ID_NAPRAVE { get; set; } // integer
        [Column, Nullable] public int? POVEZAVA_TIP { get; set; } // integer
        [Column, Nullable] public string POVEZAVA_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int? POVEZAVA_LETO { get; set; } // integer
        [Column, Nullable] public DateTime? DATUM_URA_VNOSA { get; set; } // timestamp
        [Column, Nullable] public byte[] PODPIS_GRAFICNO { get; set; } // blob
        [Column, Nullable] public int? DIREKT_OSBDVS { get; set; } // integer
        [Column, Nullable] public int? DIREKT_DVS { get; set; } // integer
        [Column, Nullable] public int? DIREKT_ODK_BN { get; set; } // integer
        [Column, Nullable] public int? DIREKT_DNI_8 { get; set; } // integer
        [Column, Nullable] public int? ZZI_STATUS { get; set; } // integer
        [Column, Nullable] public string ZZI_ID { get; set; } // char(40)
        [Column, Nullable] public string BESEDILO_GL { get; set; } // blob sub_type 1
        [Column, Nullable] public string BESEDILO_NAR_GL { get; set; } // blob sub_type 1
    }

}
