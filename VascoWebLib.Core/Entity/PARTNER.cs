﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.Entity
{
    [Table("PARTNER")]
    public partial class PARTNER
    {
        public static string DBTableName = "PARTNER";
        [Column(), Nullable] public int? SIFRA { get; set; } // integer
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column(), Nullable] public string NAZIV2 { get; set; } // char(40)
        [Column(), Nullable] public string NASLOV { get; set; } // char(30)
        [Column(), Nullable] public string POSTA { get; set; } // char(30)
        [Column(), Nullable] public int? DRZAVA { get; set; } // integer
        [Column("ZIRO_RACUN"), Nullable] public string ZIRORACUN { get; set; } // char(40)
        [Column("ZIRO_RACUN_SKLIC1"), Nullable] public string ZIRORACUNSKLIC1 { get; set; } // char(2)
        [Column("ZIRO_RACUN_SKLIC2"), Nullable] public string ZIRORACUNSKLIC2 { get; set; } // char(25)
        [Column(), Nullable] public string MATICNA { get; set; } // char(10)
        [Column(), Nullable] public string DAVCNA { get; set; } // char(20)
        [Column(), Nullable] public string IDENT { get; set; } // char(20)
        [Column(), Nullable] public string TELEFON { get; set; } // char(60)
        [Column(), Nullable] public string TELEFAKS { get; set; } // char(20)
        [Column("NAZIV_BANKE_1"), Nullable] public string NAZIVBANKE1 { get; set; } // char(30)
        [Column("NAZIV_BANKE_2"), Nullable] public string NAZIVBANKE2 { get; set; } // char(30)
        [Column("SEDEZ_PREJEMNIKA"), Nullable] public string SEDEZPREJEMNIKA { get; set; } // char(20)
        [Column("KONTAKTNA_OSEBA"), Nullable] public string KONTAKTNAOSEBA { get; set; } // char(30)
        [Column("DAVCNI_ZAVEZANEC"), Nullable] public int? DAVCNIZAVEZANEC { get; set; } // integer
        [Column("E_POSTA"), Nullable] public string EPOSTA { get; set; } // char(80)
        [Column("DOMACA_STRAN"), Nullable] public string DOMACASTRAN { get; set; } // char(40)
        [Column(), Nullable] public string SEKTOR { get; set; } // char(5)
        [Column("IZLOCEN_SID"), Nullable] public int? IZLOCENSID { get; set; } // integer
        [Column(), Nullable] public string OPOMBA { get; set; } // blob sub_type 1
        [Column("KRAJ_BANKE"), Nullable] public string KRAJBANKE { get; set; } // char(35)
        [Column("DRZAVA_BANKE"), Nullable] public int? DRZAVABANKE { get; set; } // integer
        [Column("DEVIZNI_RACUN1"), Nullable] public string DEVIZNIRACUN1 { get; set; } // char(50)
        [Column("DEVIZNI_RACUN2"), Nullable] public string DEVIZNIRACUN2 { get; set; } // char(50)
        [Column("SIFRA_POVEZAVA"), Nullable] public string SIFRAPOVEZAVA { get; set; } // char(20)
        [Column("DATUM_VNOSA"), Nullable] public DateTime? DATUMVNOSA { get; set; } // timestamp
        [Column(), Nullable] public string KODA { get; set; } // char(30)
        [Column("RIP_EAN"), Nullable] public string RIPEAN { get; set; } // char(35)
        [Column("RIP_DOBAVNICE"), Nullable] public int? RIPDOBAVNICE { get; set; } // integer
        [Column("PTT_NAZIV"), Nullable] public string PTTNAZIV { get; set; } // char(40)
        [Column("PTT_NAZIV2"), Nullable] public string PTTNAZIV2 { get; set; } // char(40)
        [Column("PTT_NAZIV3"), Nullable] public string PTTNAZIV3 { get; set; } // char(40)
        [Column("PTT_NASLOV"), Nullable] public string PTTNASLOV { get; set; } // char(30)
        [Column("PTT_POSTA"), Nullable] public string PTTPOSTA { get; set; } // char(30)
        [Column("PTT_DRZAVA"), Nullable] public int? PTTDRZAVA { get; set; } // integer
        [Column(), Nullable] public string UPORABNIK { get; set; } // char(80)
        [Column(), Nullable] public string GESLO { get; set; } // char(32)
        [Column("INTERNET_DOVOLI_VSE_ARTIKLE"), Nullable] public int? INTERNETDOVOLIVSEARTIKLE { get; set; } // integer
        [Column("NASA_SIFRA"), Nullable] public string NASASIFRA { get; set; } // char(20)
        [Column("KONTO_BRUTO_VREDNOST"), Nullable] public string KONTOBRUTOVREDNOST { get; set; } // char(8)
        [Column("SIFRA_ODPREME"), Nullable] public int? SIFRAODPREME { get; set; } // integer
        [Column("ZAPOREDJE_DOSTAVE"), Nullable] public int? ZAPOREDJEDOSTAVE { get; set; } // integer
        [Column("USTAVLJENA_PRODAJA"), Nullable] public int? USTAVLJENAPRODAJA { get; set; } // integer
        [Column("NEPREKLICNO_USTAVLJENA_PRODAJA"), Nullable] public int? NEPREKLICNOUSTAVLJENAPRODAJA { get; set; } // integer
        [Column("NI_NEPREKLICNO_USTAVLJENE"), Nullable] public int? NINEPREKLICNOUSTAVLJENE { get; set; } // integer
        [Column("DNI_USTAVLJENA_NEPR_UST"), Nullable] public int? DNIUSTAVLJENANEPRUST { get; set; } // integer
        [Column("DNI_USTAVLJENA"), Nullable] public int? DNIUSTAVLJENA { get; set; } // integer
        [Column("POSEBNOSTI_AKCIJE"), Nullable] public string POSEBNOSTIAKCIJE { get; set; } // char(20)
        [Column("SIFRA_VALUTE"), Nullable] public int? SIFRAVALUTE { get; set; } // integer
        [Column(), Nullable] public int? TOZEN { get; set; } // integer
        [Column(), Nullable] public int? OPOMINJAN { get; set; } // integer
        [Column("NE_DOBIVA_OPOMINE"), Nullable] public int? NEDOBIVAOPOMINE { get; set; } // integer
        [Column(), Nullable] public string KOMERCIALIST { get; set; } // char(5)
        [Column(), Nullable] public string POTNIK { get; set; } // char(5)
        [Column(), Nullable] public string KOMISIONAR { get; set; } // char(5)
        [Column("SIFRA_V_GK"), Nullable] public int? SIFRAVGK { get; set; } // integer
        [Column("STEVILO_POSEBNIH_CEN"), Nullable] public int? STEVILOPOSEBNIHCEN { get; set; } // integer
        [Column("STEVILO_PRODAJALN"), Nullable] public int? STEVILOPRODAJALN { get; set; } // integer
        [Column("NACIN_IZPISA_NALEPKE"), Nullable] public int? NACINIZPISANALEPKE { get; set; } // integer
        [Column("TIP_ZBIRNEGA_FAK"), Nullable] public int? TIPZBIRNEGAFAK { get; set; } // integer
        [Column("UPOSTEVAM_PRODAJALNE"), Nullable] public int? UPOSTEVAMPRODAJALNE { get; set; } // integer
        [Column("TIP_PARTNERJA"), Nullable] public int? TIPPARTNERJA { get; set; } // integer
        [Column("ROK_PLACILA"), Nullable] public int? ROKPLACILA { get; set; } // integer
        [Column("NACIN_PLACILA"), Nullable] public int? NACINPLACILA { get; set; } // integer
        [Column("VRSTA_CENE"), Nullable] public int? VRSTACENE { get; set; } // integer
        [Column("ZAMUDA_BREZ_OBRESTI_DNI"), Nullable] public int? ZAMUDABREZOBRESTIDNI { get; set; } // integer
        [Column(), Nullable] public double? RABAT { get; set; } // double precision
        [Column(), Nullable] public double? SUPERRABAT { get; set; } // double precision
        [Column("NABAVNI_RABAT"), Nullable] public double? NABAVNIRABAT { get; set; } // double precision
        [Column("POTRDITEV_RABATA"), Nullable] public int? POTRDITEVRABATA { get; set; } // integer
        [Column("MAX_RABAT_NA_AKCIJO"), Nullable] public double? MAXRABATNAAKCIJO { get; set; } // double precision
        [Column("MAX_RABAT_NA_AKCIJO_2"), Nullable] public double? MAXRABATNAAKCIJO2 { get; set; } // double precision
        [Column("PROCENT_NABAVNE_MARZE"), Nullable] public double? PROCENTNABAVNEMARZE { get; set; } // double precision
        [Column("RABATNA_SKUPINA"), Nullable] public int? RABATNASKUPINA { get; set; } // integer
        [Column("RABATNA_SKUPINA2"), Nullable] public int? RABATNASKUPINA2 { get; set; } // integer
        [Column("LIMIT_ZAPADLO"), Nullable] public double? LIMITZAPADLO { get; set; } // double precision
        [Column("LIMIT_NEZAPADLO"), Nullable] public double? LIMITNEZAPADLO { get; set; } // double precision
        [Column("LIMIT_SKUPAJ"), Nullable] public double? LIMITSKUPAJ { get; set; } // double precision
        [Column("DATUM_POGODBE"), Nullable] public DateTime? DATUMPOGODBE { get; set; } // timestamp
        [Column("STEVILKA_POGODBE"), Nullable] public string STEVILKAPOGODBE { get; set; } // char(40)
        [Column("TIP_MESECNEGA_FAK"), Nullable] public int? TIPMESECNEGAFAK { get; set; } // integer
        [Column("ZNESEK_MES_FAK"), Nullable] public double? ZNESEKMESFAK { get; set; } // double precision
        [Column("S_MESTO_MES_FAK"), Nullable] public int? SMESTOMESFAK { get; set; } // integer
        [Column("ZNESEK_MES_FAK2"), Nullable] public double? ZNESEKMESFAK2 { get; set; } // double precision
        [Column("ZNESEK_MES_FAK3"), Nullable] public double? ZNESEKMESFAK3 { get; set; } // double precision
        [Column("ZNESEK_MES_FAK4"), Nullable] public double? ZNESEKMESFAK4 { get; set; } // double precision
        [Column("MES_FAK_OD_DATUMA"), Nullable] public DateTime? MESFAKODDATUMA { get; set; } // timestamp
        [Column("MES_FAK_DO_DATUMA"), Nullable] public DateTime? MESFAKDODATUMA { get; set; } // timestamp
        [Column("MES_FAK_OPOMBA"), Nullable] public string MESFAKOPOMBA { get; set; } // blob sub_type 1
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column("ODPRTO_NEZAPADLO"), Nullable] public double? ODPRTONEZAPADLO { get; set; } // double precision
        [Column("ODPRTO_ZAP_DO_30"), Nullable] public double? ODPRTOZAPDO30 { get; set; } // double precision
        [Column("ODPRTO_ZAP_DO_60"), Nullable] public double? ODPRTOZAPDO60 { get; set; } // double precision
        [Column("ODPRTO_ZAP_DO_90"), Nullable] public double? ODPRTOZAPDO90 { get; set; } // double precision
        [Column("ODPRTO_ZAP_NAD"), Nullable] public double? ODPRTOZAPNAD { get; set; } // double precision
        [Column(), Nullable] public int? ZAOKROZI { get; set; } // integer
        [Column("DOBAVNICA_OBLIKA"), Nullable] public string DOBAVNICAOBLIKA { get; set; } // char(4)
        [Column("RACUN_OBLIKA"), Nullable] public string RACUNOBLIKA { get; set; } // char(4)
        [Column("PREDRACUN_OBLIKA"), Nullable] public string PREDRACUNOBLIKA { get; set; } // char(4)
        [Column("INTERNI_OBLIKA"), Nullable] public string INTERNIOBLIKA { get; set; } // char(4)
        [Column(), Nullable] public int? IZBRAN1 { get; set; } // integer
        [Column(), Nullable] public int? IZBRAN2 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT1 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT2 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT3 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT4 { get; set; } // integer
        [Column(), Nullable] public int? SIFRANT5 { get; set; } // integer
        [Column("PROSTO_I1"), Nullable] public int? PROSTOI1 { get; set; } // integer
        [Column("PROSTO_I2"), Nullable] public int? PROSTOI2 { get; set; } // integer
        [Column("PROSTO_R1"), Nullable] public double? PROSTOR1 { get; set; } // double precision
        [Column("PROSTO_R2"), Nullable] public double? PROSTOR2 { get; set; } // double precision
        [Column("PROSTO_C1"), Nullable] public string PROSTOC1 { get; set; } // char(60)
        [Column("PROSTO_C2"), Nullable] public string PROSTOC2 { get; set; } // char(60)
        [Column("PROSTO_C3"), Nullable] public string PROSTOC3 { get; set; } // char(60)
        [Column("PROSTO_C4"), Nullable] public string PROSTOC4 { get; set; } // char(60)
        [Column("PROSTO_C5"), Nullable] public string PROSTOC5 { get; set; } // char(60)
        [Column("PROSTO_C6"), Nullable] public string PROSTOC6 { get; set; } // char(60)
        [Column("PROSTO_C7"), Nullable] public string PROSTOC7 { get; set; } // char(80)
        [Column("PROSTO_C8"), Nullable] public string PROSTOC8 { get; set; } // char(80)
        [Column("PROSTO_C9"), Nullable] public string PROSTOC9 { get; set; } // char(80)
        [Column("PROSTO_C10"), Nullable] public string PROSTOC10 { get; set; } // char(150)
        [Column("PROSTO_D1"), Nullable] public DateTime? PROSTOD1 { get; set; } // timestamp
        [Column("PROSTO_D2"), Nullable] public DateTime? PROSTOD2 { get; set; } // timestamp
        [Column("TRA_KATERA_CENA"), Nullable] public int? TRAKATERACENA { get; set; } // integer
        [Column("VRSTA_TROSARINE"), Nullable] public int? VRSTATROSARINE { get; set; } // integer
        [Column("CASASC_RACUN"), Nullable] public double? CASASCRACUN { get; set; } // double precision
        [Column("CASASCONTO_PROCENT1"), Nullable] public double? CASASCONTOPROCENT1 { get; set; } // double precision
        [Column("CASASCONTO_PROCENT2"), Nullable] public double? CASASCONTOPROCENT2 { get; set; } // double precision
        [Column("CASASCONTO_PROCENT3"), Nullable] public double? CASASCONTOPROCENT3 { get; set; } // double precision
        [Column("EBA_RACUNI"), Nullable] public int? EBARACUNI { get; set; } // integer
        [Column(), Nullable] public string OPOMBA1 { get; set; } // blob sub_type 1
        [Column("ZIRO_RACUN_REFERENCA"), Nullable] public string ZIRORACUNREFERENCA { get; set; } // char(2)
        [Column("TIP_NEREZIDENTA"), Nullable] public int? TIPNEREZIDENTA { get; set; } // integer
        [Column("POVEZAVA_ASSIST"), Nullable] public string POVEZAVAASSIST { get; set; } // char(100)
        [Column("TUJE_SIFRE"), Nullable] public string TUJESIFRE { get; set; } // char(200)
        [Column(), Nullable] public string GSM { get; set; } // char(40)
        [Column("ZAVAROVANE_TERJATVE"), Nullable] public int? ZAVAROVANETERJATVE { get; set; } // integer
        [Column(), Nullable] public string DEJAVNOST { get; set; } // char(10)
        [Column("PROSTO_C_01"), Nullable] public string PROSTOC01 { get; set; } // char(60)
        [Column(), Nullable] public string BIC { get; set; } // char(20)
        [Column("KONTO_220"), Nullable] public string KONTO220 { get; set; } // char(8)
        [Column(), Nullable] public int? NEAKTIVEN { get; set; } // integer
        [Column("PU_SIFRA"), Nullable] public string PUSIFRA { get; set; } // char(10)
        [Column("IZLOCEN_TRR_IZPISEK"), Nullable] public int? IZLOCENTRRIZPISEK { get; set; } // integer
        [Column(), Nullable] public string PROTIKONTO { get; set; } // char(8)
        [Column("SKLIC1_BANCNI_STROSKI"), Nullable] public string SKLIC1BANCNISTROSKI { get; set; } // char(2)
        [Column("SKLIC2_BANCNI_STROSKI"), Nullable] public string SKLIC2BANCNISTROSKI { get; set; } // char(25)
        [Column("SKLIC1_NAKUP_DEVIZ"), Nullable] public string SKLIC1NAKUPDEVIZ { get; set; } // char(2)
        [Column("SKLIC2_NAKUP_DEVIZ"), Nullable] public string SKLIC2NAKUPDEVIZ { get; set; } // char(25)
        [Column(), Nullable] public string IBAN { get; set; } // char(40)
        [Column("ROK_PLACILA_KPF"), Nullable] public int? ROKPLACILAKPF { get; set; } // integer
        [Column("SIFRA_ROK_PLACILA"), Nullable] public int? SIFRAROKPLACILA { get; set; } // integer
        [Column("ST_NAROCILNICE"), Nullable] public string STNAROCILNICE { get; set; } // char(12)
        [Column("TIP_PF"), Nullable] public int? TIPPF { get; set; } // integer
        [Column("NAMEN_PL"), Nullable] public string NAMENPL { get; set; } // char(8)
        [Column("OPOMBA_ZA_RACUN"), Nullable] public string OPOMBAZARACUN { get; set; } // blob sub_type 1
        [Column("ZADNJI_TRR"), Nullable] public string ZADNJITRR { get; set; } // char(40)
        [Column("PARTNER_TIP"), Nullable] public int? PARTNERTIP { get; set; } // integer
        [Column("INTERNET_TIP_UPORABNIKA"), Nullable] public int? INTERNETTIPUPORABNIKA { get; set; } // integer
        [Column("POVEZANA_OSEBA"), Nullable] public int? POVEZANAOSEBA { get; set; } // integer
        [Column("PARAMETRI_PRERACUNA"), Nullable] public string PARAMETRIPRERACUNA { get; set; } // char(50)
        [Column("USTAVLJENA_NABAVA"), Nullable] public int? USTAVLJENANABAVA { get; set; } // integer
        [Column("NEPREKLICNO_UST_PROD_2"), Nullable] public int? NEPREKLICNOUSTPROD2 { get; set; } // integer
        [Column("ROK_PLACILA_DOBAVITELJ"), Nullable] public int? ROKPLACILADOBAVITELJ { get; set; } // integer
        [Column("NADREJEN_PARTNER"), Nullable] public int? NADREJENPARTNER { get; set; } // integer
        [Column("ODPRTO_DATUM_OBNOVITVE"), Nullable] public DateTime? ODPRTODATUMOBNOVITVE { get; set; } // timestamp
        [Column("PRIKAZ_OPOMBE"), Nullable] public int? PRIKAZOPOMBE { get; set; } // integer
        [Column("POGOJI_DOBAVE"), Nullable] public string POGOJIDOBAVE { get; set; } // char(5)
        [Column("POGOJI_DOBAVE_KRAJ"), Nullable] public string POGOJIDOBAVEKRAJ { get; set; } // char(40)
        [Column("E_RACUNI"), Nullable] public int? ERACUNI { get; set; } // integer
        [Column("E_RACUNI_DATUM"), Nullable] public DateTime? ERACUNIDATUM { get; set; } // timestamp
        [Column("E_RACUNI_EMAIL"), Nullable] public string ERACUNIEMAIL { get; set; } // char(200)
        [Column("INTERNA_KUPCEVA_STEVILKA"), Nullable] public string INTERNAKUPCEVASTEVILKA { get; set; } // char(20)
        [Column("VRSTA_OPROSTITVE"), Nullable] public int? VRSTAOPROSTITVE { get; set; } // integer
        [Column("FIZICNA_OSEBA"), Nullable] public int? FIZICNAOSEBA { get; set; } // integer
        [Column("KPF_SM1"), Nullable] public int? KPFSM1 { get; set; } // integer
        [Column("KPF_SM2"), Nullable] public int? KPFSM2 { get; set; } // integer
        [Column("KPF_SM3"), Nullable] public int? KPFSM3 { get; set; } // integer
        [Column("SKUPINA_VK"), Nullable] public int? SKUPINAVK { get; set; } // integer
        [Column("DEF_PROSTO_01"), Nullable] public string DEFPROSTO01 { get; set; } // char(40)
        [Column("DEF_PROSTO_02"), Nullable] public string DEFPROSTO02 { get; set; } // char(40)
        [Column("DEF_PROSTO_03"), Nullable] public string DEFPROSTO03 { get; set; } // char(40)
        [Column("DEF_PROSTO_04"), Nullable] public string DEFPROSTO04 { get; set; } // char(40)
        [Column("DEF_PROSTO_05"), Nullable] public string DEFPROSTO05 { get; set; } // char(40)
        [Column("DEF_PROSTO_06"), Nullable] public string DEFPROSTO06 { get; set; } // char(40)
        [Column("DEF_PROSTO_07"), Nullable] public string DEFPROSTO07 { get; set; } // char(40)
        [Column("DEF_PROSTO_08"), Nullable] public string DEFPROSTO08 { get; set; } // char(40)
        [Column(), Nullable] public int? SKRBNIK { get; set; } // integer
        [Column("DABL_KAKO_POTRJUJEM"), Nullable] public int? DABLKAKOPOTRJUJEM { get; set; } // integer
        [Column("PODPIS_DOBAVNICA"), Nullable] public int? PODPISDOBAVNICA { get; set; } // integer
        [Column("MVASCO_IMEI"), Nullable] public string MVASCOIMEI { get; set; } // char(40)
        [Column("REF_DOKUMENTI"), Nullable] public string REFDOKUMENTI { get; set; } // blob sub_type 1
        [Column("E_RACUNI_POS_XML"), Nullable] public int? ERACUNIPOSXML { get; set; } // integer
        [Column("E_RACUNI_POS_DOB"), Nullable] public int? ERACUNIPOSDOB { get; set; } // integer
        //[Column("BANKA_POSREDNICA"), Nullable] public int? BANKAPOSREDNICA { get; set; } // integer
       // [Column("ZAVAROVANE_TERJATVE_DATUM"), Nullable] public DateTime? ZAVAROVANETERJATVEDATUM { get; set; } // timestamp
    }

    [Table("PARTNER")]
    public partial class PARTNER_BASE
    {
        public static string DBTableName = "PARTNER";

        [Column(), Nullable] public int? SIFRA { get; set; } // integer
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column(), Nullable] public string NAZIV2 { get; set; } // char(40)
        [Column(), Nullable] public string NASLOV { get; set; } // char(30)
        [Column(), Nullable] public string POSTA { get; set; } // char(30)
        [Column(), Nullable] public string DAVCNA { get; set; } // char(20)
        [Column("DAVCNI_ZAVEZANEC"), Nullable] public int? DAVCNIZAVEZANEC { get; set; } // integer
        [Column("ZIRO_RACUN"), Nullable] public string ZIRORACUN { get; set; } // char(40)
        [Column(), Nullable] public string UPORABNIK { get; set; } // char(80)
        [Column(), Nullable] public string GESLO { get; set; } // char(32)
    }


    [Table("FA_PARTNER_BLAGOVNA")]
    public partial class FAPARTNERBLAGOVNA
    {
        public static string DBTableName = "FA_PARTNER_BLAGOVNA";

        [Column("AUTO_ID"), Nullable] public int? AUTOID { get; set; } // integer
        [Column(), Nullable] public int? PARTNER { get; set; } // integer
        [Column(), Nullable] public int? BLAGOVNA { get; set; } // integer

        [NotColumn(), Nullable] public string BLAGOVNA_NAZIV { get; set; } // string
    }

    [Table("FA_PARTNER_GRUPA")]
    public partial class FAPARTNERGRUPA
    {
        public static string DBTableName = "FA_PARTNER_GRUPA";

        [Column("AUTO_ID"), Nullable] public int? AUTOID { get; set; } // integer
        [Column(), Nullable] public int? PARTNER { get; set; } // integer
        [Column(), Nullable] public string GRUPA { get; set; } // char(15)

        [NotColumn(), Nullable] public string GRUPA_NAZIV { get; set; } //string
    }

    [Table("FA_PARTNER_NADGRUPA")]
    public partial class FAPARTNERNADGRUPA
    {
        public static string DBTableName = "FA_PARTNER_NADGRUPA";

        [Column("AUTO_ID"), Nullable] public int? AUTOID { get; set; } // integer
        [Column(), Nullable] public int? PARTNER { get; set; } // integer
        [Column(), Nullable] public string NADGRUPA { get; set; } // char(6)

        [NotColumn(), Nullable] public string NADGRUPA_NAZIV { get; set; } //string
    }

    [Table("FA_PARTNER_SKLADISCE")]
    public partial class FAPARTNERSKLADISCE
    {
        public static string DBTableName = "FA_PARTNER_SKLADISCE";

        [Column("AUTO_ID"), Nullable] public int? AUTOID { get; set; } // integer
        [Column(), Nullable] public int? PARTNER { get; set; } // integer
        [Column(), Nullable] public int? SKLADISCE { get; set; } // integer

        [NotColumn(), Nullable] public string SKLADISCE_NAZIV { get; set; } //string
    }

}
