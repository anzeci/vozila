using LinqToDB.Mapping;
using System;
using System.ComponentModel;

namespace VascoWebLib.Core.Entity
{
    [Table("PNW_SKUPINA")]
    public partial class PNW_SKUPINA
    {
        [Column, Nullable, PrimaryKey, DisplayName("�ifra")] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Naziv")] public string NAZIV { get; set; } // char(50)

        public override string ToString()
        {
            return $"({SIFRA}) {NAZIV}";
        }
    }
}
