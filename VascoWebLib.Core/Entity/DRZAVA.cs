using LinqToDB.Mapping;
using System;
using System.ComponentModel;

namespace VascoWebLib.Core.Entity
{
    [Table("DRZAVA")]
    public partial class DRZAVA
    {
        [Column, Nullable, PrimaryKey, DisplayName("�ifra")] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Naziv")] public string NAZIV { get; set; } // char(30)
        [Column, Nullable, DisplayName("Konto")] public string KONTO { get; set; } // char(8)
        [Column, Nullable, DisplayName("Kratica")] public string KRATICA { get; set; } // char(5)
        //[Column, Nullable, DisplayName("�lanica EU")] public int? EU_CLANICA { get; set; } // integer
        /*[Column, Nullable] public string JEZIK { get; set; } // char(30)
        [Column, Nullable] public string NAZIV_ZA_NASLOVE { get; set; } // char(40)
        [Column, Nullable] public string JEZIK_KRATICA { get; set; } // char(40)
        [Column, Nullable] public int? NE_OBNAVLJAJ { get; set; } // integer
        [Column, Nullable] public string POREKLO_BLAGA { get; set; } // char(40)
        [Column, Nullable] public int? SIFRA_POSTA { get; set; } // integer
        [Column, Nullable] public string TUJI_NAZIV1 { get; set; } // char(80)
        [Column, Nullable] public string TUJI_NAZIV2 { get; set; } // char(80)*/
        public override string ToString()
        {
            return $"({SIFRA}) {NAZIV}";
        }
    }
}
