﻿using LinqToDB.Mapping;
using System;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_BLAGOVNA")]
    public partial class FABLAGOVNA
    {
        public static string DBTableName = "FA_BLAGOVNA";

        [Column(), Nullable] public int? SIFRA { get; set; } // integer
        [Column(), Nullable] public string NAZIV { get; set; } // char(40)
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column("POT_DO_SLIKE"), Nullable] public string POTDOSLIKE { get; set; } // char(80)
        [Column("OPIS_BLAGOVNE_ZNAMKE"), Nullable] public string OPISBLAGOVNEZNAMKE { get; set; } // char(80)
        [Column("PROSTO_I1"), Nullable] public int? PROSTOI1 { get; set; } // integer
        [Column("PROSTO_I2"), Nullable] public int? PROSTOI2 { get; set; } // integer
        [Column("PROSTO_R1"), Nullable] public double? PROSTOR1 { get; set; } // double precision
        [Column("PROSTO_R2"), Nullable] public double? PROSTOR2 { get; set; } // double precision
        [Column("PROSTO_C1"), Nullable] public string PROSTOC1 { get; set; } // char(80)
        [Column("PROSTO_C2"), Nullable] public string PROSTOC2 { get; set; } // char(80)
        [Column(), Nullable] public string KONTO { get; set; } // char(8)
        [Column("KONTO_IZVOZ"), Nullable] public string KONTOIZVOZ { get; set; } // char(8)
        [Column("KONTO_REAL_NEZAVEZANCI"), Nullable] public string KONTOREALNEZAVEZANCI { get; set; } // char(8)
        [Column("KONTO_EU"), Nullable] public string KONTOEU { get; set; } // char(8)
        [Column("MAX_RABAT"), Nullable] public double? MAXRABAT { get; set; } // double precision
        [Column("S_MESTO"), Nullable] public int? SMESTO { get; set; } // integer
        [Column("OBJAVA_B2B"), Nullable] public int? OBJAVAB2B { get; set; } // integer
        [Column("OBJAVA_B2C"), Nullable] public int? OBJAVAB2C { get; set; } // integer
        [Column(), Nullable] public string OPIS { get; set; } // blob sub_type 1
    }
}
