using LinqToDB.Mapping;
using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace VascoWebLib.Core.Entity
{
    [Table("PNW_DELAVEC")]
    public partial class PNW_DELAVEC
    {
        [Column, Nullable, PrimaryKey, DisplayName("�ifra")] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Priimek")] public string PRIIMEK { get; set; } // char(30)
        [Column, Nullable, DisplayName("Naslov")] public string NASLOV { get; set; } // char(30)
        [Column, Nullable, DisplayName("Po�ta")] public string POSTA { get; set; } // char(30)
        [Column, Nullable, DisplayName("Del. mesto")] public string DELOVNO_MESTO { get; set; } // char(70)
        [Column, Nullable, DisplayName("Konto")] public string KONTO { get; set; } // char(8)
        [Column, Nullable, DisplayName("Stro�kovno mesto")] public int? SM { get; set; } // integer
        [Column, Nullable, DisplayName("Stro�kovno mesto 2")] public int? SM2 { get; set; } // integer
        [Column, Nullable, DisplayName("Stro�kovno mesto 3")] public int? SM3 { get; set; } // integer
        [Column, Nullable, DisplayName("KM v slu�bo")] public double? KM_V_SLUZBO { get; set; } // double precision
        [Column, Nullable, DisplayName("Dav�na")] public string DAVCNA { get; set; } // char(10)
        [Column, Nullable, DisplayName("TRR")] public string TRR { get; set; } // char(30)
        [Column, Nullable, DisplayName("Vozilo")] public int? VOZILO { get; set; } // integer
        [Column, Nullable, DisplayName("Skupina")] public int? SKUPINA { get; set; } // integer
        [Column, Nullable, DisplayName("Nalogodajalec")] public int? NALOGODAJALEC { get; set; } // integer
        [Column, Nullable, DisplayName("Do uredbe")] public int? DO_UREDBE { get; set; } // integer
        [Column, Nullable, DisplayName("Komercialist")] public string FA_KOMERCIALIST { get; set; } // char(5)
        [Column, Nullable, DisplayName("Neaktiven")] public int? NEAKTIVEN { get; set; } // integer
        [Column, Nullable, DisplayName("E-po�ta")] public string EMAIL { get; set; } // char(60)
        [Column, Nullable, JsonIgnoreAttribute, DisplayName("Konto")] public string EMAIL_GESLO { get; set; } // char(20)
        [Column, Nullable, DisplayName("Terenec")] public int? TERENEC { get; set; } // integer
        [Column, Nullable, DisplayName("Obra�unovalec")] public int? OBRACUNOVALEC { get; set; } // integer
        [Column, Nullable, DisplayName("Rek param")] public string REK_PARAM { get; set; } // char(30)

        public override string ToString()
        {
            return $"({SIFRA}) {PRIIMEK}";
        }

        /// <summary>
        /// VOZILO
        /// </summary>
        [Association(ThisKey = "VOZILO", OtherKey = "SIFRA", CanBeNull = true)]
        public PNW_VOZILO_MIN VOZILO_OBJ { get; set; }

        /// <summary>
        /// SM
        /// </summary>
        [Association(ThisKey = "SM", OtherKey = "SIFRA", CanBeNull = true)]
        public SM SM_OBJ { get; set; }

        /// <summary>
        /// SM2
        /// </summary>
        [Association(ThisKey = "SM2", OtherKey = "SIFRA", CanBeNull = true)]
        public SM2 SM2_OBJ { get; set; }


    }
}
