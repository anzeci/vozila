﻿using LinqToDB.Mapping;
using System;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_ZALOGA")]
    public partial class FAZALOGA
    {
        [Column(), Nullable] public string SIFRA { get; set; } // char(18)
        [Column(), Nullable] public int? SKLADISCE { get; set; } // integer
        [Column("NAR_KUP_KOLICINA"), Nullable] public double? NARKUPKOLICINA { get; set; } // double precision
        [Column("NAR_DOB_KOLICINA"), Nullable] public double? NARDOBKOLICINA { get; set; } // double precision
        [Column(), Nullable] public double? REZERVIRANO { get; set; } // double precision
        [Column(), Nullable] public double? ZALOGA { get; set; } // double precision
        [Column("DATUM_SPREMEMBE"), Nullable] public DateTime? DATUMSPREMEMBE { get; set; } // timestamp
        [Column("NEAKTIVNA_ZALOGA"), Nullable] public double? NEAKTIVNAZALOGA { get; set; } // double precision
    }
}