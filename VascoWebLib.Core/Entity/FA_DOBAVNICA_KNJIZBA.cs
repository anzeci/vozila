using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.Entity
{
    [Table("FA_DOBAVNICA_KNJIZBA")]
    public partial class FA_DOBAVNICA_KNJIZBA
    {
        [Column, Nullable] public int AUTO_ID { get; set; } // integer
        [Column, Nullable] public string NAR_KUP_STEVILKA { get; set; } // char(8)
        [Column, Nullable] public int NAR_KUP_LETO { get; set; } // integer
        [Column, Nullable] public int NAR_KUP_ZS { get; set; } // integer
        [Column, Nullable] public double KOLICINA { get; set; } // double

    }


   

}
