using LinqToDB.Mapping;
using System;
using System.ComponentModel;

namespace VascoWebLib.Core.Entity
{

    [Table("SM")]
    public partial class SM
    {
        [Column, Nullable, DisplayName("�ifra"), PrimaryKey] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Naziv")] public string NAZIV { get; set; } // char(70)
        [Column, Nullable, DisplayName("Neaktiven")] public int? NEAKTIVEN { get; set; } // integer
        public override string ToString()
        {
            return $"({SIFRA}) {NAZIV}";
        }
    }

    [Table("SM2")]
    public partial class SM2
    {
        [Column, Nullable, DisplayName("�ifra"), PrimaryKey] public int? SIFRA { get; set; } // integer
        [Column, Nullable, DisplayName("Naziv")] public string NAZIV { get; set; } // char(70)
        [Column, Nullable, DisplayName("Neaktiven")] public int? NEAKTIVEN { get; set; } // integer
        /*[Column, Nullable] public string PROSTO_C1 { get; set; } // char(60)
        [Column, Nullable] public int? PROSTO_I1 { get; set; } // integer*/
        public override string ToString()
        {
            return $"({SIFRA}) {NAZIV}";
        }
    }
}
