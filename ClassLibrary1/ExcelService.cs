﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using DevExpress.Spreadsheet;
using LinqToDB.Data;

namespace Vozila.Lib
{

    public class TReadExcelRangeResult
    {
        public CellCollection Data;
        public int Rows;
        public int Columns;
    }

    public class TReadExcelRangeResult2
    {
        public string[,] Data;
        public int Rows;
        public int Columns;
    }

    public class TTableRange
    {
        public int FromRow;
        public int ToRow;
        public int Columns;
    }

    public class TSifraNaziv
    {
        public string Sifra { get; set; }
        public string Naziv { get; set; }

    }

    public class ExcelService
    {
        DataConnection db;
        string connectionString;
        public ExcelService(string connectionString)
        {
            this.connectionString = connectionString;
            Open();
        }
        public void Open()
        {
            db = FirebirdHelper.GetDB(connectionString);
        }

        public static TReadExcelRangeResult GetExcelUsedRangeFromSheet1(string filename)
        {
            TReadExcelRangeResult Result;
            Result = new TReadExcelRangeResult();
            Result.Data = null;
            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            workbook.LoadDocument(filename);
            Worksheet worksheet = workbook.Worksheets[0];
            Result.Rows = 0;
            Result.Columns = 0;
            Range range = worksheet.GetUsedRange();
            Result.Rows = range.RowCount;
            Result.Columns = range.ColumnCount;
            Result.Data = worksheet.Cells;
            return Result;
        }
        public static TReadExcelRangeResult2 GetDataOnFrstSheet(string filename)
        {
            TReadExcelRangeResult2 Result;
            Result = new TReadExcelRangeResult2();
            //Result.Data = null;

            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            workbook.LoadDocument(filename);
            Worksheet worksheet = workbook.Worksheets[0];
            Result.Rows = 0;
            Result.Columns = 0;
            Range range = worksheet.GetUsedRange();
            Result.Rows = range.RowCount;
            Result.Columns = range.ColumnCount;
            Result.Data = new string[Result.Rows, Result.Columns];
            for (int i = 0; i < Result.Rows; i++)
            {
                for (int j = 0; j < Result.Columns; j++)
                {
                    Result.Data[i, j] = worksheet.Cells[i, j].DisplayText.ToString();
                }
            }

            return Result;
        }

        public static TTableRange GetRange(string FileName, string FirstColumnName)
        {
            TReadExcelRangeResult2 data = GetDataOnFrstSheet(FileName);
            int fromRow = -1;
            int toRow = -1;
            int toColumn = -1;

            for (int i = 0; i < data.Rows; i++)
            {

                if (data.Data[i, 0].Trim().ToUpper().Contains(FirstColumnName))
                {
                    if (fromRow == -1)
                    {
                        fromRow = i;
                    }
                    for (int j = 0; j < data.Columns; j++)
                    {
                        if (data.Data[i, j].Trim().Length == 0)
                        {
                            if (toColumn == -1)
                            {
                                toColumn = j;
                            }
                        }
                    }
                    if (toColumn == -1)
                    {
                        toColumn = data.Columns;
                    }

                }
                bool JePraznaVrstica = true;
                for (int j = 0; j < data.Columns; j++)
                {
                    if ((fromRow >= 0) && (data.Data[i, j].Trim().Length > 0))
                    {
                        JePraznaVrstica = false;
                    }
                }

                if ((fromRow >= 0) && JePraznaVrstica && toRow == -1)
                {
                    toRow = i - 1;

                }
            }
            if (toRow == -1)
            {
                toRow = data.Rows - 1;
            }

            TTableRange range = new TTableRange();

            range.FromRow = fromRow;
            range.ToRow = data.Rows;
            range.Columns = toColumn;

            return range;

        }


        public static IList<TSifraNaziv> ReadExcelTable(string FileName, string FirstColumnName)
        {
            List<TSifraNaziv> Result = new List<TSifraNaziv>();
            TTableRange range = GetRange(FileName, "SIFRA");
            TReadExcelRangeResult2 data = GetDataOnFrstSheet(FileName);
            for (int i = range.FromRow + 1; i < range.ToRow; i++)
            {
                TSifraNaziv line = new TSifraNaziv();
                line.Sifra = data.Data[i, 0];
                line.Naziv = data.Data[i, 1];
                Result.Add(line);
            }
            return Result;
        }


        public void Test()
        {
            string sql = @"INSERT INTO VOZILO
                            (
                              SASIJA
                            )
                            VALUES
                            (
                              @SASIJA
                            )";
            db.Execute(sql, DataParameter.VarChar("SASIJA", "123"));
        }

        private string CreateSql(string Znamka, Dictionary<string, string> mapPreslikava)
        {
            string Result = "";
            string Fields = "";
            string Params = "";
           

           int i = 0;
           foreach (KeyValuePair<string, string> zapis in mapPreslikava)
            {
                if (i == 0)
                { 
                    Fields = Fields + zapis.Key;
                    Params = Params + "@" + zapis.Key;
                }
                else
                { 
                    Fields = Fields + ", " + zapis.Key;
                    Params = Params + ", @" + zapis.Key;
                }
                i++;
            }

            Result = "UPDATE OR INSERT INTO VOZILO (" + Fields + ", ZNAMKA,  ROCNO_POPRAVLJANJE) VALUES (" + Params + ", '"+ Znamka + "', 0)";
            return Result;
        }

        private string GetFieldName(string str)
        {
            string[] result = str.Split('#');
            return result[0];
        }
        private string GetFieldAtributes(string str)
        {
            string[] result = str.Split('#');
            if (result.Length >= 2)
                return result[1];
            else
                return "";
        }


        public void ParseDataAndInsert(string FileName, string Znamka, Dictionary<string, string> mapPreslikava, string TipUvoza, string FirstRow)
        {

            TReadExcelRangeResult2 Result;
            Result = new TReadExcelRangeResult2();
            //Result.Data = null;

            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            workbook.LoadDocument(FileName);
            Worksheet worksheet = workbook.Worksheets[0];
            Result.Rows = 0;
            Result.Columns = 0;
            Range range = worksheet.GetUsedRange();
            Debug.WriteLine(worksheet[0,0].Value.ToString());
            string sql = CreateSql(Znamka, mapPreslikava);
            DataParameter[] parameters;
            parameters = new DataParameter[mapPreslikava.Count];

            TTableRange r = GetRange(FileName, FirstRow);
            for (int i = r.FromRow + 1; i <= r.ToRow; i++)
            {
                int k = 0;
                bool NaredimInsert = false;
                foreach (KeyValuePair<string, string> zapis in mapPreslikava)
                {

                    string value = "";
                    string indexiPolj = zapis.Value;
                    string[] arrIndexiPolj = indexiPolj.Split(',');
                    int j = 0;
                    foreach (string index in arrIndexiPolj)
                    {
                        if (j > 0)
                        {
                            value = value + " "; 
                        }
                        value = value + worksheet[i, Int32.Parse(index)].Value.ToString();
                        j++;
                    }
                    DataParameter p;
                    if (value.Trim() == "")
                    {
                        p = new DataParameter(zapis.Key, null, LinqToDB.DataType.Date);
                    }
                    else
                    { 
                        p = new DataParameter(zapis.Key, value);
                    }
                    if (zapis.Key == "SASIJA" && value.Trim() != "")
                        NaredimInsert = true;
                    parameters[k] = p;
                    k++;
                }
                if (NaredimInsert)
                    db.Execute(sql, parameters);
             }

        }

        private string ExcelCharToRowNumber(string str)
        {
            string[] arr = str.Split(',');
            string Result = "";
            int j = 0;
            foreach (string index in arr)
            {
                string strInt = "";
                if (index.Length == 1)
                {
                    strInt = ((int)index[0] - 65).ToString();
                }
                else if (index.Length == 2)
                {
                    strInt = (((int)index[0]-64)*26+(int)index[1] - 65).ToString();
                }
                if (j == 0)
                {
                    Result = strInt;
                }
                else
                {
                    Result = Result + "," + strInt;
                }
                j++;
            }
            return Result;
        }

        public void Import(string FileName, string Znamka, string TipUvoza)
        {
            if (Znamka == "Renault" && TipUvoza == "New")
            {
                Dictionary<string, string> mapPreslikava = new Dictionary<string, string>();
                mapPreslikava.Add("SASIJA", "0");
                mapPreslikava.Add("MODEL", "9,16");
                mapPreslikava.Add("VERZIJA", "10,17");
                mapPreslikava.Add("BARVA", "12");
                mapPreslikava.Add("OPCIJE", "11,18");
                mapPreslikava.Add("NOTRANJOST", "13,20");
                mapPreslikava.Add("OBLAZINJENJE", "14,21");
                mapPreslikava.Add("ST_NAROCILA", "1");
                mapPreslikava.Add("STARANJE", "7");
                mapPreslikava.Add("PREDVIDENA_MADA", "6");
                mapPreslikava.Add("MOZNOST_SPREMEMBE", "23,24,25");
                mapPreslikava.Add("STATUS", "26");
                mapPreslikava.Add("ENOTA", "27");
                mapPreslikava.Add("OSEBNI_TOVORNI", "16");
                ParseDataAndInsert(FileName, Znamka, mapPreslikava, TipUvoza, "OGRODJE");
            }
            if (Znamka == "Nissan" && TipUvoza == "New")
            {
                Dictionary<string, string> mapPreslikava = new Dictionary<string, string>();
                mapPreslikava.Add("SASIJA", "4");
                mapPreslikava.Add("MODEL", "5");
                mapPreslikava.Add("VERZIJA", "5");
                mapPreslikava.Add("BARVA", "7");
                mapPreslikava.Add("IDENT", "3");
                mapPreslikava.Add("LOKACIJA", "9");
                mapPreslikava.Add("DATUM_NAROCILA", "14");
                mapPreslikava.Add("KUPEC", "18");
                ParseDataAndInsert(FileName, Znamka, mapPreslikava, TipUvoza, "PAR.");
            }
            if (Znamka == "Dacia" && TipUvoza == "New")
            {
                Dictionary<string, string> mapPreslikava = new Dictionary<string, string>();
                mapPreslikava.Add("SASIJA", "0");
                mapPreslikava.Add("MODEL", "9,16");
                mapPreslikava.Add("VERZIJA", "10,17");
                mapPreslikava.Add("BARVA", "12");
                mapPreslikava.Add("OPCIJE", "11,18");
                mapPreslikava.Add("NOTRANJOST", "13,20");
                mapPreslikava.Add("OBLAZINJENJE", "14,21");
                mapPreslikava.Add("ST_NAROCILA", "1");
                mapPreslikava.Add("STARANJE", "7");
                mapPreslikava.Add("PREDVIDENA_MADA", "6");
                mapPreslikava.Add("MOZNOST_SPREMEMBE", "23,24,25");
                mapPreslikava.Add("STATUS", "26");
                mapPreslikava.Add("ENOTA", "27");
                mapPreslikava.Add("OSEBNI_TOVORNI", "16");
                ParseDataAndInsert(FileName, Znamka, mapPreslikava, TipUvoza, "OGRODJE");
            }
            if (Znamka == "Renault" && TipUvoza == "Kupec")
            {
                Dictionary<string, string> mapPreslikava = new Dictionary<string, string>();
                mapPreslikava.Add("SASIJA", ExcelCharToRowNumber("A"));
                mapPreslikava.Add("MODEL",  ExcelCharToRowNumber("Z,AF"));
                mapPreslikava.Add("VERZIJA", ExcelCharToRowNumber("AA,AG"));
                mapPreslikava.Add("BARVA", ExcelCharToRowNumber("AC"));
                mapPreslikava.Add("OPCIJE", ExcelCharToRowNumber("AB,AH"));
                mapPreslikava.Add("NOTRANJOST", ExcelCharToRowNumber("AE,AK"));
                mapPreslikava.Add("OBLAZINJENJE", ExcelCharToRowNumber("ZD,AJ"));
                mapPreslikava.Add("ST_NAROCILA", ExcelCharToRowNumber("C"));
                mapPreslikava.Add("ST_NAROCILA_KUPCA", ExcelCharToRowNumber("B"));
                mapPreslikava.Add("DATUM_VNOSA_KUPCA", ExcelCharToRowNumber("O"));
                mapPreslikava.Add("STATUS", ExcelCharToRowNumber("E"));
                mapPreslikava.Add("PRODAJALEC", ExcelCharToRowNumber("N"));
                mapPreslikava.Add("ENOTA", ExcelCharToRowNumber("L"));
                mapPreslikava.Add("TIP_KUPCA", ExcelCharToRowNumber("R"));
                mapPreslikava.Add("PRODAJNI_KANAL", ExcelCharToRowNumber("S"));
                mapPreslikava.Add("OSEBNI_TOVORNI", ExcelCharToRowNumber("AF"));
                mapPreslikava.Add("KUPEC", ExcelCharToRowNumber("K,U"));
                mapPreslikava.Add("TELEFON", ExcelCharToRowNumber("W"));
                mapPreslikava.Add("EMAIL", ExcelCharToRowNumber("V"));
                mapPreslikava.Add("NASLOVNIK", ExcelCharToRowNumber("M"));
                mapPreslikava.Add("MOZNOST_SPREMEMBE", ExcelCharToRowNumber("AP,AQ,AR"));
                mapPreslikava.Add("DATUM_DOBAVE", ExcelCharToRowNumber("G"));
                mapPreslikava.Add("DATUM_FAKTURE", ExcelCharToRowNumber("AS"));
                ParseDataAndInsert(FileName, Znamka,  mapPreslikava, TipUvoza, "OGRODJE");
            }
            if (Znamka == "Dacia" && TipUvoza == "Kupec")
            {
                Dictionary<string, string> mapPreslikava = new Dictionary<string, string>();
                mapPreslikava.Add("SASIJA", ExcelCharToRowNumber("A"));
                mapPreslikava.Add("MODEL", ExcelCharToRowNumber("Z,AF"));
                mapPreslikava.Add("VERZIJA", ExcelCharToRowNumber("AA,AG"));
                mapPreslikava.Add("BARVA", ExcelCharToRowNumber("AC"));
                mapPreslikava.Add("OPCIJE", ExcelCharToRowNumber("AB,AH"));
                mapPreslikava.Add("NOTRANJOST", ExcelCharToRowNumber("AE,AK"));
                mapPreslikava.Add("OBLAZINJENJE", ExcelCharToRowNumber("ZD,AJ"));
                mapPreslikava.Add("ST_NAROCILA", ExcelCharToRowNumber("C"));
                mapPreslikava.Add("ST_NAROCILA_KUPCA", ExcelCharToRowNumber("B"));
                mapPreslikava.Add("DATUM_VNOSA_KUPCA", ExcelCharToRowNumber("O"));
                mapPreslikava.Add("STATUS", ExcelCharToRowNumber("E"));
                mapPreslikava.Add("PRODAJALEC", ExcelCharToRowNumber("N"));
                mapPreslikava.Add("ENOTA", ExcelCharToRowNumber("L"));
                mapPreslikava.Add("TIP_KUPCA", ExcelCharToRowNumber("R"));
                mapPreslikava.Add("PRODAJNI_KANAL", ExcelCharToRowNumber("S"));
                mapPreslikava.Add("OSEBNI_TOVORNI", ExcelCharToRowNumber("AF"));
                mapPreslikava.Add("KUPEC", ExcelCharToRowNumber("K,U"));
                mapPreslikava.Add("TELEFON", ExcelCharToRowNumber("W"));
                mapPreslikava.Add("EMAIL", ExcelCharToRowNumber("V"));
                mapPreslikava.Add("NASLOVNIK", ExcelCharToRowNumber("M"));
                mapPreslikava.Add("MOZNOST_SPREMEMBE", ExcelCharToRowNumber("AP,AQ,AR"));
                mapPreslikava.Add("DATUM_DOBAVE", ExcelCharToRowNumber("G"));
                mapPreslikava.Add("DATUM_FAKTURE", ExcelCharToRowNumber("AS"));
                ParseDataAndInsert(FileName, Znamka, mapPreslikava, TipUvoza, "OGRODJE");
            }
            if (Znamka == "Renault" && TipUvoza == "Odprema")
            {
                Dictionary<string, string> mapPreslikava = new Dictionary<string, string>();
                mapPreslikava.Add("SASIJA", ExcelCharToRowNumber("A"));
                mapPreslikava.Add("MODEL", ExcelCharToRowNumber("Z,AF"));
                mapPreslikava.Add("VERZIJA", ExcelCharToRowNumber("AA,AG"));
                mapPreslikava.Add("BARVA", ExcelCharToRowNumber("AC"));
                mapPreslikava.Add("OPCIJE", ExcelCharToRowNumber("AB,AH"));
                mapPreslikava.Add("NOTRANJOST", ExcelCharToRowNumber("AE,AK"));
                mapPreslikava.Add("OBLAZINJENJE", ExcelCharToRowNumber("ZD,AJ"));
                mapPreslikava.Add("ST_NAROCILA", ExcelCharToRowNumber("C"));
                mapPreslikava.Add("ST_NAROCILA_KUPCA", ExcelCharToRowNumber("B"));
                mapPreslikava.Add("DATUM_VNOSA_KUPCA", ExcelCharToRowNumber("O"));
                mapPreslikava.Add("STATUS", ExcelCharToRowNumber("E"));
                mapPreslikava.Add("PRODAJALEC", ExcelCharToRowNumber("N"));
                mapPreslikava.Add("ENOTA", ExcelCharToRowNumber("L"));
                mapPreslikava.Add("TIP_KUPCA", ExcelCharToRowNumber("R"));
                mapPreslikava.Add("PRODAJNI_KANAL", ExcelCharToRowNumber("S"));
                mapPreslikava.Add("OSEBNI_TOVORNI", ExcelCharToRowNumber("AF"));
                mapPreslikava.Add("KUPEC", ExcelCharToRowNumber("K,U"));
                mapPreslikava.Add("TELEFON", ExcelCharToRowNumber("W"));
                mapPreslikava.Add("EMAIL", ExcelCharToRowNumber("V"));
                mapPreslikava.Add("NASLOVNIK", ExcelCharToRowNumber("M"));
                mapPreslikava.Add("MOZNOST_SPREMEMBE", ExcelCharToRowNumber("AP,AQ,AR"));
                mapPreslikava.Add("DATUM_DOBAVE", ExcelCharToRowNumber("G"));
                mapPreslikava.Add("DATUM_FAKTURE", ExcelCharToRowNumber("AS"));
                ParseDataAndInsert(FileName, Znamka, mapPreslikava, TipUvoza, "OGRODJE");
            }
        }
    }


}
