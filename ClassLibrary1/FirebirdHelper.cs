﻿using FirebirdSql.Data.FirebirdClient;
using LinqToDB.Data;

namespace Vozila.Lib
{
    public static class FirebirdHelper
    {
        public static string GetConnectionString(string User, string Password, string Database, string DataSource, int Port = 3050, int Dialect = 1, string Charset = "WIN1250", int PacketSize = 4096, int ServerType = 0)
        {
            var connBuilder = new FbConnectionStringBuilder
            {
                UserID = User,
                Password = Password,
                Database = Database,
                DataSource = DataSource,
                Port = Port,
                Dialect = Dialect,
                Charset = Charset,
                PacketSize = PacketSize,
                ServerType = ServerType == 0 ? FbServerType.Default : FbServerType.Embedded
            };
            return connBuilder.ToString();
        }

        public static DataConnection GetDB(string connectionString)
        {
            return new DataConnection("Firebird", connectionString);
        }

    }
}
