using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace VascoWebLib.Core.UI.Helpers
{
    public static class VascoWebHelper
    {
        private static IHttpContextAccessor _httpContextAccessor;
        private static IActionContextAccessor _actionContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor, IActionContextAccessor actionContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _actionContextAccessor = actionContextAccessor;
        }

        public static HttpContext HttpContext
        {
            get { return _httpContextAccessor.HttpContext; }
        }

        public static IUrlHelper UrlHelper
        {
            get
            {
                var urlFactory = new UrlHelperFactory();
                return urlFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
            }
        }


        public static string AppBaseUrl
        {
            get
            {
                return $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}";
            }
        }

        public static string GetUserAgent
        {
            get { return HttpContext.Request.Headers["User-Agent"].ToString(); }
        }

        public static string GetScheme
        {
            get { return HttpContext.Request.Scheme; }
        }

        public static string GetAuthenticatedUsername()
        {
            return ((ClaimsIdentity)HttpContext.User.Identity).Claims.Where(c => c.Type == ClaimTypes.GivenName)
                   .Select(c => c.Value).SingleOrDefault();
        }

        public static string GetAuthenticatedUserName()
        {
            return ((ClaimsIdentity)HttpContext.User.Identity).Claims.Where(c => c.Type == ClaimTypes.Name)
                   .Select(c => c.Value).SingleOrDefault();
        }

        public static string GetAuthenticatedUserID()
        {
            return ((ClaimsIdentity)HttpContext.User.Identity).Claims.Where(c => c.Type == ClaimTypes.Sid)
                   .Select(c => c.Value).SingleOrDefault();
        }

        public static bool IsUserAuthenticated()
        {
            return HttpContext.User.Identity.IsAuthenticated;
        }



        public static string ClientIP()
        {
            return HttpContext.Connection.RemoteIpAddress.ToString();
        }

        public static string LocalIP()
        {
            return HttpContext.Connection.LocalIpAddress.ToString();
        }

        public static string GetHtmlContentString(this IHtmlContent content)
        {
            var writer = new System.IO.StringWriter();
            content.WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }


        public static string GetCookieValueFromResponse(HttpResponse response, string cookieName)
        {
            
            foreach (var headers in response.Headers.Values)
                foreach (var header in headers)
                    if (header.StartsWith(cookieName))
                    {
                        var p1 = header.IndexOf('=');
                        var p2 = header.IndexOf(';');
                        return System.Net.WebUtility.UrlDecode(header.Substring(p1 + 1, p2 - p1 - 1));
                    }
            return null;
        }
        /// <summary>
        /// Get unique GUID
        /// </summary>
        public static string UID
        {
            get
            {
                return Guid.NewGuid().ToString("N");
            }
        }
    }
}