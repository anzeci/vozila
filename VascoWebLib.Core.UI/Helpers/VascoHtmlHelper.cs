using DevExtreme.AspNet.Mvc.Builders;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Html;
using VascoWebLib.Core.UI.Model.View;
using VascoWebLib.Core.UI.Model;

namespace VascoWebLib.Core.UI.Helpers
{
    public static class VascoHtmlHelper
    {

        public static DataGridBuilder<T> VascoListView<T>(this IHtmlHelper html, ListViewModel listModel)
        {
            var builder = html.DevExtreme().DataGrid<T>();
            builder.DataSource(d => d.Mvc().Controller(listModel.Controller).LoadAction(listModel.LoadAction).Key(listModel.Key));
            CreateListView(html, listModel, builder);
            return builder;
        }

        private static void CreateListView<T>(IHtmlHelper html, ListViewModel listModel, DataGridBuilder<T> builder)
        {
            var options = listModel.ListViewOptions;
            builder.ID(listModel.ViewID);

            var attributes = new Dictionary<string, object>();
            attributes.Add("View", "ListView");

            if (listModel.IsPageViewModel())
            {
                attributes.Add("ViewAutoHeight", listModel.AutoHeightOnPage.ToString());
            }

            builder.ElementAttr(attributes);

            builder
            .ShowBorders(options.ShowBorders)
            .ShowRowLines(options.ShowRowLines)
            .ShowColumnLines(options.ShowColumnLines)
            .ShowColumnHeaders(options.ShowColumnHeaders)
            .Scrolling(scrolling => scrolling.Mode(options.VirtualPager ? GridScrollingMode.Virtual : GridScrollingMode.Standard).ColumnRenderingMode(options.VirtualPager ? GridColumnRenderingMode.Virtual : GridColumnRenderingMode.Standard))
            .Paging(p => p.PageSize(options.PageSize))
            .Pager(p => p.Visible(!options.VirtualPager))
            .FilterRow(f => f.Visible(options.FilterRowVisible))
            .HeaderFilter(f => f.Visible(options.HeaderFilterVisible))
            .GroupPanel(p => p.Visible(options.GroupPanelVisible))
            .Grouping(g => g.AutoExpandAll(options.AutoExpandGroups))
            .ColumnAutoWidth(options.ColumnAutoWidth)
            .RemoteOperations(options.RemoteOperations);


            if (options.ShowColumnChooser)
            {
                builder.ColumnChooser(c =>
                {
                    c.AllowSearch(true);
                    c.Enabled(true);
                    c.Mode(GridColumnChooserMode.Select);
                    c.Height(450);
                });
            }

            //initialized
            var initializedJS = new StringBuilder();
            initializedJS.Append("checkContentResize();");
            initializedJS.Append("focusListViewSearch();");

            if (initializedJS.Length > 0)
            {
                builder.OnInitialized("function (e) { " + initializedJS.ToString() + " }");
            }

            if (options.SelectionMode == ListViewSelectionMode.None)
                builder.Selection(s => s.Mode(SelectionMode.None));
            else if (options.SelectionMode == ListViewSelectionMode.Single)
            {
                builder.Selection(s => s.Mode(SelectionMode.Single));
                if (listModel.ListViewOptions.RowDblClickSelection && !string.IsNullOrEmpty(listModel.ParentViewID))
                    listModel.RowDblClickJavascriptHandlers.Add(listModel.ParentViewID + "_RowSelectionClicked(e);");
            }
            else if (options.SelectionMode == ListViewSelectionMode.Multiple)
                builder.Selection(s => s.Mode(SelectionMode.Multiple).SelectAllMode(SelectAllMode.AllPages).ShowCheckBoxesMode(GridSelectionShowCheckBoxesMode.Always));

            if (options.SearchEnabled)
            {
                builder.SearchPanel(s => { s.HighlightSearchText(true).Visible(true); });
                builder.Option("vasco.searchPosition", (int)options.SearchPosition);
                builder.OnToolbarPreparing("Vasco.listView.onToolbarPreparing");
            }

            if (listModel.AllowInsert && !string.IsNullOrEmpty(listModel.InsertFormAction))
            {
                builder.Option("vasco.allowAdd", true);
                builder.Option("vasco.insertFormActionAsDialog", listModel.InsertFormActionAsDialog);
                builder.Option("vasco.insertFuncName", listModel.InsertJSFuncName);
                builder.OnToolbarPreparing("Vasco.listView.onToolbarPreparing");
            }
            builder.Columns(c =>
            {
                string colWidth = "50px";
                if (ListViewOptions.CompactTheme)
                {
                    colWidth = "40px";
                }
                if (listModel.AllowView && !string.IsNullOrEmpty(listModel.ViewFormAction))
                {

                    builder.Option("vasco.viewFormActionAsDialog", listModel.ViewFormActionAsDialog);
                    c.Add().Caption(" ").AllowHiding(false).ShowInColumnChooser(false).Width(colWidth).CellTemplate((r) => { return html.DevExtreme().Button().ID(listModel.ViewID + "_btnView").Icon("folder").Hint("Odpri").OnClick($"function(e) {{ e.event.preventDefault(); e.event.stopPropagation(); {listModel.ViewJSFuncName}(e, data); }}"); });
                }

                if (listModel.AllowEdit && !string.IsNullOrEmpty(listModel.EditFormAction))
                {
                    builder.Option("vasco.editFormActionAsDialog", listModel.EditFormActionAsDialog);
                    c.Add().Caption(" ").AllowHiding(false).ShowInColumnChooser(false).Width(colWidth).CellTemplate((r) => { return html.DevExtreme().Button().ID(listModel.ViewID + "_btnEdit").Icon("edit").Hint("Popravi").OnClick($"function(e) {{ e.event.preventDefault(); e.event.stopPropagation(); {listModel.EditJSFuncName}(e, data); }}"); });
                }

                if (listModel.AllowDelete && !string.IsNullOrEmpty(listModel.DeleteAction))
                {
                    c.Add().Caption(" ").AllowHiding(false).ShowInColumnChooser(false).Width(colWidth).CellTemplate((r) => { return html.DevExtreme().Button().ID(listModel.ViewID + "_btnDelete").Hint("Odstrani").Icon("remove").OnClick($"function(e) {{ e.event.preventDefault(); e.event.stopPropagation(); {listModel.DeleteJSFuncName}(e, data); }}"); });
                }
            });


            //row click handlers
            var rowClickHandlers = new StringBuilder();
            foreach (var handler in listModel.RowClickJavascriptHandlers)
            {
                rowClickHandlers.AppendLine(handler);
                if (!handler.EndsWith(";"))
                    rowClickHandlers.Append(";");
            }
            //row dbl click handlers
            var rowDblClickHandlers = new StringBuilder();
            foreach (var handler in listModel.RowDblClickJavascriptHandlers)
            {
                rowDblClickHandlers.AppendLine(handler);
                if (!handler.EndsWith(";"))
                    rowDblClickHandlers.Append(";");
            }


            //row click definition
            builder.OnRowClick(@"function (e) {
               
                prevClickTime = lastClickTime;
                lastClickTime = new Date();
                if (prevClickTime && lastClickTime - prevClickTime < 250) {
                    clearTimeout(clickTimer);
                    //DBL CLICK
                    " + rowDblClickHandlers.ToString() + @"
                }
                else {
                    clickTimer = setInterval(function () {
                        clearTimeout(clickTimer);
                        //SINGLE CLICK
                        " + rowClickHandlers.ToString() + @"
                    }, 250);
                }
            }");


            /*if ((detailView != null) && (html.ViewContext.View is RazorView))
            {
                builder.Columns(c =>
                {
                    c.Add().AllowHiding(false).ShowInColumnChooser(false).Width(60).CellTemplate((r) => { return html.VascoDetailViewAction(detailView); });
                });
                #region ComponentViewTest
                /*var razorView = html.ViewContext.View as RazorView;
                builder.Columns(c =>
                {
                    dynamic razorPage = razorView.RazorPage;
                    IViewComponentHelper componentHelper = (IViewComponentHelper)razorPage.Component;
                    c.Add().Width(60).CellTemplate((r) => { return componentHelper.InvokeAsync(DetailFormActionViewComponent.ComponentName, detailForm).Result; });
                });*/

            /*
            }*/
        }


        public static ButtonBuilder SetupVascoDialogButton(this ButtonBuilder btn, ButtonDialogViewModel model) {

            var sb = new StringBuilder();
            sb.Append("function (e) {");
            sb.Append("var reqParam = {};");
            if (!string.IsNullOrEmpty(model.JSParamCallback)) {
                sb.Append($"var reqParamFn = {model.JSParamCallback};");
                sb.Append($"reqParam = reqParamFn();");
            }
            sb.Append("var fnDialogResultCallback = null;");
            if (!string.IsNullOrEmpty(model.JSDialogResultCallback))
            {
                sb.Append($"fnDialogResultCallback = {model.JSDialogResultCallback};");
            }
            sb.Append($"Vasco.ui.showDialogView('{model.Controller}', '{model.Action}', reqParam, fnDialogResultCallback);");
            sb.Append("}");

            btn.OnClick(sb.ToString());
            return btn;
        }


        public static PopupToolbarItemBuilder VascoSetupDialogToolbarButton(this PopupToolbarItemBuilder toolbarButton, DialogViewModel dialogView, bool bottomToolbar, ToolbarViewButton button)
        {

            ToolbarItemLocation location = ToolbarItemLocation.After;
            switch (button.ButtonLocation)
            {
                case ToolbarViewButtonLocation.After:
                    location = ToolbarItemLocation.After;
                    break;
                case ToolbarViewButtonLocation.Before:
                    location = ToolbarItemLocation.Before;
                    break;
                case ToolbarViewButtonLocation.Center:
                    location = ToolbarItemLocation.Center;
                    break;
                default:
                    break;
            }

            ButtonType buttonType = ButtonType.Normal;

            switch (button.ButtonType)
            {
                case ToolbarViewButtonType.Danger:
                    buttonType = ButtonType.Danger;
                    break;
                case ToolbarViewButtonType.Default:
                    buttonType = ButtonType.Default;
                    break;
                case ToolbarViewButtonType.Normal:
                    buttonType = ButtonType.Normal;
                    break;
                case ToolbarViewButtonType.Success:
                    buttonType = ButtonType.Success;
                    break;
                default:
                    break;
            }

            toolbarButton.Location(location)
                .Toolbar(bottomToolbar ? Toolbar.Bottom : Toolbar.Top).Widget(w =>
                                           w.Button()
                                           .Text(button.Text)
                                           .Icon(button.Icon)
                                           .Type(buttonType)
                                           .Option("vasco.DialogResult", button.DialogResult.ToString())
                                           .Option("vasco.ID", button.ID.ToString())
                                           .Option("vasco.Tag", button.Tag)
                                           .Option("vasco.ViewID", button.ViewID)
                                           .Option("vasco.ParentViewID", button.ParentViewID)
                                           .Option("vasco.ButtonCommand", button.ButtonCommand)
                                           .Option("vasco.ControllerNameRedirect", button.ControllerNameRedirect)
                                           .Option("vasco.ControllerNameGet", button.ControllerNameGet)
                                           .Option("vasco.ControllerNamePost", button.ControllerNamePost)
                                           .Option("vasco.ActionNameRedirect", button.ActionNameRedirect)
                                           .Option("vasco.ActionNameGet", button.ActionNameGet)
                                           .Option("vasco.ActionNamePost", button.ActionNamePost)
                                           .OnClick($"{dialogView.DialogID + dialogView.ToolbarButtonJSClickedFuncName}"));

            return toolbarButton;

        }

        public static ToolbarItemBuilder VascoSetupViewToolbarButton(this ToolbarItemBuilder toolbar, ViewModel viewModel, ToolbarViewButton button)
        {
            SetupToolbarButtons(toolbar, viewModel.ToolbarButtonJSClickedFuncName, button);
            return toolbar;
        }


        public static ToolbarItemBuilder VascoSetupViewToolbarButton(this ToolbarItemBuilder toolbar, PageViewModel viewModel, ToolbarViewButton button)
        {
            SetupToolbarButtons(toolbar, viewModel.PageID +  viewModel.ToolbarButtonJSClickedFuncName, button);
            return toolbar;
        }

        private static void SetupToolbarButtons(ToolbarItemBuilder toolbar, string onClickedJSFunctionHandler, ToolbarViewButton button)
        {
            ToolbarItemLocation location = ToolbarItemLocation.After;
            switch (button.ButtonLocation)
            {
                case ToolbarViewButtonLocation.After:
                    location = ToolbarItemLocation.After;
                    break;
                case ToolbarViewButtonLocation.Before:
                    location = ToolbarItemLocation.Before;
                    break;
                case ToolbarViewButtonLocation.Center:
                    location = ToolbarItemLocation.Center;
                    break;
                default:
                    break;
            }

            ButtonType buttonType = ButtonType.Normal;

            switch (button.ButtonType)
            {
                case ToolbarViewButtonType.Danger:
                    buttonType = ButtonType.Danger;
                    break;
                case ToolbarViewButtonType.Default:
                    buttonType = ButtonType.Default;
                    break;
                case ToolbarViewButtonType.Normal:
                    buttonType = ButtonType.Normal;
                    break;
                case ToolbarViewButtonType.Success:
                    buttonType = ButtonType.Success;
                    break;
                default:
                    break;
            }

            toolbar.Location(location).Widget(w =>
                                           w.Button()
                                           .Text(button.Text)
                                           .Disabled(button.Disabled)
                                           .Visible(button.Visible)
                                           .Icon(button.Icon)
                                           .Type(buttonType)
                                           .Option("vasco.DialogResult", button.DialogResult.ToString())
                                           .Option("vasco.ID", button.ID)
                                           .Option("vasco.Tag", button.Tag)
                                           .Option("vasco.ViewID", button.ViewID)
                                           .Option("vasco.ParentViewID", button.ParentViewID)
                                           .Option("vasco.ButtonCommand", button.ButtonCommand)
                                           .Option("vasco.ControllerNameRedirect", button.ControllerNameRedirect)
                                           .Option("vasco.ControllerNameGet", button.ControllerNameGet)
                                           .Option("vasco.ControllerNamePost", button.ControllerNamePost)
                                           .Option("vasco.ActionNameRedirect", button.ActionNameRedirect)
                                           .Option("vasco.ActionNameGet", button.ActionNameGet)
                                           .Option("vasco.ActionNamePost", button.ActionNamePost)
                                           .OnClick("function (e){ " + onClickedJSFunctionHandler+"(e);}"));
        }

        public static SelectBoxBuilder SetupVascoSifEdit(this SelectBoxBuilder sb, SifEditViewModel model)
        {
             sb.ID(model.ID)
             .DataSource(d => d.Mvc().Controller(model.Controller).LoadAction(model.LoadAction).Key(model.ValueField))
             .DataSourceOptions(o => o.Paginate(model.PagingEnabled).PageSize(model.PageSize))
             .DropDownButtonTemplate("")
             .MinSearchLength(1)
             .SearchTimeout(model.SearchTimeout)
             .SearchEnabled(true)
             .SearchMode(DropDownSearchMode.Contains)
             .SearchExpr(model.SearchFields)
             .DisplayExpr(new JS($"function (data) {{ {model.DisplayExpression} }}"))
             .ValueExpr(model.ValueField)
             .OnValueChanged("Vasco.sifEditView.valueChangedCallback")
             .ShowSelectionControls(false);

            if (!string.IsNullOrEmpty(model.ParentViewID))
                sb.Option("vasco.parentViewID", model.ParentViewID);

            if (!string.IsNullOrEmpty(model.SelectListViewFormAction))
           {
                sb.Option("vasco.selectListViewFormController", model.Controller);
                sb.Option("vasco.selectListViewFormAction", model.SelectListViewFormAction);
                sb.FieldTemplate(new JS("Vasco.sifEditView.fieldTemplate2"));
           }

            #region INITIAL_VALUE
            if (model.DefaultValue != null && model.DefaultValueObject != null)
            {
                sb.DataSource(new[] { model.DefaultValueObject });
                sb.Option("vasco.defaultValue", model.DefaultValue);
                sb.Option("vasco.valueField", model.ValueField);
                sb.Option("vasco.loadUrl", VascoWebHelper.UrlHelper.Action(new Microsoft.AspNetCore.Mvc.Routing.UrlActionContext { Controller = model.Controller , Action = model.LoadAction } ));
                sb.Option("vasco.paginate", model.PagingEnabled);
                sb.Option("vasco.pageSize", model.PageSize);

                sb.OnInitialized(@"function (e) { 
                    Vasco.sifEditView.initializeDefaultValue(e);
                }");
            }
            #endregion
            return sb;
        }

        public static IHtmlContent VascoSifEdit(this IHtmlHelper html, SifEditViewModel model)
        {
            var sb = html.DevExtreme().SelectBox();
            sb.SetupVascoSifEdit(model);
            return sb;
        }



        public static FormBuilder<T> VascoDetailView<T>(this IHtmlHelper html, DetailViewModel detailModel)
        {
            var builder = html.DevExtreme().Form<T>();
            builder.ID(detailModel.ViewID)
            .ColCount(detailModel.FormColumnCount)
            .ShowValidationSummary(detailModel.ValidateData)
            .ReadOnly(detailModel.EditState == ViewEditState.View)
            .FormData(detailModel.FormData);


            #region OnInitialized
            
            var onInitializedSB = new StringBuilder();

            //handler funkcija , ki je lahko definirana na vsakem pogledu posebej
            onInitializedSB.Append($"var fnInitialized = window['{detailModel.ViewID}_OnInitialized'];");
            onInitializedSB.Append("if (typeof fnInitialized === 'function')");
            onInitializedSB.Append("{ fnInitialized(e); }");

            //default focus polja
            if (!string.IsNullOrEmpty(detailModel.DefaultFocusedField))
            {
                //definicija fokus funkcije, ki se potem lahko kli�e tudi direktno v javascript preko form instance
                onInitializedSB.Append(" e.component.vascoFocus = function() { ");
                onInitializedSB.Append($@" var editor = e.component.getEditor('{detailModel.DefaultFocusedField}');
                                           if(editor) editor.focus();");
                onInitializedSB.Append(" }; ");

                //klic fokus funkcije (preko timeout)
                onInitializedSB.Append($@" setTimeout(function (arg){{ 
                e.component.vascoFocus();}},{detailModel.FocusTimeout});");
                
            }
            else if (!string.IsNullOrEmpty(detailModel.DefaultFocusedControlID))
            {
                onInitializedSB.Append($@" setTimeout(function (arg){{ 
                var editor = $('#{detailModel.DefaultFocusedControlID}').{detailModel.DefaultFocusedControlName}('instance');
                if(editor) editor.focus();
                }},{detailModel.FocusTimeout});");
            }

            //enable enter as tab
            if (detailModel.EnableEnterAsTab) {
                onInitializedSB.Append($"  Vasco.ui.enable_enter_as_tab('{detailModel.ViewID}'); ");
            }

            builder.OnInitialized($@"function (e) {{ {onInitializedSB.ToString()} }}");
            #endregion

         
            return builder;
        }
    
    }
}
