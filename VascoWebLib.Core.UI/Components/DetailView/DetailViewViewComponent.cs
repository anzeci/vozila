using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib.Core.UI.Components.DetailView
{
    public class DetailViewViewComponent : ViewComponent
    {
        public DetailViewViewComponent()
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(DetailViewModel model)
        {
            return await Task.Run<IViewComponentResult>(() => { return View(model); });
        }
    }
}