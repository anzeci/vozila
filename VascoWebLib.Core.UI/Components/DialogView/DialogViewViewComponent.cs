using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib.Core.UI.Components.DialogView
{
    public class DialogViewViewComponent : ViewComponent
    {
        public DialogViewViewComponent()
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(DialogViewModel model)
        {
            return await Task.Run<IViewComponentResult>(() => { return View(model); });
        }
    }
}