using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VascoWebLib.Core.UI.Interfaces.Menu;
using VascoWebLib.Core.UI.Model.Menu;

namespace VascoWebLib.Core.UI.Components.Menu
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly MenuViewModel model;

        public MenuViewComponent(IVascoMenuService MenuService)
        {
            model = new MenuViewModel(MenuService);
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.Run<IViewComponentResult>(() => { return View(model); });
        }
    }
}