using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib.Core.UI.Components.ListView
{
    public class ListViewViewComponent : ViewComponent
    {
        public ListViewViewComponent()
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(ListViewModel model)
        {
            return await Task.Run<IViewComponentResult>(() => { return View(model); });
        }
    }
}