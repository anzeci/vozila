using VascoWebLib.Core.UI.Model.View;

namespace VascoWebLib.Core.UI.Model
{
    public class ToolbarViewButton
    {
        public ToolbarViewButton()
        {
            ButtonType = ToolbarViewButtonType.Normal;
            ButtonLocation = ToolbarViewButtonLocation.Center;
            DialogResult = DialogViewResult.drNone;
        }

        /// <summary>
        /// Button ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Ko se klikne gumb, na kateri kontroler na strežniku se naredi redirection
        /// </summary>
        public string ControllerNameRedirect { get; set; } = string.Empty;

        /// <summary>
        /// Ko se klikne gumb, na katero akcijo (od kontrolerja) na strežniku se naredi redirection
        /// </summary>
        public string ActionNameRedirect { get; set; } = string.Empty;

        /// <summary>
        /// Ko se klikne gumb, kateri kontroler na strežniku se kliče za pridobitev podatkov
        /// </summary>
        public string ControllerNameGet { get; set; } = string.Empty;

        /// <summary>
        /// Ko se klikne gumb, katera akcija (od kontrolerja) na strežniku se kliče za pridobitev podatkov
        /// </summary>
        public string ActionNameGet { get; set; } = string.Empty;

        /// <summary>
        /// Ko se klikne gumb, kateri kontroler na strežniku se kliče za pošiljanje podatkov
        /// </summary>
        public string ControllerNamePost { get; set; } = string.Empty;

        /// <summary>
        /// Ko se klikne gumb, katera akcija (od kontrolerja) na strežniku se kliče za pošiljanje podatkov
        /// </summary>
        public string ActionNamePost { get; set; } = string.Empty;

        /// <summary>
        /// Kaj avtomatsko naredi gumb
        /// </summary>
        public ToolbarViewButtonCommand ButtonCommand { get; set; } = ToolbarViewButtonCommand.cmdNone;

        /// <summary>
        /// Poljubna oznaka
        /// </summary>
        public string Tag { get; set; } = string.Empty;

        /// <summary>
        /// ID od (ListView/DetailView) od katerega gumb pridobi podatke
        /// </summary>
        public string ViewID { get; set; } = string.Empty;

        /// <summary>
        /// ID od (ListView/DetailView/SifEditView) katerega želimo osvežiti po akciji gumba
        /// </summary>
        public string ParentViewID { get; set; } = string.Empty;

        public string Text { get; set; } = string.Empty;
        public string Icon { get; set; } = string.Empty;
        public ToolbarViewButtonType ButtonType { get; set; }
        public ToolbarViewButtonLocation ButtonLocation { get; set; }
        public DialogViewResult DialogResult { get; set; }

        public bool Disabled { get; set; } = false;
        public bool Visible { get; set; } = true;
    }

    /// <summary>
    /// Akcija, ki se izvede ob kliku na gumb
    /// </summary>
    public enum ToolbarViewButtonCommand
    {
        cmdNone = 0,
        cmdPostFormData = 1,
        cmdSelectListViewRows = 2
        //TODO other actions
    }

    /// <summary>
    /// Lokacija gumba v toolbar
    /// </summary>
    public enum ToolbarViewButtonLocation
    {
        After = 0,
        Before = 1,
        Center = 2,
    }

    /// <summary>
    /// Tip gumba (barva)
    /// </summary>
    public enum ToolbarViewButtonType
    {
        Danger = 1,
        Default = 2,
        Normal = 3,
        Success = 4
    }

    /// <summary>
    /// Se upošteva pri DialogView (ali se dialog zapre)
    /// </summary>
    public enum DialogViewResult
    {
        drNone = 0,
        drYes = 1,
        drNo = 2,
        drOk = 3,
        drCancel = 4,
        drConfirm = 5
    }
}