using System.Collections.Generic;
using VascoWebLib.Core.UI.Interfaces.Menu;

namespace VascoWebLib.Core.UI.Model.Menu
{
    public class MenuViewModel
    {
        public MenuViewModel(IVascoMenuService MenuService)
        {
            MenuItems = MenuService.GetMenuItems();
        }

        public IList<IVascoMenuItem> MenuItems { get; private set; }
    }
}