using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.UI.Model
{
    public class WebResponseModel
    {
        public WebResponseStatus Status { get; set; }
        public string Message { get; set; }

        public static WebResponseModel ResponseOk(string message)
        {
            return new WebResponseModel
            {
                Status = WebResponseStatus.Ok,
                Message = message
            };
        }
        public static WebResponseModel ResponseError(string message)
        {
            return new WebResponseModel
            {
                Status = WebResponseStatus.Error,
                Message = message
            };
        }
    }


    public enum WebResponseStatus
    {
        Error = 0,
        Ok = 1
    }
}
