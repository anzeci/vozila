using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.UI.Model.View
{
    public class SifEditViewModel
    {
        /// <summary>
        /// Unique ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// ParentViewID
        /// </summary>
        public string ParentViewID { get; set; }
        /// <summary>
        /// Kontroller od koder pridobi podatke
        /// </summary>
        public string Controller { get; set; }
        /// <summary>
        /// Akcija od koder pridobi JSON podatke
        /// </summary>
        public string LoadAction { get; set; }
        /// <summary>
        /// Akcija od koder prikaže selection dialog
        /// </summary>
        public string SelectListViewFormAction { get; set; }

        /// <summary>
        /// Value field
        /// </summary>
        public string ValueField { get; private set; }
        /// <summary>
        /// Display field
        /// </summary>
        public string DisplayField { get; private set; }


        private string displayExpression;

        /// <summary>
        /// Oblika formata za prikaz vrstice. Javascript format:
        /// '(' + data.SIFRA +') ' + data.NAZIV
        /// </summary>
        public string DisplayExpression
        {
            get
            {
                if (string.IsNullOrEmpty(displayExpression))
                    return $"if(!data) return ''; return '(' + data.{ValueField} + ') ' + data.{DisplayField};";
                else
                    return $"if(!data) return ''; return {displayExpression};";
            }
            private set
            {
                displayExpression = value;
            }
        }

        public static SifEditViewModel NewInstance(string id, string valueField, string nameField, string parentViewID = "")
        {
            return new SifEditViewModel(id, valueField, nameField, parentViewID);
        }

        /// <summary>
        /// Setup controller and action for lookup data, optionally list selection
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="selectFormAction"></param>
        /// <returns></returns>
        public SifEditViewModel DataSource(string controller, string loadAction = "SifEditViewData", string selectFormAction = "SifEditViewSelectionDialog")
        {
            Controller = controller;
            LoadAction = loadAction;
            SelectListViewFormAction = selectFormAction;
            return this;
        }


        private SifEditViewModel(string id, string valueField, string nameField, string parentViewID = "")
        {
            if (string.IsNullOrEmpty(id))
                ID = "view" + Guid.NewGuid().ToString("N");
            else
                ID = id;

            ParentViewID = parentViewID;

            ValueField = valueField;
            DisplayField = nameField;
            SetSearchFields(ValueField + ";" + DisplayField);
            PagingEnabled = true;
            PageSize = 20;
            SearchTimeout = 0;
        }

        /// <summary>
        /// Example searchFields: SIFRA;NAZIV;NASLOV;DAVCNA
        /// </summary>
        /// <param name="searchFields"></param>
        public void SetSearchFields(string searchFields)
        {
            SearchFields = new string[0];
            if (!string.IsNullOrEmpty(searchFields))
            {
                SearchFields = searchFields.Split(';');
            }
        }

        /// <summary>
        /// Returns search fields array
        /// </summary>
        public string[] SearchFields { get; set; }


        /// <summary>
        /// PageSize (number of items)
        /// Default value 10
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// PageSize (number of items)
        /// Default value 10
        /// </summary>
        public SifEditViewModel SetPageSize(int value)
        {
            PageSize = value;
            return this;
        }


        /// <summary>
        /// Wait time to load data after last key press
        /// Default value 0ms; zaradi hitrosti izpolnjevanja obrazcev
        /// </summary>
        public int SearchTimeout { get; set; }

        /// <summary>
        /// Wait time to load data after last key press
        /// Default value 500ms
        /// </summary>
        public SifEditViewModel SetSearchTimeout(int value)
        {
            SearchTimeout = value;
            return this;
        }

        /// <summary>
        /// Is paging enabled (load data by pages)
        /// Default value TRUE
        /// </summary>
        public bool PagingEnabled { get; set; }

        /// <summary>
        /// Is paging enabled (load data by pages)
        /// Default value TRUE
        /// </summary>
        public SifEditViewModel SetPagingEnabled(bool value)
        {
            PagingEnabled = value;
            return this;
        }

        /// <summary>
        /// SifEdit default value
        /// </summary>
        public object DefaultValue { get; set; }

        public SifEditViewModel SetDefaultValue(object value)
        {
            DefaultValue = value;
            return this;
        }
        /// <summary>
        /// SifEdit default value object
        /// </summary>
        public object DefaultValueObject { get; set; }

        public SifEditViewModel SetDefaultValueObject(object value)
        {
            DefaultValueObject = value;
            return this;
        }

    }
}
