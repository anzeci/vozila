using System;

namespace VascoWebLib.Core.UI.Model.View
{
    /// <summary>
    /// Osnovni razred za stran z (ViewModel: ListView, DetailView) modelom
    /// </summary>
    public class PageViewModel : FrameViewModel
    {
        private readonly string _pageID;

        public PageViewModel(ViewModel viewModel):base()
        {
            _pageID = $"page{Guid.NewGuid().ToString("N")}";
            ViewModel = viewModel;
            ViewModel.Frame = this;
            GetToolbarButtons();
        }

        /// <summary>
        /// Vrne ID strani
        /// </summary>
        public string PageID
        {
            get
            {
                return _pageID;
            }
        }

        /// <summary>
        /// Model
        /// </summary>
        public ViewModel ViewModel { get; set; }


        /// <summary>
        /// Vrne naziv JavaScript funckije, ki se izvede, ko se klikne na toolbar button (zgornji ali spodnji)
        /// Definirana funkcija ima param (objekt): {action, dialogResult, event, cancel, cancelReason}
        /// action = tekstovna vrednost (Id) akcije gumba
        /// dialogResult = tekstovna vrednost  (drNone,drYes,drNo,drOk,drCancel,drConfirm); zapiranje dialoga v primeru <> drNone
        /// cancel = (true|false) �e se �eli prekiniti akcijo zapiranja dialoga v primeru dialogResult <> drNone
        /// cancelReason = dodatno sporo�ilo, zakaj je pri�lo do preklica zapiranja dialoga
        /// viewID
        /// parentViewID
        /// buttonCommand
        /// controllerNameGet
        /// actionNameGet
        /// controllerNamePost
        /// actionNamePost
        /// controllerNameRedirect
        /// actionNameRedirect
        /// {PageID}_PageToolbarButtonClicked
        /// </summary>
        public string ToolbarButtonJSClickedFuncName
        {
            get
            {
                return $"_PageToolbarButtonClicked";
            }
        }

    }
}