namespace VascoWebLib.Core.UI.Model.View
{
    public class DetailViewModel : ViewModel
    {
        public DetailViewModel()
        {
            ViewType = ViewType.DetailView;
        }

        /// <summary>
        /// Kontroler za podatke
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Akcija za vnos (dejanski INSERT v podatkovno bazo)
        /// </summary>
        public string InsertAction { get; set; }

        /// <summary>
        /// Akcija za urejanje (dejanski UPDATE v podatkovni bazi)
        /// </summary>
        public string EditAction { get; set; }

        /// <summary>
        /// Akcija za brisanje (dejanski DELETE v podatkovni bazi)
        /// </summary>
        public string DeleteAction { get; set; }

        /// <summary>
        /// Akcija za preusmeritev po insert/update/delete, ki se izvede preko strani. V primeru dialogov se ignorira (ker se dialog zapre)
        /// </summary>
        public string RedirectAction { get; set; }

        /// <summary>
        /// DetailView form data
        /// </summary>
        public object FormData { get; set; }

        /// <summary>
        /// V katero polje se postavi focus ob prikazu (�e je to polje prazno, potem se preveri �e DefaultFocusedControlID)
        /// </summary>
        public string DefaultFocusedField { get; set; }

        /// <summary>
        /// Katera UI kontrola dobi fokus ob prikazu (v primeru, da je DefaultFocusedField prazen se preverja to polje)
        /// </summary>
        public string DefaultFocusedControlID { get; set; }

        /// <summary>
        /// Naziv dxWidget, ki ga �elimo fokusirati; dxTextBox, dxDateBox, dxNumberBox ....
        /// https://js.devexpress.com/Demos/WidgetsGallery/
        /// </summary>
        public string DefaultFocusedControlName { get; set; }

        /// <summary>
        /// Koliko milisekund po initalizaciji naj naredi focus na nastavjeno polje/kontrolo za fokus
        /// Default = 600
        /// </summary>
        public int FocusTimeout { get; set; } = 600;

        /// <summary>
        /// �tevilo kolon za Form
        /// </summary>
        public int FormColumnCount { get; set; } = 1;

        /// <summary>
        /// Ali je lokalna validacija podatkov omogo�ena
        /// </summary>
        public bool ValidateData { get; set; } = true;

        /// <summary>
        /// Ali je ENTER tipka simulacija TAB tipke (torej ob pritisku na enter se fokusira naslednje polje)
        /// </summary>
        public bool EnableEnterAsTab { get; set; } = true;


        /// <summary>
        /// Naziv Javascript funkcije, ko se klikne akcija v toolbar
        /// </summary>
        public string DetailViewToolbarButtonJSClickedFuncName
        {
            get
            {
                return $"{ViewID}_DetailView_ToolbarButtonClicked";
            }
        }
        

    }
}