using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.UI.Model.View
{
    /// <summary>
    /// Model za stran ali dialog, ki ima listview
    /// </summary>
    public class ListViewModel : ViewModel
    {
       
        public ListViewModel()
        {
            ViewType = ViewType.ListView;
            AllowDelete = true;
            AllowEdit = true;
            AllowInsert = true;
            ListViewOptions = new ListViewOptions();
            RowTextDisplayExpression = "''";
            RowClickJavascriptHandlers = new List<string>();
            RowDblClickJavascriptHandlers = new List<string>();
        }

        /// <summary>
        /// Seznam JS funkcij, ki se kli�ejo ob RowClick
        /// Primer: ListView1RowClick(e)
        /// </summary>
        public List<string> RowClickJavascriptHandlers { get; private set; }

        /// <summary>
        /// Seznam JS funkcij, ki se kli�ejo ob RowDblClick
        /// Primer: ListView1RowDblClick(e)
        /// </summary>
        public List<string> RowDblClickJavascriptHandlers { get; private set; }


        /// <summary>
        /// Klju�a zapisa
        /// npr.["LETO","STEVILKA"] ali "SIFRA"
        /// </summary>
        public string[] Key { get; set; }
        /// <summary>
        /// Kontroler za podatke
        /// </summary>
        public string Controller { get; set; }
        /// <summary>
        /// Akcija kontrolerja, ki vrne json podatke
        /// </summary>
        public string LoadAction { get; set; }
        /// <summary>
        /// Akcija, ki vrne DetailForm
        /// </summary>
        public string EditFormAction { get; set; }

        /// <summary>
        /// Ali se edit form prika�e kot dialog 
        /// </summary>
        public bool EditFormActionAsDialog { get; set; } = true;
        /// <summary>
        /// Akcija, ki vrne DetailForm
        /// </summary>
        public string InsertFormAction { get; set; }
        /// <summary>
        /// Ali se edit form prika�e kot dialog 
        /// </summary>
        public bool InsertFormActionAsDialog { get; set; } = true;
        /// <summary>
        /// Akcija, ki vrne DetailForm
        /// </summary>
        public string ViewFormAction { get; set; }
        /// <summary>
        /// Ali se view form prika�e kot dialog 
        /// </summary>
        public bool ViewFormActionAsDialog { get; set; } = true;
        /// <summary>
        /// Akcija za brisanje (dejanski DELETE v podatkovni bazi)
        /// </summary>
        public string DeleteAction { get; set; }
      
        /// <summary>
        /// Razne nastavitve za prikaz dxDataGrid
        /// </summary>
        public ListViewOptions ListViewOptions { get; set; }

        /// <summary>
        /// JS Expression izraz, ki vrne tekstovno vrednost naziva vrstice 
        /// Na voljo je spremenljivka rowData
        /// Primer vrednosti "'(' + rowData.SIFRA+') ' + rowData.NAZIV"
        /// </summary>
        public string RowTextDisplayExpression { get; set; }


        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e ob kliku na INSERT
        /// </summary>
        public string InsertJSFuncName
        {
            get
            {
                return $"{ViewID}_Insert";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e ob kliku na DELETE
        /// </summary>
        public string DeleteJSFuncName
        {
            get
            {
                return $"{ViewID}_Delete";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e ob kliku na EDIT
        /// </summary>
        public string EditJSFuncName
        {
            get
            {
                return $"{ViewID}_Edit";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e ob kliku na VIEW
        /// </summary>
        public string ViewJSFuncName
        {
            get
            {
                return $"{ViewID}_View";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se uporablja za pridobitev opisa vrstice (recimo "(SIFRA) NAZIV")
        /// Expression (izraz) se definira 
        /// </summary>
        public string RowTextJSFuncName
        {
            get
            {
                return $"{ViewID}_RowText";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e, ko se zapre dialog, ki se je odprl preko seznama (Insert,Edit)
        /// </summary>
        public string DialogResultJSFuncName
        {
            get
            {
                return $"{ViewID}_DialogResult";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e, ko se klikne akcija v toolbar
        /// </summary>
        public string ListViewToolbarButtonJSClickedFuncName
        {
            get
            {
                return $"{ViewID}_ListView_ToolbarButtonClicked";
            }
        }




    }
}
