using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.UI.Model.View
{
    public class ButtonDialogViewModel
    {
        public ButtonDialogViewModel()
        {
        }
       
        /// Kateri kontroler
        /// </summary>
        public string Controller { get; set; }
        /// <summary>
        /// Katera akcija v kontrolerju vrne dialog view
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// JavaScript funkcija, ki vrne parametre, ki se pošljejo
        /// function () { return { param1: 1, param2:3} }
        /// </summary>
        public string JSParamCallback { get; set; }
        /// <summary>
        /// JavaScript funkcija, ko se kliče DialogResult
        /// function (result) { }
        /// To se kliče samo v primeru, da je izpolnjeno in, da gumb dialoga nima določene avtomatske akcije (ButtonCommand)
        /// </summary>
        public string JSDialogResultCallback { get; set; }
    }
}
