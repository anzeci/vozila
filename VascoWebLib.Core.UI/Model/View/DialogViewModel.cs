using System;

namespace VascoWebLib.Core.UI.Model.View
{
    /// <summary>
    /// Osnovni razred za dialog, ki se odpre na dolo�eni strani
    /// </summary>
    public class DialogViewModel : FrameViewModel
    {
        private readonly string _dialogID;

        public DialogViewModel(ViewModel viewModel)
        {
            ViewModel = viewModel;
            ViewModel.Frame = this;
            _dialogID = $"dialog{Guid.NewGuid().ToString("N")}";
            FullScreen = false;
            DragEnabled = false;
            ShowTitle = true;
            Visible = true;
            CloseOnOutsideClick = true;
        }

      

        /// <summary>
        /// Vrne ID dialoga
        /// </summary>
        public string DialogID
        {
            get
            {
                return _dialogID;
            }
        }

        /// <summary>
        /// Dialog title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Model
        /// </summary>
        public ViewModel ViewModel { get; set; }

        /// <summary>
        /// Ali se prika�e �ez cel zaslon
        /// </summary>
        public bool FullScreen { get; set; }

        /// <summary>
        /// Ali se prika�e glava okna s prostorom za naziv
        /// </summary>
        public bool ShowTitle { get; set; }

        /// <summary>
        /// Ali se lahko okno premika z vle�enjem
        /// </summary>
        public bool DragEnabled { get; set; }

        /// <summary>
        /// Ali se okno zapre, ko se klikne izven njegovega okvirja
        /// </summary>
        public bool CloseOnOutsideClick { get; set; }

        /// <summary>
        /// Ali je dialog viden
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e , ko postane dialog neviden
        /// {DialogID}_Hidden
        /// </summary>
        public string HiddenJSFuncName
        {
            get
            {
                return "_Hidden";
            }
        }

        /// <summary>
        /// Vrne naziv javascript funkcije, ki se kli�e , ko postane dialog viden
        /// {DialogID}_Shown
        /// </summary>
        public string ShownJSFuncName
        {
            get
            {
                return "_Shown";
            }
        }

        /// <summary>
        /// Vrne naziv JavaScript funckije, ki se izvede, ko se klikne na toolbar button (zgornji ali spodnji)
        /// Definirana funkcija ima param (objekt): {action, dialogResult, event, cancel, cancelReason}
        /// action = tekstovna vrednost (Id) akcije gumba
        /// dialogResult = tekstovna vrednost  (drNone,drYes,drNo,drOk,drCancel,drConfirm); zapiranje dialoga v primeru <> drNone
        /// cancel = (true|false) �e se �eli prekiniti akcijo zapiranja dialoga v primeru dialogResult <> drNone
        /// cancelReason = dodatno sporo�ilo, zakaj je pri�lo do preklica zapiranja dialoga
        /// viewID
        /// parentViewID
        /// buttonCommand
        /// controllerNameGet
        /// actionNameGet
        /// controllerNamePost
        /// actionNamePost
        /// controllerNameRedirect
        /// actionNameRedirect
        /// {ViewID}_DialogToolbarButtonClicked
        /// </summary>
        public string ToolbarButtonJSClickedFuncName
        {
            get
            {
                return "_DialogToolbarButtonClicked";
            }
        }
    }
}