using System;
using System.Collections.Generic;

namespace VascoWebLib.Core.UI.Model.View
{
    public abstract class ViewModel: FrameViewModel
    {
        private readonly string _viewID;
        private string _parentViewID;

        public ViewModel()
        {
            _viewID = $"view{Guid.NewGuid().ToString("N")}";
            _parentViewID = string.Empty;
            ParentViewType = ViewType.None;
            SetReadOnly();
        }

        /// <summary>
        /// PageViewModel| DialogViewModel
        /// </summary>
        public FrameViewModel Frame { get; set; }

        /// <summary>
        /// ViewID
        /// </summary>
        public string ViewID
        {
            get
            {
                return _viewID;
            }
        }

        /// <summary>
        /// ParentViewID
        /// </summary>
        public string ParentViewID
        {
            get
            {
                return _parentViewID;
            }
            set
            {
                _parentViewID = value;
            }
        }

        /// <summary>
        /// Parent ViewType
        /// </summary>
        public ViewType ParentViewType { get; set; }

        /// <summary>
        /// ViewType
        /// </summary>
        protected ViewType ViewType { get; set; }

        /// <summary>
        /// EditState (View,Edit,Insert)
        /// </summary>
        public ViewEditState EditState { get; set; }

        /// <summary>
        /// Naslov do mape, kjer je definiran DetailView/ListView uporabni�ki vmesnik (/Views/Shared/XXXX/_XXXXView.cshtml)
        /// </summary>
        public string ViewPath { get; set; }

        /// <summary>
        /// Naslov/naziv pogleda
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Je dovoljeno dodajanje
        /// </summary>
        public bool AllowInsert { get; set; }

        /// <summary>
        /// Je dovoljeno urejanje
        /// </summary>
        public bool AllowEdit { get; set; }

        /// <summary>
        /// Je dovoljeno brisanje
        /// </summary>
        public bool AllowDelete { get; set; }

        /// <summary>
        /// Je dovoljeno gledanje
        /// </summary>
        public bool AllowView { get; set; }

        /// <summary>
        /// ReadOnlyView
        /// </summary>
        public void SetReadOnly()
        {
            EditState = ViewEditState.View;
            AllowDelete = false;
            AllowEdit = false;
            AllowInsert = false;
            AllowView = true;
        }


        /// <summary>
        /// Dolo�i vse pravice
        /// </summary>
        public void SetAllowInsertUpdateDelete(bool value)
        {
            AllowInsert = value;
            AllowEdit = value;
            AllowDelete = value;
        }

        /// <summary>
        /// Tip avtomatske vi�ine, v kolikor je pogled (ListView/DetailView) na strani (ni v dialogu)
        /// </summary>
        public ViewHeightType AutoHeightOnPage { get; set; } = ViewHeightType.Fill;

        /// <summary>
        /// Vrne naziv JavaScript funckije, ki se izvede, ko se klikne na toolbar button (zgornji ali spodnji)
        /// Definirana funkcija ima param (objekt): {action, dialogResult, event, cancel, cancelReason}
        /// action = tekstovna vrednost (Id) akcije gumba
        /// dialogResult = tekstovna vrednost  (drNone,drYes,drNo,drOk,drCancel,drConfirm); zapiranje dialoga v primeru <> drNone
        /// cancel = (true|false) �e se �eli prekiniti akcijo zapiranja dialoga v primeru dialogResult <> drNone
        /// cancelReason = dodatno sporo�ilo, zakaj je pri�lo do preklica zapiranja dialoga
        /// viewID
        /// parentViewID
        /// buttonCommand
        /// controllerNameGet
        /// actionNameGet
        /// controllerNamePost
        /// actionNamePost
        /// controllerNameRedirect
        /// actionNameRedirect
        /// {ViewID}_ToolbarButtonClicked
        /// </summary>
        public string ToolbarButtonJSClickedFuncName
        {
            get
            {
                return $"{ViewID}_ToolbarButtonClicked";
            }
        }

    }

    public enum ViewType
    {
        None,
        ListView,
        DetailView
    }

    public enum ViewHeightType
    {
        None,
        Fill,
        Half
    }

    public enum ViewEditState
    {
        View,
        Edit,
        Insert
    }
}