using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.UI.Model.View
{
    public abstract class FrameViewModel
    {
        private readonly List<ToolbarViewButton> _BottomToolbar;
        private readonly List<ToolbarViewButton> _TopToolbar;

        public FrameViewModel()
        {
            _BottomToolbar = new List<ToolbarViewButton>();
            _TopToolbar = new List<ToolbarViewButton>();
        }

        /// <summary>
        /// Gumbi v glavi frame
        /// </summary>
        public List<ToolbarViewButton> TopToolbar
        {
            get
            {
                return _TopToolbar;
            }
        }

        /// <summary>
        /// Gumbi v nogi frame
        /// </summary>
        public List<ToolbarViewButton> BottomToolbar
        {
            get
            {
                return _BottomToolbar;
            }
        }

        /// <summary>
        /// V razredih, ki dedujejo po tem razredu se lahko dolo�i default gumbe
        /// </summary>
        public virtual void GetToolbarButtons() {

        }

    }

   
}
