namespace VascoWebLib.Core.UI.Model.View
{
    public class ListViewOptions
    {
        /// <summary>
        /// Konstruktor (default vrednosti)
        /// </summary>
        public ListViewOptions()
        {
            //default false
            ShowBorders = false;
            GroupPanelVisible = false;
            AutoExpandGroups = false;
            RowDblClickSelection = false;
            //default true
            HeaderFilterVisible = true;
            FilterRowVisible = true;
            RemoteOperations = true;
            ShowRowLines = true;
            ShowColumnLines = false;
            ShowColumnHeaders = true;
            PageSize = 30;
            SelectionMode = ListViewSelectionMode.None;
            SearchEnabled = true;
            SearchPosition = ListViewSearchPosition.Left;
            ColumnAutoWidth = true;
            ShowColumnChooser = true;
            FocusSearchBoxOnShow = true;
            
        }

        public static bool CompactTheme = true;

        public string ViewID { get; private set; }

        public bool ShowBorders { get; set; }

        /// <summary>
        /// Ali je viden prostor za grupiranje
        /// </summary>
        public bool GroupPanelVisible { get; set; }

        /// <summary>
        /// Omogočeno filtriranje po posameznem stolpcu
        /// </summary>
        public bool HeaderFilterVisible { get; set; }

        /// <summary>
        /// Ali je vidna vrstica z možnostjo filtra po posameznem stolpcu
        /// </summary>
        public bool FilterRowVisible { get; set; }

        /// <summary>
        /// Ali avtomatsko razširi vse grupe
        /// </summary>
        public bool AutoExpandGroups { get; set; }

        /// <summary>
        /// Ali se vse operacije (iskanje, sortiranje, grupiranje ...) izvajajo na strežniku (SQL poizvedba v bazi).
        /// </summary>
        public bool RemoteOperations { get; set; }

        /// <summary>
        /// Ali prikazuje črte stolpcev
        /// </summary>
        public bool ShowColumnLines { get; set; }

        /// <summary>
        /// Ali prikazuje črte vrstic
        /// </summary>
        public bool ShowRowLines { get; set; }

        /// <summary>
        /// Ali prikazuje glavo (naziv stolpcev)
        /// </summary>
        public bool ShowColumnHeaders { get; set; }

        /// <summary>
        /// Max število zapisov na eni strani
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Možnost izbire zapisov
        /// ListViewSelectionMode.None, ListViewSelectionMode.Single, ListViewSelectionMode.Multiple
        /// </summary>
        public ListViewSelectionMode SelectionMode { get; set; }


        /// <summary>
        /// Ali se z dvojnim klikom izbere zapis oz. se kliče procedura za izbiro v parent view ( v primeru dialoga se dialog zapre)
        /// </summary>
        public bool RowDblClickSelection { get; set; } = false;


        /// <summary>
        /// Ali je omogočeno iskanje po vseh kolonah
        /// </summary>
        public bool SearchEnabled { get; set; }

        /// <summary>
        /// Pozicija, kje nad seznamom se nahaja polje za iskanje
        /// </summary>
        public ListViewSearchPosition SearchPosition { get; set; }

        /// <summary>
        /// Avtomatska širina stolpcev
        /// </summary>
        public bool ColumnAutoWidth { get; set; }

        /// <summary>
        /// Ali je možna poljubna izbira stolpcev
        /// </summary>
        public bool ShowColumnChooser { get; set; }

        /// <summary>
        /// Ali fokusira polje za iskanje ob prikazu
        /// </summary>
        public bool FocusSearchBoxOnShow { get; set; }

        /// <summary>
        /// Ob scroll-u seznama se sproti nalagajo in prikazujejo  zapisi. Če je false pa se vidijo številke za strani.
        /// </summary>
        public bool VirtualPager { get; set; } = true;

    }

    /// <summary>
    /// ListView selection mode enums
    /// </summary>
    public enum ListViewSelectionMode
    {
        None = 0,
        Single = 1,
        Multiple = 2
    }

    /// <summary>
    /// ListView search panel position
    /// </summary>
    public enum ListViewSearchPosition
    {
        Left = 1,
        Center = 2,
        Right = 3
    }


}