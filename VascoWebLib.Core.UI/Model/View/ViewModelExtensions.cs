namespace VascoWebLib.Core.UI.Model.View
{
    public static class ViewModelExtensions
    {
        /// <summary>
        /// Is ListViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool IsListViewModel(this ViewModel model)
        {
            return model is ListViewModel;
        }

        /// <summary>
        /// Cast as ListViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ListViewModel AsListViewModel(this ViewModel model)
        {
            return model as ListViewModel;
        }

        /// <summary>
        /// Is DetailViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool IsDetailViewModel(this ViewModel model)
        {
            return model is DetailViewModel;
        }

        /// <summary>
        /// Cast as DetailViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static DetailViewModel AsDetailViewModel(this ViewModel model)
        {
            return model as DetailViewModel;
        }

        /// <summary>
        /// Is PageViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool IsPageViewModel(this ViewModel model)
        {
            return model.Frame is PageViewModel;
        }

        /// <summary>
        /// Cast As PageViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static PageViewModel AsPageViewModel(this ViewModel model)
        {
            return model.Frame as PageViewModel;
        }

        /// <summary>
        /// Is DialogViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool IsDialogViewModel(this ViewModel model)
        {
            return model.Frame is DialogViewModel;
        }

        /// <summary>
        /// Cast As DialogViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static DialogViewModel AsDialogViewModel(this ViewModel model)
        {
            return model.Frame as DialogViewModel;
        }

    }
}