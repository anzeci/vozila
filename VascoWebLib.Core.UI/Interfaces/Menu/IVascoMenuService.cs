using System;
using System.Collections.Generic;
using System.Text;

namespace VascoWebLib.Core.UI.Interfaces.Menu
{
    public interface IVascoMenuService
    {
        string ImagesRootPath { get;}
        IList<IVascoMenuItem> GetMenuItems();
    }
}
