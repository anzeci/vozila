using System.Collections.Generic;

namespace VascoWebLib.Core.UI.Interfaces.Menu
{
    public interface IVascoMenuItem
    {
        string ID { get; set; }
        string CategoryId { get; set; }
        string Text { get; set; }
        bool Expanded { get; set; }
        bool Selected { get; set; }
        string Image { get; set; }
        string Url { get; set; }
        IList<IVascoMenuItem> SubItems { get; set; }

        void CheckSelected(string currentUrl);
    }
}