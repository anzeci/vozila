﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vozila.Lib;

namespace Vozila
{
    public partial class Form1 : Form
    {
        private string conStr = @"User=SYSDBA;Password=masterkey;Database=C:\baze\Vozila.FDB;DataSource=anzec-w10;
            Port=3050;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;
            MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Test();


          //  PartnerRepository par = new PartnerRepository(new SqlConnectionProvider { Provider = FirebirdConnection.FirebirdProviderName, ConnectionString = ConnectionString_PAR });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Import("F:\\podatki\\Anze\\Vozila\\tmp\\renault_nov2.xlsx", "Renault", "New");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Import("F:\\podatki\\Anze\\Vozila\\tmp\\nissan_nov.xlsx", "Nissan", "New");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Import("F:\\podatki\\Anze\\Vozila\\tmp\\dacia_nov.xlsx", "Dacia", "New");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Import("F:\\podatki\\Anze\\Vozila\\tmp\\renault_kupec.xlsx", "Renault", "Kupec");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Import("F:\\podatki\\Anze\\Vozila\\tmp\\dacia_kupec.xlsx", "Renault", "Kupec");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Vozila.Lib.ExcelService ES = new Vozila.Lib.ExcelService(conStr);
            ES.Import("F:\\podatki\\Anze\\Vozila\\tmp\\renault_odprema.xlsx", "Renault", "Odprema");
        }
    }
}
